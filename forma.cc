/*
 * forma.cc
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "forma.hh"
#include <ctype.h>
#include <regex>

std::string Forma::sqlite_pattern_name { "PATTERN" };
std::string Forma::sqlite_pattern_table { "(IDF INTEGER PRIMARY KEY NOT NULL, " \
    "FNAME TEXT, "							\
    "TOKENS TEXT NOT NULL, "						\
    "NOTE TEXT, "							\
    "STATS INTEGER NOT NULL, "						\
    "FOREIGN KEY(STATS) REFERENCES TEST(IDV));" };

int Forma::retrieve_forma(void *data, int argc, char **argv, char **azColName) {
  Forma ptrn;
  Forma* p_ptrn = &ptrn;
  Record rec;
  Glib::ustring rtype;
  std::vector<Forma>* forme { static_cast<std::vector<Forma>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("IDF") && argv[i]) {
      ptrn.set_idf(argv[i]);
      if (forme->size() > 0 && ptrn.get_idf() == forme->rbegin()->get_idf()) {
	p_ptrn = &*(forme->rbegin());
      }
    } else if (std::string(azColName[i]) == std::string("FNAME") && argv[i]) {
      p_ptrn->set_name(argv[i]);
    } else if (std::string(azColName[i]) == std::string("TOKENS") && argv[i]) {
      p_ptrn->set_pattern(argv[i]);
    } else if (std::string(azColName[i]) == std::string("NOTE") && argv[i]) {
      p_ptrn->set_note(argv[i]);
    } else if (std::string(azColName[i]) == std::string("STATS") && argv[i]) {
      p_ptrn->set_idv(argv[i]);
    } else if (std::string(azColName[i]) == std::string("RTYPE") && argv[i]) {
      rtype.assign(argv[i]);
    } else if (std::string(azColName[i]) == std::string("CORRECT") && argv[i]) {
      rec.correct = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("TOTAL") && argv[i]) {
      rec.total = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("DATE") && argv[i]) {
      rec.date = atol(argv[i]);
    } else if (std::string(azColName[i]) == std::string("ERROR") && argv[i]) {
      rec.error = atoi(argv[i]);
      p_ptrn->set_record(Test::get_test_type(rtype),rec);
    }
  }
  if (p_ptrn == &ptrn) {
    forme->push_back(ptrn);
  }
  return 0;
}

std::vector<Sintagma> Forma::parse_tokens(const std::string& str) {
  std::vector<Sintagma> tokens;
  std::string seq { str };
  std::regex tkr { "[nav][[:digit:]]*\\[[^\\]]*\\]\\([^\\)]*\\)" };
  std::smatch matches;
  while (std::regex_search(seq, matches, tkr)) {
    std::smatch::iterator iter { matches.begin() };
    std::string token { *iter };
    SyntType st;
    VbConjug vc { VbConjug::null };
    AjConjug ac { AjConjug::null };
    std::size_t sqbrk { token.find("[", 1) };
    switch (token[0]) {
    case 'n':
      st = SyntType::nominale;
      break;
    case 'a':
      st = SyntType::aggettivale;
      ac = Parola::get_aconjugation(atoi(token.substr(1, sqbrk - 1).c_str()));
      break;
    case 'v':
      st = SyntType::verbale;
      vc = Parola::get_vconjugation(atoi(token.substr(1, sqbrk - 1).c_str()));
      break;
    }
    std::size_t esqbrk { token.find("]", sqbrk) };
    Glib::ustring part { token.substr(sqbrk + 1, esqbrk - sqbrk - 1) };
    Glib::ustring label { token.substr(esqbrk + 2, token.size() - esqbrk - 3) };
    Sintagma snt { st, label, vc, ac, part };
    tokens.push_back(snt);
    seq = matches.suffix().str();
  }
  return tokens;
}

Forma::Forma(const Forma& frm) : idf (frm.idf), name (frm.name), pattern (frm.pattern), note (frm.note) {
  idv = frm.idv;
  creation = frm.creation;
  statistics = frm.statistics;
}

Forma& Forma::operator=(const Forma& frm) {
  if (this != &frm) {
    idf = frm.idf;
    name = frm.name;
    pattern = frm.pattern;
    note = frm.note;
    idv = frm.idv;
    creation = frm.creation;
    statistics = frm.statistics;
  }
  return *this;
}

bool Forma::operator==(const Forma &pat) const {
  if (idf == pat.idf && name.compare(pat.name) == 0) {
    if (get_pattern_string().compare(pat.get_pattern_string()) == 0 && note.compare(pat.note) == 0) {
      return true;
    }
  }
  return false;
}

bool Forma::operator!=(const Forma &pat) const {
  if (idf != pat.idf || name.compare(pat.name) != 0 || get_pattern_string().compare(pat.get_pattern_string()) != 0 || note.compare(pat.note) != 0) {
      return true;
  }
  return false;
}

std::string Forma::get_pattern_string(void) const {
  std::stringstream ss;
  std::vector<Sintagma>::const_iterator iter { pattern.cbegin() };
  while (iter != pattern.cend()) {
    ss << *iter;
    iter++;
  }
  return ss.str();
}

Glib::ustring Forma::print_pattern(void) const {
  Glib::ustring str;
  std::vector<Sintagma>::const_iterator iter { pattern.cbegin() };
  while (iter != pattern.cend()) {
    str.append(iter->print() + "\t");
    iter++;
  }
  return str;
}
