/*
 * test_scelta_mult.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TESTSCELTAMULT_HH_
#define _TESTSCELTAMULT_HH_

#include "test_engine.hh"

class TestSceltaMult : public TestEngine {
public:
  inline ~TestSceltaMult() {}
protected:
  std::set<short> right_answers;
  Glib::ustring risposta_esatta;
  // main
  Gtk::Button* answer1_button;
  Gtk::Label* answer1_label;
  Gtk::Label* answer1up_label;
  sigc::connection answer1_connection;
  Gtk::Button* answer2_button;
  Gtk::Label* answer2_label;
  Gtk::Label* answer2up_label;
  sigc::connection answer2_connection;
  Gtk::Button* answer3_button;
  Gtk::Label* answer3_label;
  Gtk::Label* answer3up_label;
  sigc::connection answer3_connection;
  Gtk::Button* answer4_button;
  Gtk::Label* answer4_label;
  Gtk::Label* answer4up_label;
  sigc::connection answer4_connection;
  Gtk::Button* answer5_button;
  Gtk::Label* answer5_label;
  Gtk::Label* answer5up_label;
  sigc::connection answer5_connection;
  Gtk::Button* answer6_button;
  Gtk::Label* answer6_label;
  Gtk::Label* answer6up_label;
  sigc::connection answer6_connection;
  
  inline TestSceltaMult() {}
  void init_test_scelta_mult(Glib::RefPtr<Gtk::Builder> bldr);
  void connect_signals(void);
  void pause_test(void);
  void continue_test(void);
  void on_window_closed(void);
};

#endif // _TESTSCELTAMULT_HH_
