/*
 * drawing_kanji_sheet.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DRAWINGKANJISHEET_HH_
#define _DRAWINGKANJISHEET_HH_

#include <gtkmm.h>
#include "kanji.hh"

class DrawingKanjiSheet: public Gtk::DrawingArea {
 public:
  DrawingKanjiSheet();
  inline virtual ~DrawingKanjiSheet() {}
  inline void set_kanji(const std::vector<Kanji>& vec) { kanji = vec; }
  virtual int count_pagine(void) = 0;
  virtual void draw_kanji_table(const Cairo::RefPtr<Cairo::Context>& cr);
  virtual void set_up_kanji_table(const Cairo::RefPtr<Cairo::Context>& cr) = 0;
  virtual void draw_kanji_grid(const Cairo::RefPtr<Cairo::Context>& cr, double dy = 0.0) = 0;
  virtual void draw_kanji_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int pagina, bool pages = false) = 0;
protected:
  virtual void get_preferred_width_vfunc(int& minimum_width, int& natural_width) const;
  virtual void get_preferred_height_for_width_vfunc(int width, int& minimum_height, int& natural_height) const;
  virtual void get_preferred_height_vfunc(int& minimum_height, int& natural_height) const;
  virtual void get_preferred_width_for_height_vfunc(int height, int& minimum_width, int& natural_width) const;
  virtual void on_size_allocate(Gtk::Allocation& allocation);
  virtual void on_realize(void);
  virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
  Glib::RefPtr<Gdk::Window> ref_window;
  std::vector<Kanji> kanji;
  double left_margin;
  double right_margin;
  double up_margin;
  double down_margin;
  double cWidth;
  double cHeight;
  double pWidth;
  double pHeight;
};

#endif // _DRAWINGKANJISHEET_HH_
