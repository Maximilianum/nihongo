/*
 * database.hh
 * Copyright (C) 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DATABASE_H_
#define _DATABASE_H_

#include <iostream>
#include <sqlite3.h>
#include "db_engine.hh"
#include "parola.hh"
#include "kanji.hh"
#include "forma.hh"
#include "lezione.hh"

class Database : public DBEngine {
public:  
  Database(const Glib::ustring& db_path);
  inline ~Database() { sqlite3_close(sqlt_db); }
  // test
  Test get_test(const long long int id);
  std::map<TestType,Record> get_records(const long long int id);
  long long int insert(const Test& vc, ItemType it);
  void insert(const Record& rec, long long int id, TestType tt);
  void update(const Record& rec, long long int id, TestType tt);
  void delete_record(const long long int id, TestType tt);
  void delete_test(const long long int id);
  // parole
  std::vector<Parola> get_parole(const Glib::ustring& where = Glib::ustring());
  std::vector<long long int> get_id_parole(const Glib::ustring& where);
  Parola get_parola(const long long int id);
  Parola get_parola_random(const Glib::ustring& where = Glib::ustring());
  void insert(const Parola& wrd);
  void update(const Parola& wrd);
  void delete_parola(const long long id);
  void reset_parola(const long long int id);
  // kanji
  std::vector<Kanji> get_kanji(const Glib::ustring& where = Glib::ustring());
  std::vector<long long int> get_id_kanji(const Glib::ustring& where);
  Kanji get_kanji(const long long int id);
  Kanji get_kanji_random(void);
  void insert(const Kanji& knj);
  void update(const Kanji& knj);
  void delete_kanji(const long long id);
  void reset_kanji(const long long int id);
  // forme
  std::vector<Forma> get_forme(const Glib::ustring& where = Glib::ustring());
  Forma get_forma(const long long int id);
  void insert(const Forma& frm);
  void update(const Forma& frm);
  void delete_forma(const long long id);
  // lezioni
  std::vector<Lezione> get_lezioni(const Glib::ustring& where = Glib::ustring());
  Lezione get_lezione(const long long int id);
  std::vector<Parola> get_parole_della_lezione(long long int id);
  std::vector<Kanji> get_kanji_della_lezione(long long int id);
  std::vector<Forma> get_patterns_della_lezione(long long int id);
  long long int insert(const Lezione& lsn);
  void insert_lword(long long int idl, long long int idw);
  void insert_lkanji(long long int idl, long long int idk);
  void insert_lpattern(long long int idl, long long int idf);
  void update(const Lezione& lsn);
  void delete_lezione(const long long id);
  void delete_lword(long long int idl, long long int idw);
  void delete_lkanji(long long int idl, long long int idk);
  void delete_lpattern(long long int idl, long long int idf);
};

#endif // _DATABASE_H_

