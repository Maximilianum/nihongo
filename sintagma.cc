/*
 * sintagma.cc
 *
 * Copyright (C) 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "sintagma.hh"

std::ostream& operator<<(std::ostream& os, const Sintagma& snt) {
  switch (snt.type) {
  case SyntType::nominale:
    os << 'n';
    break;
  case SyntType::aggettivale:
    os << 'a' << Parola::get_conjugation_index(snt.aform);
    break;
  case SyntType::verbale:
    os << 'v' << Parola::get_conjugation_index(snt.vform);
    break;
  }
  os << "[" << snt.part.raw() << "]";
  os << '(' << snt.label.raw() << ')';
  return os;
}

std::istream& operator>>(std::istream& is, Sintagma& snt) {
  std::string str;
  is >> str;
  std::cout << "op<< " << str << std::endl;
  return is;
}

Sintagma& Sintagma::operator=(const Sintagma& sn) {
  if (this != &sn) {
    type = sn.type;
    label = sn.label;
    vform = sn.vform;
    aform = sn.aform;
    part = sn.part;
  }
  return *this;
}

bool Sintagma::operator<(const Sintagma& sn) const {
  if (type < sn.type) {
    return true;
  } else if (type == sn.type) {
    if (type == SyntType::aggettivale && aform < sn.aform) {
      return true;
    } else if (type == SyntType::verbale && vform < sn.vform) {
      return true;
    }
  }
  return false;
}

bool Sintagma::operator==(const Sintagma& sn) const {
  if (type == sn.type && label.compare(sn.label) == 0 && vform == sn.vform && aform == sn.aform && part.compare(sn.part) == 0) {
    return true;
  }
  return false;
}

Glib::ustring Sintagma::print(void) const {
  std::stringstream ss;
  switch (type) {
  case SyntType::nominale:
    ss << "〜";
    break;
  case SyntType::aggettivale:
    ss << "agg.vo " << Parola::get_conjugation_str(aform);
    break;
  case SyntType::verbale:
    ss << "verbo " << Parola::get_conjugation_str(vform);
    break;
  }
  ss << "\t" << part.raw();
  if (label.compare(Glib::ustring()) != 0) {
    ss << " (" << label.raw() << ")";
  }
  return Glib::ustring(ss.str());
}
