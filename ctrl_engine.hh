/*
 * ctrl_engine.hh
 * Copyright (C) 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CTRL_ENGINE_HH_
#define _CTRL_ENGINE_HH_

#include <gtkmm.h>
#include <giomm/file.h>
#include <iostream>
#include <fstream>
#include "combobox_tmcr.hh"

class CtrlEngine {
 public:
  virtual inline ~CtrlEngine() {}
  inline Gtk::Window* get_main_window(void) { return main_win; }
protected:
  virtual void connect_signals(void) {}
  inline void on_cancel_button_clicked(Gtk::Window *win) { win->hide(); }
  // combobox
  void load_data_combobox(std::vector<Glib::ustring>::iterator start, std::vector<Glib::ustring>::iterator end, Glib::RefPtr<Gtk::ListStore> list);
  int get_row_index_for_value_in_combobox(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring &value);
  Gtk::TreeModel::iterator get_combobox_item(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring& str);
  // main win
  bool on_main_win_focus_in(GdkEventFocus *event);
  bool on_main_win_focus_out(GdkEventFocus *event);
  // TreeView
  std::vector<Gtk::TreeRowReference> get_treeview_row_ref(const std::vector<Gtk::TreePath> &sel, Glib::RefPtr<Gtk::TreeModel> model);

  /**
   * variables
   **/

  // window
  Gtk::Window *main_win;
  ComboboxTreeModelColumnRecord combobox_tmcr;
};

#endif // _CTRL_ENGINE_HH_
