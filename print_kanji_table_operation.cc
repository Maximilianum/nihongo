/*
 * print_kanji_table_operation.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "print_kanji_table_operation.hh"
#include "kakikata_sheet.hh"
#include "oboeru_sheet.hh"

PrintKanjiTableOperation::PrintKanjiTableOperation() {

}

PrintKanjiTableOperation::~PrintKanjiTableOperation() {

}

Glib::RefPtr<PrintKanjiTableOperation> PrintKanjiTableOperation::create() {
	return Glib::RefPtr<PrintKanjiTableOperation>{ new PrintKanjiTableOperation() };
}

void PrintKanjiTableOperation::on_begin_print(const Glib::RefPtr<Gtk::PrintContext>& print_context) {
	Cairo::RefPtr<Cairo::Context> cr { print_context->get_cairo_context() };
	set_n_pages(kakuArea->count_pagine());
	kakuArea->set_up_kanji_table(cr);
}

void PrintKanjiTableOperation::on_draw_page(const Glib::RefPtr<Gtk::PrintContext>& context, int page_nr) {
	Cairo::RefPtr<Cairo::Context> cr { context->get_cairo_context() };
	kakuArea->draw_kanji_grid(cr);
	kakuArea->draw_kanji_at_page(cr, page_nr);
}
