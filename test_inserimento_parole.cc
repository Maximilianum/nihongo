/*
 * test_inserimento_parole.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_inserimento_parole.hh"

TestInserimentoParole::TestInserimentoParole(Glib::RefPtr<Gtk::Builder> bldr, Database* db, const std::vector<long long int>& vec) : database (db), content (vec) {
  shuffle<long long int>(&content);
  current = content.begin();
  init_test_inserimento(bldr);
  curr_vconj = options.vconj;
  curr_aconj = options.aconj;
  connect_signals();
}

void TestInserimentoParole::run_test(const TestOptions& opts) {
  int nr = content.size();
  question_tot_label->set_text(Glib::ustring::format(nr));
  main_win->present();
  answer_entry->grab_focus();
  timer = 0;
  nr_quest = 0;
  options = opts;
  update_window();
}

AjConjug TestInserimentoParole::get_random_adjective_conjugation(void) {
  int n = TestEngine::random(2, 8);
  return Parola::get_aconjugation(n);
}

VbConjug TestInserimentoParole::get_random_verb_conjugation(void) {
  int n = TestEngine::random(2, 13);
  return Parola::get_vconjugation(n);
}

void TestInserimentoParole::update_window(void) {
  try {
    current_word = database->get_parola(*current);
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestInserimentoParole::update_window() " << dbe.get_message() << std::endl;
  }
  Glib::ustring question;
  Glib::ustring upquestion;
  question_nr_label->set_text(Glib::ustring::format(nr_quest + 1));
  timer_connection = Glib::signal_timeout().connect_seconds(sigc::mem_fun(this, &TestInserimentoParole::on_timer_fired), 1);
  time_label->set_text("00 : 00");
  if (options.tst_type == TestType::cnj_ins) {
    curr_negative = options.conj_negative >= 0 ? options.conj_negative : TestEngine::random_bool();
    curr_plain = options.conj_style >= 0 ? options.conj_style : TestEngine::random_bool();
    Glib::ustring neg_str = curr_negative ? "negativa" : "affermativa";
    Glib::ustring style_str = curr_plain ? "colloquiale" : "cortese";
    if (options.aconj == AjConjug::all) {
      curr_aconj = get_random_adjective_conjugation();
    } else if (options.vconj == VbConjug::all) {
      curr_vconj = get_random_verb_conjugation();
    }
    bool verb = current_word.is_verbo();
    Glib::ustring conjugation;
    if (verb) {
      conjugation = Parola::get_conjugation_str(curr_vconj);
      switch (curr_vconj) {
      case VbConjug::presente:
      case VbConjug::passato:
      case VbConjug::volitivo:
	conjugation += Glib::ustring(" (") + neg_str + Glib::ustring(" - ") + style_str + Glib::ustring(")");
	break;
      case VbConjug::desiderativo:
      case VbConjug::imperativo:
	conjugation += Glib::ustring(" (") + neg_str + Glib::ustring(")");
	break;
      case VbConjug::esortativo:
      case VbConjug::obbligo:
      case VbConjug::potenziale:
      case VbConjug::passivo:
      case VbConjug::causativo:
	conjugation += Glib::ustring(" (") + style_str + Glib::ustring(")");
	break;
      }
    } else {
      conjugation = Parola::get_conjugation_str(curr_aconj);
      conjugation += Glib::ustring(" (") + neg_str + Glib::ustring(" - ") + style_str + Glib::ustring(")");
    }
    if (current_word.get_furigana().compare(Glib::ustring()) == 0) {
      upquestion = Glib::ustring("\"") + conjugation + Glib::ustring("\"");
      question = current_word.get_kotoba();
    } else {
      upquestion = Glib::ustring("\"") + conjugation + Glib::ustring("\"");
      question = current_word.get_kotoba() + Glib::ustring(" (") + current_word.get_furigana() + Glib::ustring(" )");
    }
  } else {
    question.assign(current_word.get_kotoba());
  }
  upquestion_label->set_text(upquestion);
  question_label->set_text(question);
  answer_entry->set_text("");
}

void TestInserimentoParole::on_risposta(short key) {
  Glib::ustring esatta;
  Glib::ustring answ { answer_entry->get_text() };
  if (options.tst_type == TestType::cnj_ins) {
    if (current_word.is_verbo()) {
      esatta = current_word.get_conjugation(curr_vconj, curr_plain, curr_negative);
    } else {
      esatta = current_word.get_conjugation(curr_aconj, curr_plain, curr_negative);
    }
  } else if (options.tst_type == TestType::read_wrd_ins) {
    esatta = current_word.get_furigana();
  }
  bool result { answ.compare(esatta) == 0 };
  timer_connection.disconnect();
  try {
    Record rec { current_word.test(options.tst_type, result) };
    database->update(rec, current_word.get_idv(), options.tst_type);
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestInserimentoParole::on_risposta() " << dbe.get_message() << std::endl;
  }
  if (!result) {
    Glib::ustring msg { Glib::ustring::compose("Sbagliato, la risposta giusta è\n<b><big>%1</big></b>", esatta) };
    Gtk::MessageDialog* alert { new Gtk::MessageDialog(*main_win, "", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
    alert->set_message(msg, true);
    int res_id = alert->run();
    delete alert;
  }
  next_question();
}

void TestInserimentoParole::next_question(void) {
  timer = 0;
  nr_quest++;
  current++;
  if (current == content.end()) {
    main_win->hide();
  } else {
    update_window();
  }
}

