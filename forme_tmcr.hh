/*
 * forme_tmcr.hh
 *
 * Copyright (C) 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FORME_TREE_MODEL_COLUMN_RECORD_HH_
#define _FORME_TREE_MODEL_COLUMN_RECORD_HH_

#include <gtkmm.h>

class FormeTreeModelColumnRecord : public Gtk::TreeModelColumnRecord {
 public:
  FormeTreeModelColumnRecord() { add(idf);
    add(name);
    add(pattern);
    add(descr);
    add(note);
    add(fcolour);
    add(fsize); }
  Gtk::TreeModelColumn<long long int> idf;
  Gtk::TreeModelColumn<Glib::ustring> name;
  Gtk::TreeModelColumn<Glib::ustring> pattern;
  Gtk::TreeModelColumn<Glib::ustring> descr;
  Gtk::TreeModelColumn<Glib::ustring> note;
  Gtk::TreeModelColumn<Gdk::RGBA> fcolour;
  Gtk::TreeModelColumn<double> fsize;
};

#endif // _FORME_TREE_MODEL_COLUMN_RECORD_HH_
