/*
 * parola.hh
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PAROLA_HH_
#define _PAROLA_HH_

#include "test.hh"

enum class VbConjug { null, all, presente, passato, te, desiderativo, volitivo, esortativo, imperativo, obbligo, potenziale, passivo, causativo, condizionale };

enum class AjConjug { null, all, presente, passato, sospensivo, causale, avverbiale, diventare, rendere };

class Parola: public Test {
public:
  // static class functions
  static inline std::string get_sqlite_word_name(void) { return sqlite_word_name; }
  static inline std::string get_sqlite_word_table(void) { return sqlite_word_table; }
  static int retrieve_parola(void *data, int argc, char **argv, char **azColName);
  static int retrieve_id(void *data, int argc, char **argv, char **azColName);
  static inline Glib::ustring get_type_string(int val) { return val > 0 && val <= types.size() ? types[val -1] : Glib::ustring("????"); }
  static std::vector<Glib::ustring> get_types(void) { return types; }
  static int get_type_index_from_name(const Glib::ustring &str);
  static Glib::ustring get_conjugation_str(VbConjug c);
  static Glib::ustring get_conjugation_str(AjConjug c);
  static int get_conjugation_index(VbConjug c);
  static int get_conjugation_index(AjConjug c);
  static VbConjug get_vconjugation(int index);
  static AjConjug get_aconjugation(int index);
  static inline Test get_test(const Parola& wrd) { return Test(wrd.idw, wrd.statistics); }
  Parola() : idw (0), wtype (0), kotoba (""), furigana (""), itariago ("") {}
  Parola(const Parola &verbum);
  Parola(long long int id, int t, const Glib::ustring& kot, const Glib::ustring& yom, const Glib::ustring& ita) : idw (id), wtype (t), kotoba (kot), furigana (yom), itariago (ita) {}
  Parola(int t, const Glib::ustring& kot, const Glib::ustring& yom, const Glib::ustring& ita) : idw (0), wtype (t), kotoba (kot), furigana (yom), itariago (ita) {}
  Parola& operator=(const Parola& verbum);
  bool operator<(const Parola& verbum) const;
  bool operator==(const Parola& verbum) const;
  bool operator!=(const Parola& verbum) const;
  inline void set_idw(long long int val) { idw = val; }
  inline void set_idw(const char* cstr) { idw = atoll(cstr); }
  inline long long int get_idw(void) const { return idw; }
  inline void set_wtype(int val) { wtype = val; }
  inline void set_wtype(const char* cstr) { wtype = atoi(cstr); }
  inline int get_wtype(void) const { return wtype; }
  inline void set_kotoba(const Glib::ustring &str) { kotoba.assign(str); }
  inline void set_kotoba(const char* cstr) { kotoba.assign(cstr); }
  inline Glib::ustring get_kotoba(void) const { return kotoba; }
  inline void set_furigana(const Glib::ustring &str) { furigana.assign(str); }
  inline void set_furigana(const char* cstr) { furigana.assign(cstr); }
  inline Glib::ustring get_furigana(void) const { return furigana; }
  inline void set_itariago(const Glib::ustring &str) { itariago.assign(str); }
  inline void set_itariago(const char* cstr) { itariago.assign(cstr); }
  inline Glib::ustring get_itariago(void) const { return itariago; }
  inline bool is_valid(void) const { return wtype > 0 && kotoba.compare(Glib::ustring("")) != 0 && itariago.compare(Glib::ustring("")) != 0; }
  bool has_kanji(void) const;
  inline bool is_verbo(void) const { return wtype > 3 && wtype < 9; }
  inline bool is_aggettivo(void) const { return wtype > 1 && wtype < 4; }
  std::list<Glib::ustring> get_kanji_list(void) const;
  void get_forme_list(std::list<Glib::ustring> &lista) const;
  Glib::ustring get_conjugation(VbConjug vc, bool style = false, bool negative = false) const;
  Glib::ustring get_conjugation(AjConjug ac, bool style = false, bool negative = false) const;
  Glib::ustring get_presente(bool piana = false, bool negativa = false) const;
  Glib::ustring get_passato(bool piana = false, bool negativa = false) const;
  Glib::ustring get_sospensivo(bool piana = false, bool negativa = false) const;
  Glib::ustring get_causale(void) const;
  Glib::ustring get_avverbiale(void) const;
  Glib::ustring get_diventare(bool piana = false, bool negativa = false) const;
  Glib::ustring get_rendere(bool piana = false, bool negativa = false) const;
  Glib::ustring get_desiderativo(bool negativa = false) const;
  Glib::ustring get_volitivo(bool piana = false, bool negativa = false) const;
  Glib::ustring get_esortativo(bool piana = false) const;
  Glib::ustring get_imperativo(bool negativa = false) const;
  Glib::ustring get_obbligo(bool piana = false) const;
  Glib::ustring get_potenziale(bool piana = false) const;
  Glib::ustring get_passivo(bool piana = false) const;
  Glib::ustring get_causativo(bool piana = false) const;
  Glib::ustring get_condizionale(void) const;
  Glib::ustring get_verbo_base(short index) const;
  Glib::ustring get_verbo_in_te(bool ta = false) const;
private:
  static std::string sqlite_word_name;
  static std::string sqlite_word_table;
  static const Glib::ustring godan_table;
  static const std::vector<Glib::ustring> types;
  long long int idw;
  int wtype;
  Glib::ustring kotoba;
  Glib::ustring furigana;
  Glib::ustring itariago;
};

#endif // _PAROLA_HH_
