/*
 * test_inserimento_numeri.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_inserimento_numeri.hh"

TestInserimentoNumeri::TestInserimentoNumeri(Glib::RefPtr<Gtk::Builder> bldr) : current (content.begin()) {
	init_test_inserimento(bldr);
	connect_signals();
}

void TestInserimentoNumeri::run_test(const TestOptions& opts) {
	options = opts;
	content = get_numbers();
	current = content.begin();
	question_tot_label->set_text(Glib::ustring::format(content.size()));
	main_win->present();
	answer_entry->grab_focus();
	timer = 0;
	nr_quest = 0;
	update_window();
}

void TestInserimentoNumeri::update_window(void) {
	Glib::ustring quest;
	question_nr_label->set_text(Glib::ustring::format(nr_quest + 1));
	timer_connection = Glib::signal_timeout().connect_seconds(sigc::mem_fun(this, &TestInserimentoNumeri::on_timer_fired), 1);
	time_label->set_text("00 : 00");
	quest = options.verso == CIFRE_HIRAGANA ? current->question : current->answer;
	question_label->set_text(quest);
	answer_entry->set_text("");
}

void TestInserimentoNumeri::on_risposta(short tasto) {
	Glib::ustring risposta { answer_entry->get_text() };
	Glib::ustring corretta { options.verso == CIFRE_HIRAGANA ? current->answer : current->question };
	bool risultato { risposta.compare(corretta) == 0 };
	timer_connection.disconnect();
	if (!risultato) {
		Glib::ustring messaggio { "Sbagliato, la risposta giusta è\n<b><big>" + corretta + "</big></b>" };
		Gtk::MessageDialog* alert { new Gtk::MessageDialog(*main_win, "", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
		alert->set_message(messaggio, true);
		int res_id { alert->run() };
		delete alert;
	}
	next_question();
}

void TestInserimentoNumeri::next_question(void) {
	timer = 0;
	nr_quest++;
	current++;
	if (current == content.end()) {
		main_win->hide();
	} else {
		update_window();
	}
}

std::vector<Number> TestInserimentoNumeri::get_numbers(void) {
  std::vector<Number> vec;
  std::vector<std::pair<Glib::ustring,Glib::ustring>> vec1;
  if (options.tstn_mode == TestNumbersMode::cardinal) {
    int nrmax, nrgen;
    switch (options.max_val) {
    case 0:
      nrmax = 10;
      break;
    case 1:
      nrmax = 99;
      break;
    case 2:
      nrmax = 999;
      break;
    case 3:
      nrmax = 9999;
      break;
    case 4:
      nrmax = 99999999;
      break;
    }
    for (short i = 0; i < options.quest_nr; i++) {
      Number generato { Glib::ustring::format(i), to_hiragana(i) };
      vec.push_back(generato);
    }
  } else if (options.tstn_mode == TestNumbersMode::quantity) {
    Number quantita[] { {"1","ひとつ"}, {"2","ふたつ"}, {"3","みっつ"}, {"4","よっつ"}, {"5","いつつ"},{"6","むっつ"}, {"7","ななつ"}, {"8","やっつ"}, {"9","ここのつ"}, {"10","とお"} };
    for (short i = 0; i < 10; i++) {
      vec.push_back(quantita[i]);
    }
  } else if (options.tstn_mode == TestNumbersMode::calendar) {
    Number calendario[] { { "一日", "ついたち"}, {"二日", "ふつか"}, {"三日", "みっか"}, {"四日", "よっか"}, {"五日", "いつか"}, {"六日", "むいか"}, {"七日", "なのか"}, {"八日", "ようか"},
	{"九日", "ここのか"}, {"十日", "とおか"}, {"十一日", "じゅういちにち"}, {"十二日", "じゅうににち"}, {"十三日", "じゅうさんにち"}, {"十四日", "じゅうよっか"}, {"十五日", "じゅうごにち"},
	{"十六日", "じゅうろくにち"}, {"十七日", "じゅうななにち"}, {"十八日", "じゅうはちにち"}, {"十九日", "じゅうきゅうにち"}, {"二十日", "はつか"}, {"二十一日", "にじゅういちにち"},
	{"二十二日", "にじゅうににち"}, {"二十三日", "にじゅうさんにち"}, {"二十四日", "にじゅうよっか"}, {"二十五日", "にじゅうごにち"}, { "二十六日", "にじゅうろくにち"},
	{"二十七日","にじゅうななにち"}, {"二十八日", "にじゅうはちにち"}, {"二十九日", "にじゅうきゅうにち"}, {"三十日", "さんじゅうにち"}, {"三十一日", "さんじゅういちにち"} };
    for (short i = 0; i < 31; i++) {
      vec.push_back(calendario[i]);
    }
  }
  shuffle<Number>(&vec);
  return vec;
}

Glib::ustring TestInserimentoNumeri::to_hiragana(int val) {
  Glib::ustring hira;
  Glib::ustring cifre { Glib::ustring::format(val) };
  Glib::ustring::size_type nc { cifre.size() };
  if (nc > 0) {
    int n = 0;
    do {
      switch (nc - n - 1) {
      case 0: // unita'
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring( "いち"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("に"));
	} else if (cifre[n] == '3') {
	  hira.append (Glib::ustring("さん"));
	} else if (cifre[n] == '4') {
	  hira.append (Glib::ustring("よん"));
	} else if (cifre[n] == '5') {
	  hira.append (Glib::ustring("ご"));
	} else if (cifre[n] == '6') {
	  hira.append (Glib::ustring("ろく"));
	} else if (cifre[n] == '7') {
	  hira.append (Glib::ustring("なな"));
	} else if (cifre[n] == '8') {
	  hira.append (Glib::ustring("はち"));
	} else if (cifre[n] == '9') {
	  hira.append (Glib::ustring("きゅう"));
	}
	break;
      case 1: // decine
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring("じゅう"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("にじゅう"));
	} else if (cifre[n] == '3') {
	  hira.append(Glib::ustring("さんじゅう"));
	} else if (cifre[n] == '4') {
	  hira.append(Glib::ustring("よんじゅう"));
	} else if (cifre[n] == '5') {
	  hira.append(Glib::ustring("ごじゅう"));
	} else if (cifre[n] == '6') {
	  hira.append(Glib::ustring("ろくじゅう"));
	} else if (cifre[n] == '7') {
	  hira.append(Glib::ustring("ななじゅう"));
	} else if (cifre[n] == '8') {
	  hira.append(Glib::ustring("はちじゅう"));
	} else if (cifre[n] == '9') {
	  hira.append(Glib::ustring("きゅうじゅう"));
	}
	break;
      case 2: // centinaia
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring("ひゃく"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("にひゃく"));
	} else if (cifre[n] == '3') {
	  hira.append(Glib::ustring("さんびゃく"));
	} else if (cifre[n] == '4') {
	  hira.append(Glib::ustring("よんひゃく"));
	} else if (cifre[n] == '5') {
	  hira.append(Glib::ustring("ごひゃく"));
	} else if (cifre[n] == '6') {
	  hira.append(Glib::ustring("ろっぴゃく"));
	} else if (cifre[n] == '7') {
	  hira.append(Glib::ustring("ななひゃく"));
	} else if (cifre[n] == '8') {
	  hira.append(Glib::ustring("はっぴゃく"));
	} else if (cifre[n] == '9') {
	  hira.append(Glib::ustring("きゅうひゃく"));
	}
	break;
      case 3: // migliaia
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring("せん"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("にせん"));
	} else if (cifre[n] == '3') {
	  hira.append(Glib::ustring("さんぜん"));
	} else if (cifre[n] == '4') {
	  hira.append(Glib::ustring("よんせん"));
	} else if (cifre[n] == '5') {
	  hira.append(Glib::ustring("ごせん"));
	} else if (cifre[n] == '6') {
	  hira.append(Glib::ustring("ろくせん"));
	} else if (cifre[n] == '7') {
	  hira.append(Glib::ustring("ななせん"));
	} else if (cifre[n] == '8') {
	  hira.append(Glib::ustring("はっせん"));
	} else if (cifre[n] == '9') {
	  hira.append(Glib::ustring("きゅうせん"));
	}
	break;
      case 4: // decine di migliaia
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring("ばん"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("にばん"));
	} else if (cifre[n] == '3') {
	  hira.append(Glib::ustring("さんばん"));
	} else if (cifre[n] == '4') {
	  hira.append(Glib::ustring("よんばん"));
	} else if (cifre[n] == '5') {
	  hira.append(Glib::ustring("ごばん"));
	} else if (cifre[n] == '6') {
	  hira.append(Glib::ustring("ろくばん"));
	} else if (cifre[n] == '7') {
	  hira.append(Glib::ustring("ななばん"));
	} else if (cifre[n] == '8') {
	  hira.append(Glib::ustring("はちばん"));
	} else if (cifre[n] == '9') {
	  hira.append(Glib::ustring("きゅうばん"));
	}
	break;
      case 5: // centinaia di migliaia
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring("じゅう"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("にじゅう"));
	} else if (cifre[n] == '3') {
	  hira.append(Glib::ustring("さんじゅう"));
	} else if (cifre[n] == '4') {
	  hira.append(Glib::ustring("よんじゅう"));
	} else if (cifre[n] == '5') {
	  hira.append(Glib::ustring("ごじゅう"));
	} else if (cifre[n] == '6') {
	  hira.append(Glib::ustring("ろくじゅう"));
	} else if (cifre[n] == '7') {
	  hira.append(Glib::ustring("ななじゅう"));
	} else if (cifre[n] == '8') {
	  hira.append(Glib::ustring("はちじゅう"));
	} else if (cifre[n] == '9') {
	  hira.append(Glib::ustring("きゅうじゅう"));
	}
	break;
      case 6: // milioni
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring("ひゃく"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("にひゃく"));
	} else if (cifre[n] == '3') {
	  hira.append(Glib::ustring("さんびゃく"));
	} else if (cifre[n] == '4') {
	  hira.append(Glib::ustring("よんひゃく"));
	} else if (cifre[n] == '5') {
	  hira.append(Glib::ustring("ごひゃく"));
	} else if (cifre[n] == '6') {
	  hira.append(Glib::ustring("ろっぴゃく"));
	} else if (cifre[n] == '7') {
	  hira.append(Glib::ustring("ななひゃく"));
	} else if (cifre[n] == '8') {
	  hira.append(Glib::ustring("はっぴゃく"));
	} else if (cifre[n] == '9') {
	  hira.append(Glib::ustring("きゅうひゃく"));
	}
	break;
      case 7: // decine di milioni
	if (cifre[n] == '1') {
	  hira.append(Glib::ustring("せん"));
	} else if (cifre[n] == '2') {
	  hira.append(Glib::ustring("にせん"));
	} else if (cifre[n] == '3') {
	  hira.append(Glib::ustring("さんぜん"));
	} else if (cifre[n] == '4') {
	  hira.append(Glib::ustring("よんせん"));
	} else if (cifre[n] == '5') {
	  hira.append(Glib::ustring("ごせん"));
	} else if (cifre[n] == '6') {
	  hira.append(Glib::ustring("ろくせん"));
	} else if (cifre[n] == '7') {
	  hira.append(Glib::ustring("ななせん"));
	} else if (cifre[n] == '8') {
	  hira.append(Glib::ustring("はっせん"));
	} else if (cifre[n] == '9') {
	  hira.append(Glib::ustring("きゅうせん"));
	}
	break;
      }
      n++;
    } while (n < nc);
    if (val == 0)
      hira.append(Glib::ustring("ぜろ"));
  }
  return hira;
}
