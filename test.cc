/*
 * test.cc (ex voce.cc)
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "test.hh"

std::string Test::sqlite_test_name { "TEST" };
std::string Test::sqlite_test_table { "(IDV INTEGER PRIMARY KEY NOT NULL, " \
    "TDATE INTEGER);" };
std::string Test::sqlite_record_name { "RECORD" };
std::string Test::sqlite_record_table { "(IDR INTEGER PRIMARY KEY NOT NULL, " \
    "RIFFT INTEGER NOT NULL, "						\
    "RTYPE TEXT NOT NULL, "						\
    "CORRECT INTEGER NOT NULL, "					\
    "TOTAL INTEGER NOT NULL, "						\
    "DATE INTEGER NOT NULL, "						\
    "ERROR INTEGER NOT NULL, "					       	\
    "FOREIGN KEY(RIFFT) REFERENCES TEST(IDV));" };

int Test::retrieve_test(void *data, int argc, char **argv, char **azColName) {
  Test tst;
  Test* ptst = &tst;
  Record rec;
  Glib::ustring rtype;
  std::vector<Test>* items { static_cast<std::vector<Test>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("IDV") && argv[i]) {
      tst.set_idv(argv[i]);
      if (items->size() > 0 && tst.get_idv() == items->rbegin()->get_idv()) {
	ptst = &*(items->rbegin());
      }
    } else if (std::string(azColName[i]) == std::string("TDATE") && argv[i]) {
      ptst->set_date(atol(argv[i]));
    } else if (std::string(azColName[i]) == std::string("RTYPE") && argv[i]) {
      rtype.assign(argv[i]);
    } else if (std::string(azColName[i]) == std::string("CORRECT") && argv[i]) {
      rec.correct = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("TOTAL") && argv[i]) {
      rec.total = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("DATE") && argv[i]) {
      rec.date = atol(argv[i]);
    } else if (std::string(azColName[i]) == std::string("ERROR") && argv[i]) {
      rec.error = atoi(argv[i]);
      ptst->set_record(Test::get_test_type(rtype), rec);
    }
  }
  if (ptst == &tst) {
    items->push_back(tst);
  }
  return 0;
}

int Test::retrieve_record(void *data, int argc, char **argv, char **azColName) {
  Record rec;
  Glib::ustring rtype;
  std::map<TestType,Record>* stats { static_cast<std::map<TestType,Record>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("RTYPE") && argv[i]) {
      rtype.assign(argv[i]);
    } else if (std::string(azColName[i]) == std::string("CORRECT") && argv[i]) {
      rec.correct = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("TOTAL") && argv[i]) {
      rec.total = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("DATE") && argv[i]) {
      rec.date = atol(argv[i]);
    } else if (std::string(azColName[i]) == std::string("ERROR") && argv[i]) {
      rec.error = atoi(argv[i]);
    }
  }
  stats->insert(std::pair<TestType,Record>(get_test_type(rtype), rec));
  return 0;
}

Glib::ustring Test::get_date_string(time_t raw) {
  struct tm* data_ora = localtime(&raw);
  return Glib::ustring::compose("%1:%2:%3 %4/%5/%6", Glib::ustring::format(std::setfill(L'0'), std::setw(2), data_ora->tm_hour), Glib::ustring::format(std::setfill(L'0'), std::setw(2), data_ora->tm_min), Glib::ustring::format(std::setfill(L'0'), std::setw(2), data_ora->tm_sec), Glib::ustring::format(std::setfill(L'0'), std::setw(2), data_ora->tm_mday), Glib::ustring::format(std::setfill(L'0'), std::setw(2), data_ora->tm_mon + 1), Glib::ustring::format(std::setfill(L'0'), std::setw(4), data_ora->tm_year + 1900));
}

Glib::ustring Test::get_test_type_id(TestType tt) {
  switch (tt) {
  case TestType::mem_wrd_sel:
    return Glib::ustring(MEM_WS);
  case TestType::cnj_sel:
    return Glib::ustring(CNJ_S);
  case TestType::cnj_ins:
    return Glib::ustring(CNJ_I);
  case TestType::read_wrd_sel:
    return Glib::ustring(READ_WS);
  case TestType::read_wrd_ins:
    return Glib::ustring(READ_WI);
  case TestType::mem_knj_sel:
    return Glib::ustring(MEM_KS);
  }
  return std::string();
}

TestType Test::get_test_type(const Glib::ustring& str) {
  if (str.compare(MEM_WS) == 0) {
    return TestType::mem_wrd_sel;
  } else if (str.compare(CNJ_S) == 0) {
    return TestType::cnj_sel;
  } else if (str.compare(CNJ_I) == 0) {
    return TestType::cnj_ins;
  } else if (str.compare(READ_WS) == 0) {
    return TestType::read_wrd_sel;
  } else if (str.compare(READ_WI) == 0) {
    return TestType::read_wrd_ins;
  } else if (str.compare(MEM_KS) == 0) {
    return TestType::mem_knj_sel;
  }
  return TestType::null;
}

Glib::ustring Test::get_test_type(TestType tt) {
  switch (tt) {
  case TestType::mem_wrd_sel:
    return Glib::ustring("memorizzazione parole (risposta multipla)");
  case TestType::cnj_sel:
    return Glib::ustring("coniugazione (risposta multipla)");
  case TestType::cnj_ins:
    return Glib::ustring("coniugazione (inserimento)");
  case TestType::read_wrd_sel:
    return Glib::ustring("lettura parole (risposta multipla)");
  case TestType::read_wrd_ins:
    return Glib::ustring("lettura parole (inserimento)");
  case TestType::mem_knj_sel:
    return Glib::ustring("memorizzazione kanji (risposta multipla)");
  }
  return std::string();
}

short Test::get_test_index(TestType tt) {
  switch (tt) {
  case TestType::mem_wrd_sel:
    return 0;
  case TestType::cnj_sel:
    return 1;
  case TestType::cnj_ins:
    return 2;
  case TestType::read_wrd_sel:
    return 3;
  case TestType::read_wrd_ins:
    return 4;
  case TestType::mem_knj_sel:
    return 5;
  }
  return -1;
}

Test::Test(long long int id, const std::map<TestType,Record>& stats) : idv (id), creation (time(nullptr)) {
  set_statistics(stats);
}

Test::Test(const Test& tst) {
  idv = tst.idv;
  creation = tst.creation;
  statistics = tst.statistics;
}

Test& Test::operator=(const Test &tst) {
  if (this != &tst) {
    idv = tst.idv;
    creation = tst.creation;
    statistics = tst.statistics;
  }
  return *this;
}

void Test::set_record(const TestType& index, const Record& rec) {
  std::pair<std::map<TestType,Record>::iterator,bool> ret { statistics.insert(std::pair<TestType,Record>(index, rec)) };
  if (!ret.second) {
    statistics[index] = rec;
  }
}

Record Test::get_record(TestType index) const {
  try {
    return statistics.at(index);
  } catch (const std::out_of_range& oor) {
    return { 0, 0, 0, false };
  }
}

Record Test::test(TestType tt, bool result) {
  Record rec = get_record(tt);
  rec.total++;
  if (result) {
    rec.correct++;
  }
  rec.error = !result;
  rec.date = time(nullptr);
  set_record(tt, rec);
  return rec;
}
