/*
 * test_controller.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TESTCONTROLLER_HH_
#define _TESTCONTROLLER_HH_

#include <gtkmm.h>
#include "ctrl_engine.hh"
#include "database.hh"
#include "parole_tmcr.hh"
#include "kanji_tmcr.hh"
#include "lezioni_tmcr.hh"
#include "test_engine.hh"
#include "drawing_kanji_sheet.hh"
#include "print_kanji_table_operation.hh"

#define PAGE_WORD  0
#define PAGE_KANJI 1
#define PAGE_NUMBERS 2

class TestController : public CtrlEngine {
public:
  inline TestController() {}
  TestController(Glib::RefPtr<Gtk::Builder> bldr);
  inline ~TestController() {}
  inline void set_database(Database* db) { database = db; }
  inline void present_window(void) { main_win->present(); }
  void init_window(void);
protected:
  static Glib::ustring get_estimate_time_string(int ni);
  void update_statusbar(int ni);
  void connect_signals(void);
  void on_main_page_switch(Gtk::Widget* page, guint page_num);
  bool on_main_win_focus_in(GdkEventFocus* event);
  bool on_main_win_focus_out(GdkEventFocus* event);
  void on_window_closed(void);
  void load_lezioni_combobox(void);
  void on_start_clicked(void);
  // page parole
  void load_parole_in_treeview(void);
  Glib::ustring get_parole_sqlite_string(void);
  void on_parole_test_ttype_changed(void);
  void on_parole_objective_changed(void);
  void on_conjugation_type_radiobutton_toggled(Gtk::RadioButton* button);
  void on_polite_radiobutton_toggled(Gtk::RadioButton* button);
  void on_negative_radiobutton_toggled(Gtk::RadioButton* button);
  void on_parole_conjugation_changed(void);
  void on_parole_enable_lezione(void);
  void on_parole_type_changed(void);
  void on_parole_enable_stats(void);
  // page kanji
  void load_kanji_in_treeview(void);
  Glib::ustring get_kanji_sqlite_string(void);
  void on_kanji_mode_radiobutton_toggled(Gtk::RadioButton* button);
  void on_kanji_enable_lezione(void);
  void on_kanji_enable_stats(void);
  void on_kanji_kaku_clicked(Gtk::Button* button);
  void print_or_preview(Gtk::PrintOperationAction print_action);
  void on_printOperation_status_changed(const Glib::RefPtr<PrintKanjiTableOperation>& operation);
  void on_printOperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr<PrintKanjiTableOperation>& operation);
  // page numeri
  void on_numbers_mode_radiobutton_toggled(Gtk::RadioButton* button);
  void on_numbers_totquest_changed(void);

private:
  static std::vector<Glib::ustring> parola_types;
  static std::vector<Glib::ustring> test_objectives;
  static std::vector<Glib::ustring> conjugation_strings;
  static std::vector<Glib::ustring> verso_strings;
  Glib::RefPtr<Gtk::Builder> builder;
  Database* database;
  TestOptions woptions;
  TestOptions koptions;
  TestOptions noptions;
  TestEngine* test_engine;
  sigc::connection win_closed_connection;
  // main window
  Gtk::Notebook* test_notebook;
  unsigned short main_page;
  Gtk::Statusbar* main_status;
  int stts_id;
  // toolbar
  Gtk::Toolbar* main_toolbar;
  Gtk::ToolButton* start_test_toolbutton;
  /*
   * page parole
   */
  ParoleTreeModelColumnRecord parole_tmcr;
  Glib::RefPtr<Gtk::ListStore> parole_liststore;
  Gtk::TreeView* parole_treeview;
  // filtro parole
  Gtk::ComboBox* test_parole_type_combobox;
  sigc::connection test_parole_type_connection;
  Glib::RefPtr<Gtk::ListStore> test_parole_type_liststore;
  short test_wtype;
  Gtk::ComboBox* test_parole_date_combobox;
  Glib::RefPtr<Gtk::ListStore> test_date_liststore;
  Gtk::CheckButton* test_parole_lezione_checkbutton;
  LezioniTreeModelColumnRecord lezioni_tmcr;
  Gtk::ComboBox* test_parole_lezione_combobox;
  Glib::RefPtr<Gtk::ListStore> test_parole_lezione_liststore;
  sigc::connection test_parole_lezione_connection;
  Gtk::CheckButton* test_parole_stats_checkbutton;
  Gtk::Scale* test_parole_stats_scale;
  // test
  Gtk::ComboBox* test_parole_ttype_combobox;
  Glib::RefPtr<Gtk::ListStore> test_parole_ttype_liststore;
  Gtk::ComboBox* test_parole_objective_combobox;
  Glib::RefPtr<Gtk::ListStore> test_parole_objective_liststore;
  sigc::connection test_parole_objective_connection;
  Gtk::ComboBox* test_parole_verso_combobox;
  Glib::RefPtr<Gtk::ListStore> test_parole_verso_liststore;
  Gtk::ComboBox* test_parole_conjugation_combobox;
  Glib::RefPtr<Gtk::ListStore> test_parole_conjugation_liststore;
  sigc::connection test_parole_conjugation_connection;
  // opzioni
  Gtk::Frame* test_parole_options_frame;
  Gtk::RadioButton* test_negative_radiobutton1;
  Gtk::RadioButton* test_negative_radiobutton2;
  Gtk::RadioButton* test_negative_radiobutton3;
  Gtk::RadioButton* test_style_radiobutton1;
  Gtk::RadioButton* test_style_radiobutton2;
  Gtk::RadioButton* test_style_radiobutton3;
  Gtk::RadioButton* test_parole_type_radiobutton1;
  Gtk::RadioButton* test_parole_type_radiobutton2;
  /*
   * page kanji
   */
  KanjiTreeModelColumnRecord kanji_tmcr;
  Glib::RefPtr<Gtk::ListStore> kanji_liststore;
  Gtk::TreeView* kanji_treeview;
  Gtk::RadioButton* test_kanji_memo_radiobutton;
  Gtk::RadioButton* test_kanji_reading_radiobutton;
  Gtk::ComboBox* test_kanji_verso_combobox;
  Glib::RefPtr<Gtk::ListStore> test_kanji_verso_liststore;
  Gtk::ComboBox* test_kanji_lezione_combobox;
  sigc::connection test_kanji_lezione_connection;
  Glib::RefPtr<Gtk::ListStore> test_kanji_lezione_liststore;
  Gtk::CheckButton* test_kanji_lezione_checkbutton;
  Gtk::ComboBox* test_kanji_date_combobox;
  Gtk::CheckButton* test_kanji_stats_checkbutton;
  Gtk::Scale* test_kanji_stats_scale;
  Gtk::Button* kakikata_button;
  Gtk::Button* oboeteiru_button;
  DrawingKanjiSheet* kanji_sheet;
  Glib::RefPtr<Gtk::PageSetup> m_ref_pagesetup;
  Glib::RefPtr<Gtk::PrintSettings> m_ref_printsettings;
  // page numeri
  Gtk::RadioButton* test_numbers_mode_radiobutton1;
  Gtk::RadioButton* test_numbers_mode_radiobutton2;
  Gtk::RadioButton* test_numbers_mode_radiobutton3;
  Gtk::ComboBox* test_numbers_verso_combobox;
  Glib::RefPtr<Gtk::ListStore> test_numbers_verso_liststore;
  Gtk::Entry* test_numbers_totquest_entry;
  Gtk::ComboBox* test_numbers_maxvalue_combobox;
  Glib::RefPtr<Gtk::ListStore> test_numbers_maxvalue_liststore;

};

#endif // _TESTCONTROLLER_HH_
