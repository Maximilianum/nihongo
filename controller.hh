/*
 * controller.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#define APP_FILENAME "nihongo"
#define APP_NAME "Nihongo"
#define VERSION "1.9.7"
#define BUILD_NUMBER "326"
#define COPYRIGHT "Copyright © 2011 - 2021 Massimiliano Maniscalco"
#define APP_WEBSITE "https://gitlab.com/Maximilianum/nihongo"

#define PAGE_WORD    0
#define PAGE_KANJI   1
#define PAGE_PATTERN 2
#define PAGE_LESSON  3

#include <gtkmm.h>
#include <giomm/file.h>
#include <iostream>
#include <fstream>
#include "ctrl_app_engine.hh"
#include "database.hh"
#include "stats_tmcr.hh"
#include "parole_tmcr.hh"
#include "kanji_tmcr.hh"
#include "forme_tmcr.hh"
#include "lezioni_tmcr.hh"
#include "test_controller.hh"

class Controller : public CtrlAppEngine {
public:
  inline Controller() { app_status = APP_STATUS::idle; }
  Controller(Glib::RefPtr<Gtk::Builder> bldr, Glib::RefPtr<Gtk::Application> app, const Glib::ustring& main_path);
  ~Controller();
protected:
  void connect_signals(void);
  // main win
  bool on_main_win_focus_in(GdkEventFocus *event);
  bool on_main_win_focus_out(GdkEventFocus *event);
  void on_main_page_switch(Gtk::Widget *page, guint page_num);
  void on_main_treeselection_changed(void);
  // main toolbar and menu
  void on_add_action(void);
  void on_remove_action(void);
  void on_test_action(void);
  void on_info_action(void);
  void on_reset_action(void);
  void on_search_changed(void);
  Glib::ustring get_search_sqlite(void);
  void on_increase_font_size(void);
  void on_decrease_font_size(void);
  void show_settings(void);
  // info window
  void load_stats_in_treeview(const std::map<TestType,Record>& stats);
  void populate_parola_info(const Parola& par);
  void populate_kanji_info(const Kanji& knj);
  void populate_forma_info(const Forma& frm);
  void populate_lezione_info(const Lezione& lsn);
  // page parole
  void load_parole_in_treeview(void);
  void parole_remove_selected_rows(const std::vector<Gtk::TreePath>& sel);
  void on_parole_tree_double_clicked(GdkEventButton *event);
  void parole_reset_selected_rows(const std::vector<Gtk::TreePath>& sel);
  // add parola win
  void on_add_parola_confirm_button(void);
  // page kanji
  void load_kanji_in_treeview(void);
  void kanji_remove_selected_rows(const std::vector<Gtk::TreePath>& sel);
  void on_kanji_tree_double_clicked(GdkEventButton *event);
  void kanji_reset_selected_rows(const std::vector<Gtk::TreePath>& sel);
  // add kanji win
  void on_add_kanji_confirm_button(void);
  // page forme
  void load_forme_in_treeview(void);
  void forme_treeview_double_click(GdkEventButton *event);
  void forme_remove_selected_rows(const std::vector<Gtk::TreePath>& sel);
  // add forma win
  void on_add_forma_confirm_button(void);
  // page lezioni
  void load_lezioni_in_treeview(void);
  void on_add_lezione(void);
  void on_lezione_name_edited(const Glib::ustring& path, const Glib::ustring& new_text);
  void lezioni_remove_selected_rows(const std::vector<Gtk::TreePath>& sel);
  void on_add_to_lectio(void);
  // settings window
  void on_treeview_fontsize_activate(void);
  void update_pango_attributes(void);
	
private:
  Database* database;
  Gtk::Notebook* main_notebook;
  Gtk::Statusbar* main_status;
  unsigned short main_page;
  int stsi_sel_id;
  int stsi_disk_id;
  double font_size;
  double font_scale;
  // main toolbar
  Gtk::Toolbar* main_toolbar;
  Gtk::ToolButton* add_toolbutton;
  Gtk::ToolButton* rem_toolbutton;
  Gtk::ToolButton* info_toolbutton;
  Gtk::ToolButton* reset_toolbutton;
  Gtk::ToolButton* test_toolbutton;
  Gtk::SearchEntry* search_toolitem;
  Gtk::ToolItem* lectio_toolitem;
  Gtk::ComboBox* lectio_combobox;
  Glib::RefPtr<Gtk::ListStore> lectio_liststore;
  sigc::connection lectio_connection;
  // main menu
  Gtk::MenuBar* main_menu;
  Gtk::MenuItem* quit_menuitem;
  Gtk::MenuItem* add_menuitem;
  Gtk::MenuItem* rem_menuitem;
  Gtk::MenuItem* lectio_menuitem;
  Gtk::MenuItem* reset_menuitem;
  Gtk::MenuItem* info_menuitem;
  Gtk::MenuItem* font_size_up_menuitem;
  Gtk::MenuItem* font_size_down_menuitem;
  Gtk::MenuItem* show_settings_menuitem;
  Gtk::MenuItem* about_menuitem;
  // info window
  Gtk::Window* info_window;
  Gtk::Label* info_voce_id_label;
  Gtk::Label* info_voce_frame_label;
  Gtk::Label* info_voce_label;
  Gtk::Label* info_date_label;
  Gtk::Notebook* info_notebook;
  Gtk::TextView* info_details_textview;
  StatsTreeModelColumnRecord stats_tmcr;
  Glib::RefPtr<Gtk::ListStore> info_stats_liststore;
  Gtk::TreeView* info_stats_treeview;
  // page parole
  ParoleTreeModelColumnRecord parole_tmcr;
  Glib::RefPtr<Gtk::ListStore> parole_liststore;
  Gtk::TreeView* parole_treeview;
  Glib::RefPtr<Gtk::TreeSelection> parole_treeselection;
  // add parola win
  Gtk::Window* parola_window;
  Glib::RefPtr<Gtk::ListStore> tipi_parola_liststore;
  Gtk::Entry* kotoba_entry;
  Gtk::Entry* furigana_entry;
  Gtk::Entry* traduzione_entry;
  Gtk::ComboBox* tipo_parola_combobox;
  Gtk::Button* parola_confirm_button;
  Gtk::Button* parola_cancel_button;
  // page kanji
  KanjiTreeModelColumnRecord kanji_tmcr;
  Glib::RefPtr<Gtk::ListStore> kanji_liststore;
  Gtk::TreeView* kanji_treeview;
  Glib::RefPtr<Gtk::TreeSelection> kanji_treeselection;
  // add kanji win
  Gtk::Window* kanji_window;
  Gtk::Entry* kanji_entry;
  Gtk::Entry* radicale_entry;
  Gtk::Entry* tratti_entry;
  Gtk::Entry* trattir_entry;
  Gtk::Entry* kunyomi_entry;
  Gtk::Entry* onyomi_entry;
  Gtk::Entry* significato_entry;
  Gtk::Button* kanji_cancel_button;
  Gtk::Button* kanji_confirm_button;
  // page forme
  FormeTreeModelColumnRecord forme_tmcr;
  Glib::RefPtr<Gtk::ListStore> forme_liststore;
  Gtk::TreeView* forme_treeview;
  Glib::RefPtr<Gtk::TreeSelection> forme_treeselection;
  // add forma
  Gtk::Window* form_window;
  Gtk::Entry* form_name_entry;
  Gtk::Entry* form_pattern_entry;
  Gtk::Entry* form_note_entry;
  Gtk::Button* form_cancel_button;
  Gtk::Button* form_confirm_button;
  // page lezioni
  LezioniTreeModelColumnRecord lezioni_tmcr;
  Glib::RefPtr<Gtk::TreeStore> lezioni_treestore;
  Gtk::TreeView* lezioni_treeview;
  Glib::RefPtr<Gtk::TreeSelection> lezioni_treeselection;
  Gtk::TreeViewColumn* lezioni_name_treeviewcolumn;
  Gtk::CellRendererText* lezioni_name_cellrenderertext;
  // settings window
  Gtk::Window* settings_window;
  Gtk::Entry* treeview_fontsize_entry;
  Gtk::Entry* gui_scale_entry;
  // test
  TestController *test_ctrl;
};

#endif // _CONTROLLER_H_
