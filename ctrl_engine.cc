/*
 * ctrl_engine.cc
 * Copyright (C) 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctrl_engine.hh"

/**
 * general function to load text data from a vector in a combobox liststore
 */

void CtrlEngine::load_data_combobox(std::vector<Glib::ustring>::iterator start, std::vector<Glib::ustring>::iterator end, Glib::RefPtr<Gtk::ListStore> liststore) {
  liststore->clear();
  while (start != end) {
    Gtk::TreeModel::iterator iter = liststore->append();
    Gtk::TreeModel::Row row = *iter;
    row[combobox_tmcr.column] = *start;
    start++;
  }
}

int CtrlEngine::get_row_index_for_value_in_combobox(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring &value) {
  int index = 0;
  Gtk::TreeModel::iterator iter = liststore->children().begin();
  Gtk::TreeModel::iterator end = liststore->children().end();
  while (iter != end)	{
    Gtk::TreeModel::Row row = *iter;
    if (row[combobox_tmcr.column] == value) {
      return index;
    }
    index++;
    iter++;
  }
  return -1;
}

Gtk::TreeModel::iterator CtrlEngine::get_combobox_item(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring& str) {
  Gtk::TreeModel::iterator iter = liststore->children().begin();
  Gtk::TreeModel::iterator end = liststore->children().end();
  while (iter != end)	{
    Gtk::TreeModel::Row row = *iter;
    if (row[combobox_tmcr.column] == str) {
      return iter;
    }
    iter++;
  }
  return iter;
}

/**
 * TreeView utility function
 **/

std::vector<Gtk::TreeRowReference> CtrlEngine::get_treeview_row_ref(const std::vector<Gtk::TreePath> &sel, Glib::RefPtr<Gtk::TreeModel> model) {
  std::vector<Gtk::TreeRowReference> sel_ref;
  std::vector<Gtk::TreePath>::const_iterator iter = sel.cbegin();
  while (iter != sel.cend())	{
    Gtk::TreeRowReference ref = Gtk::TreeRowReference(model, *iter);
    sel_ref.push_back(ref);
    iter++;
  }
  return sel_ref;
}

