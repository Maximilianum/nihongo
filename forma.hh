/*
 * forma.hh
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FORMA_HH_
#define _FORMA_HH_

#include <vector>
#include "test.hh"
#include "sintagma.hh"

class Forma: public Test {
public:
  static inline std::string get_sqlite_pattern_name(void) { return sqlite_pattern_name; }
  static inline std::string get_sqlite_pattern_table(void) { return sqlite_pattern_table; }
  static int retrieve_forma(void *data, int argc, char **argv, char **azColName);
  static std::vector<Sintagma> parse_tokens(const std::string& str);
  inline Forma() {}
  Forma(const Forma &frm);
  inline Forma(const Glib::ustring& nm, const Glib::ustring& pt, const Glib::ustring& nt) : name (nm), pattern (parse_tokens(pt)), note (nt) {}
  inline Forma(long long int id, const Glib::ustring& nm, const Glib::ustring& pt, const Glib::ustring& nt) : idf (id), name (nm), pattern (parse_tokens(pt)), note (nt) {}
  inline ~Forma () {}
  Forma& operator=(const Forma& pat);
  inline bool operator<(const Forma &pat) const { return idf < pat.idf; }
  bool operator==(const Forma &pat) const;
  bool operator!=(const Forma &pat) const;
  inline void set_idf(long long int id) { idf = id; }
  inline void set_idf(const char* cstr) { idf = atoll(cstr); }
  inline long long int get_idf(void) const { return idf; }
  inline void set_name(const Glib::ustring& str) { name.assign(str); }
  inline void set_name(const char* cstr) { name.assign(cstr); }
  inline Glib::ustring get_name(void) const { return name; }
  inline void set_pattern(const std::vector<Sintagma>& vec) { pattern = vec; }
  inline void set_pattern(const Glib::ustring& str) { pattern = parse_tokens(str.raw()); }
  inline void set_pattern(const char* cstr) { pattern = parse_tokens(std::string(cstr)); }
  inline std::vector<Sintagma> get_pattern(void) const { return pattern; }
  std::string get_pattern_string(void) const;
  Glib::ustring print_pattern(void) const;
  inline void set_note(const Glib::ustring& str) { note.assign(str); }
  inline void set_note(const char* cstr) { note.assign(cstr); }
  inline Glib::ustring get_note(void) const { return note; }
private:
  static std::string sqlite_pattern_name;
  static std::string sqlite_pattern_table;
  long long int idf;
  Glib::ustring name;
  std::vector<Sintagma> pattern;
  Glib::ustring note;
};

#endif // _FORMA_HH_
