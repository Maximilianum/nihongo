/*
 * test_scelta_mult_parole.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_scelta_mult_parole.hh"
#include <chrono>
#include <random>

TestSceltaMultParole::TestSceltaMultParole(Glib::RefPtr<Gtk::Builder> bldr, Database* db, const std::vector<long long int>& vec) : database (db), content (vec) {
  shuffle<long long int>(&content);
  current = content.begin();
  init_test_scelta_mult(bldr);
  curr_vconj = options.vconj;
  curr_aconj = options.aconj;
  connect_signals();
}

void TestSceltaMultParole::run_test(const TestOptions& opts) {
  question_tot_label->set_text(Glib::ustring::format(content.size()));
  main_win->present();
  timer = 0;
  nr_quest = 0;
  options = opts;
  update_window();
}

AjConjug TestSceltaMultParole::get_random_adjective_conjugation(void) {
  int n = TestEngine::random(2, 8);
  return Parola::get_aconjugation(n);
}

VbConjug TestSceltaMultParole::get_random_verb_conjugation(void) {
  int n = TestEngine::random(2, 13);
  return Parola::get_vconjugation(n);
}

void TestSceltaMultParole::update_window(void) {
  Glib::ustring question;
  Glib::ustring upquestion;
  try {
    current_word = database->get_parola(*current);
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestSceltaMultParole::update_window() " << dbe.get_message() << std::endl;
  }
  question_nr_label->set_text(Glib::ustring::format(nr_quest + 1));
  timer_connection = Glib::signal_timeout().connect_seconds(sigc::mem_fun (*this, &TestSceltaMultParole::on_timer_fired), 1);
  time_label->set_text("00 : 00");
  if (options.tst_type == TestType::cnj_sel) {
    curr_negative = options.conj_negative >= 0 ? options.conj_negative : TestEngine::random_bool();
    curr_plain = options.conj_style >= 0 ? options.conj_style : TestEngine::random_bool();
    Glib::ustring neg_str = curr_negative ? "negativa" : "affermativa";
    Glib::ustring style_str = curr_plain ? "colloquiale" : "cortese";
    if (options.aconj == AjConjug::all) {
      curr_aconj = get_random_adjective_conjugation();
    } else if (options.vconj == VbConjug::all) {
      curr_vconj = get_random_verb_conjugation();
    }
    bool verb = current_word.is_verbo();
    Glib::ustring conjugation;
    if (verb) {
      conjugation = Parola::get_conjugation_str(curr_vconj);
      switch (curr_vconj) {
      case VbConjug::presente:
      case VbConjug::passato:
      case VbConjug::volitivo:
	conjugation += Glib::ustring(" (") + neg_str + Glib::ustring(" - ") + style_str + Glib::ustring(")");
	break;
      case VbConjug::desiderativo:
      case VbConjug::imperativo:
	conjugation += Glib::ustring(" (") + neg_str + Glib::ustring(")");
	break;
      case VbConjug::esortativo:
      case VbConjug::obbligo:
      case VbConjug::potenziale:
      case VbConjug::passivo:
      case VbConjug::causativo:
	conjugation += Glib::ustring(" (") + style_str + Glib::ustring(")");
	break;
      }
    } else {
      conjugation = Parola::get_conjugation_str(curr_aconj);
      conjugation += Glib::ustring(" (") + neg_str + Glib::ustring(" - ") + style_str + Glib::ustring(")");
    }
    if (current_word.get_furigana().compare(Glib::ustring()) == 0) {
      upquestion = Glib::ustring("\"") + conjugation + Glib::ustring("\"");
      question = current_word.get_kotoba();
    } else {
      upquestion = Glib::ustring("\"") + conjugation + Glib::ustring("\"");
      question = current_word.get_kotoba() + Glib::ustring(" (") + current_word.get_furigana() + Glib::ustring(" )");
    }
  } else {
    switch(options.tst_type) {
    case TestType::mem_wrd_sel:
      if (options.verso == JAP_ITA) {
	upquestion = current_word.get_furigana();
	question = current_word.get_kotoba();
      } else {
	question.assign(current_word.get_itariago());
      }
      break;
    case TestType::read_wrd_sel:
      question.assign(current_word.get_kotoba());
      break;
    }
  }
  upquestion_label->set_text(upquestion);
  question_label->set_text(question);
  update_answers();
}

void TestSceltaMultParole::update_answers(void) {
  short right = TestEngine::random(0, 5);
  right_answers.clear();
  right_answers.insert(right_answers.begin(), right);
  std::pair<Glib::ustring, Glib::ustring> risposte[6];
  Parola wrd;
  if (options.tst_type == TestType::cnj_sel) {
    AjConjug risposte_a[6];
    VbConjug risposte_v[6];
    bool verb = current_word.is_verbo();
    Glib::ustring rispostaEsatta;
    if (verb) {
      rispostaEsatta = current_word.get_conjugation(curr_vconj, curr_plain, curr_negative);
    } else {
      rispostaEsatta = current_word.get_conjugation(curr_aconj, curr_plain, curr_negative);
    }
    for (short ns = 0; ns < 6; ns++) {
      if (right_answers.count(ns) > 0) {
	if (verb) {
	  risposte_v[ns] = curr_vconj;
	} else {
	  risposte_a[ns] = curr_aconj;
	}
	risposte[ns].first = rispostaEsatta;
      } else {
	AjConjug ra;
	VbConjug rv;
	bool again;
	Glib::ustring stringa;
	do {
	  if (verb) {
	    rv = get_random_verb_conjugation();
	  } else {
	    ra = get_random_adjective_conjugation();
	  }
	  bool is_plain = TestEngine::random_bool();
	  bool is_negative = TestEngine::random_bool();
	  if (verb) {
	    stringa = current_word.get_conjugation(rv, is_plain, is_negative);
	  } else {
	    stringa = current_word.get_conjugation(ra, is_plain, is_negative);
	  }
	  again = false;
	  if (stringa.compare(rispostaEsatta) == 0) {
	    again = true;
	  }
	  if (!again) {
	    for (short i = 0; i < ns; i++) {
	      if (risposte[i].first.compare(stringa) == 0) {
		again = true;
		break;
	      }
	    }
	  }
	}
	while (again);
	if (verb) {
	  risposte_v[ns] = rv;
	} else {
	  risposte_a[ns] = ra;
	}
	risposte[ns].first = stringa;
      }
    }
  } else {
    std::vector<Parola> wrds;
    try {
      if (options.tst_type == TestType::read_wrd_sel) {
	wrds = database->get_parole(Glib::ustring("where WORD.YOMIKATA != ''"));
      } else {
	wrds = database->get_parole(Glib::ustring::compose("where WORD.WTYPE = %1", current_word.get_wtype()));
      }
    } catch (const Database::DBError& dbe) {
      std::cerr << "TestSceltaMultParole::update_answers() " << dbe.get_message() << std::endl;
    }
    for (short n = 0; n < 6; n++) {
      if (right_answers.count(n) > 0) {
	wrd = current_word;
      } else {
	wrd = wrds[TestEngine::random(0, wrds.size() - 1)];
	if (wrd == current_word) {
	  right_answers.insert(right_answers.begin(), n);
	}
      }
      risposte[n] = create_labels(wrd);
    }
  }
  answer1_label->set_text(risposte[0].first);
  answer1up_label->set_text(risposte[0].second);
  answer2_label->set_text(risposte[1].first);
  answer2up_label->set_text(risposte[1].second);
  answer3_label->set_text(risposte[2].first);
  answer3up_label->set_text(risposte[2].second);
  answer4_label->set_text(risposte[3].first);
  answer4up_label->set_text(risposte[3].second);
  answer5_label->set_text(risposte[4].first);
  answer5up_label->set_text(risposte[4].second);
  answer6_label->set_text(risposte[5].first);
  answer6up_label->set_text(risposte[5].second);
  risposta_esatta.assign(risposte[*(right_answers.begin())].first);
}

std::pair<Glib::ustring, Glib::ustring> TestSceltaMultParole::create_labels(const Parola &wrd) {
  switch (options.tst_type) {
  case TestType::mem_wrd_sel:
    if (options.verso == JAP_ITA) {
      return std::pair<Glib::ustring, Glib::ustring>(wrd.get_itariago(), Glib::ustring());
    } else {
      return std::pair<Glib::ustring, Glib::ustring>(wrd.get_kotoba(), wrd.get_furigana());
    }
  case TestType::read_wrd_sel:
    return std::pair<Glib::ustring, Glib::ustring>(wrd.get_furigana(), Glib::ustring());
  }
  return std::pair<Glib::ustring, Glib::ustring>(Glib::ustring(), Glib::ustring());
}

void TestSceltaMultParole::on_risposta(short tasto) {
  bool risultato = right_answers.count(tasto) > 0;
  timer_connection.disconnect();
  try {
    Record rec { current_word.test(options.tst_type, risultato) };
    database->update(rec, current_word.get_idv(), options.tst_type);
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestSceltaMultParole::on_risposta() " << dbe.get_message() << std::endl;
  }
  if (!risultato) {
    Glib::ustring msg { "Sbagliato, la risposta giusta è\n<b><big>" + risposta_esatta + "</big></b>" };
    Gtk::MessageDialog *alert { new Gtk::MessageDialog(*main_win, "", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
      alert->set_message(msg, true);
      int res_id = alert->run();
      delete alert;
  }
  next_question();
}

void TestSceltaMultParole::next_question(void) {
  timer = 0;
  nr_quest++;
  current++;
  if (current == content.end()) {
    main_win->hide();
  } else {
    update_window();
  }
}
