// parola.cc
//
// Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "parola.hh"

std::string Parola::sqlite_word_name { "WORD" };
std::string Parola::sqlite_word_table { "(IDW INTEGER PRIMARY KEY NOT NULL, " \
    "WTYPE INTEGER NOT NULL, "						\
    "KOTOBA TEXT NOT NULL, "						\
    "YOMIKATA TEXT, "							\
    "ITARIAGO TEXT NOT NULL, "						\
    "STATS INTEGER NOT NULL, "
    "FOREIGN KEY(STATS) REFERENCES TEST(IDV));" };

const Glib::ustring Parola::godan_table ( "うわいうえおくかきくけこぐがぎぐげごすさしすせそつたちつてとぬなにぬねのぶばびぶべぼむまみむめもるらりるれろ" );
const std::vector<Glib::ustring> Parola::types { "nome", "agg.vo i", "agg.vo na", "v. godan tr.", "v. godan intr.", "v. ichidan tr.", "v. ichidan intr.", "v. irregolare", "pronome pers.", "pronome dim.", "pronome int.", "avverbio", "particella" };

int Parola::retrieve_parola(void *data, int argc, char **argv, char **azColName) {
  Parola wrd;
  Parola* pwrd = &wrd;
  Record rec;
  Glib::ustring rtype;
  std::vector<Parola>* parole { static_cast<std::vector<Parola>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("IDW") && argv[i]) {
      wrd.set_idw(argv[i]);
      if (parole->size() > 0 && wrd.get_idw() == parole->rbegin()->get_idw()) {
	pwrd = &*(parole->rbegin());
      }
    } else if (std::string(azColName[i]) == std::string("WTYPE") && argv[i]) {
      pwrd->set_wtype(argv[i]);
    } else if (std::string(azColName[i]) == std::string("KOTOBA") && argv[i]) {
      pwrd->set_kotoba(argv[i]);
    } else if (std::string(azColName[i]) == std::string("YOMIKATA") && argv[i]) {
      pwrd->set_furigana(argv[i]);
    } else if (std::string(azColName[i]) == std::string("ITARIAGO") && argv[i]) {
      pwrd->set_itariago(argv[i]);
    } else if (std::string(azColName[i]) == std::string("STATS") && argv[i]) {
      pwrd->set_idv(argv[i]);
    } else if (std::string(azColName[i]) == std::string("RTYPE") && argv[i]) {
      rtype.assign(argv[i]);
    } else if (std::string(azColName[i]) == std::string("CORRECT") && argv[i]) {
      rec.correct = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("TOTAL") && argv[i]) {
      rec.total = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("DATE") && argv[i]) {
      rec.date = atol(argv[i]);
    } else if (std::string(azColName[i]) == std::string("ERROR") && argv[i]) {
      rec.error = atoi(argv[i]);
      pwrd->set_record(Test::get_test_type(rtype),rec);
    }
  }
  if (pwrd == &wrd) {
    parole->push_back(wrd);
  }
  return 0;
}

int Parola::retrieve_id(void *data, int argc, char **argv, char **azColName) {
  std::vector<long long int>* parole { static_cast<std::vector<long long int>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("IDW") && argv[i]) {
      parole->push_back(atoll(argv[i]));
    }
  }
  return 0;
}

int Parola::get_type_index_from_name(const Glib::ustring &str) {
  for (int index = 0; index < types.size(); index++) {
    if (str.compare(types[index]) == 0) {
      return index;
    }
  }
  return -1;
}

Glib::ustring Parola::get_conjugation_str(VbConjug c) {
  switch (c) {
  case VbConjug::null:
    return Glib::ustring("null");
  case VbConjug::all:
    return Glib::ustring("all");
  case VbConjug::presente:
    return Glib::ustring("presente");
  case VbConjug::passato:
    return Glib::ustring("passato");
  case VbConjug::te:
    return Glib::ustring("forma in て");
  case VbConjug::desiderativo:
    return Glib::ustring("desiderativo");
  case VbConjug::volitivo:
    return Glib::ustring("volitivo");
  case VbConjug::esortativo:
    return Glib::ustring("esortativo");
  case VbConjug::imperativo:
    return Glib::ustring("imperativo");
  case VbConjug::obbligo:
    return Glib::ustring("obbligo");
  case VbConjug::potenziale:
    return Glib::ustring("potenziale");
  case VbConjug::passivo:
    return Glib::ustring("passivo");
  case VbConjug::causativo:
    return Glib::ustring("causativo");
  case VbConjug::condizionale:
    return Glib::ustring("condizionale");
  default:
    return Glib::ustring("");
  }
}

Glib::ustring Parola::get_conjugation_str(AjConjug c) {
  switch (c) {
  case AjConjug::null:
    return Glib::ustring("null");
  case AjConjug::all:
    return Glib::ustring("all");
  case AjConjug::presente:
    return Glib::ustring("presente");
  case AjConjug::passato:
    return Glib::ustring("passato");
  case AjConjug::sospensivo:
    return Glib::ustring("sospensivo");
  case AjConjug::causale:
    return Glib::ustring("causale");
  case AjConjug::avverbiale:
    return Glib::ustring("avverbiale");
  case AjConjug::diventare:
    return Glib::ustring("diventare");
  case AjConjug::rendere:
    return Glib::ustring("rendere");
  default:
    return Glib::ustring("");
  }
}

int Parola::get_conjugation_index(VbConjug c) {
  std::vector<VbConjug> vc { VbConjug::null, VbConjug::all, VbConjug::presente, VbConjug::passato, VbConjug::te, VbConjug::desiderativo, VbConjug::volitivo, VbConjug::esortativo, VbConjug::imperativo, VbConjug::obbligo, VbConjug::potenziale, VbConjug::passivo, VbConjug::causativo, VbConjug::condizionale };
  for (int n = 0; n < vc.size(); n++) {
    if (vc[n] == c) {
      return n;
    }
  }
  return -1;
}

int Parola::get_conjugation_index(AjConjug c) {
  std::vector<AjConjug> ac { AjConjug::null, AjConjug::all, AjConjug::presente, AjConjug::passato, AjConjug::sospensivo, AjConjug::causale, AjConjug::avverbiale, AjConjug::diventare, AjConjug::rendere };
  for (int n = 0; n < ac.size(); n++) {
    if (ac[n] == c) {
      return n;
    }
  }
  return -1;
}

VbConjug Parola::get_vconjugation(int index) {
  std::vector<VbConjug> vc { VbConjug::null, VbConjug::all, VbConjug::presente, VbConjug::passato, VbConjug::te, VbConjug::desiderativo, VbConjug::volitivo, VbConjug::esortativo, VbConjug::imperativo, VbConjug::obbligo, VbConjug::potenziale, VbConjug::passivo, VbConjug::causativo, VbConjug::condizionale };
  return vc[index];
}

AjConjug Parola::get_aconjugation(int index) {
  std::vector<AjConjug> ac { AjConjug::null, AjConjug::all, AjConjug::presente, AjConjug::passato, AjConjug::sospensivo, AjConjug::causale, AjConjug::avverbiale, AjConjug::diventare, AjConjug::rendere };
  return ac[index];
}

Parola::Parola(const Parola &verbum) : idw (verbum.idw), wtype (verbum.wtype), kotoba (verbum.kotoba), furigana (verbum.furigana), itariago (verbum.itariago) {
  idv = verbum.idv;
  creation = verbum.creation;
  statistics = verbum.statistics;
}

Parola& Parola::operator=(const Parola &verbum) {
  if (this != &verbum) {
    idw = verbum.idw;
    wtype = verbum.wtype;
    kotoba = verbum.kotoba;
    furigana = verbum.furigana;
    itariago = verbum.itariago;
    idv = verbum.idv;
    creation = verbum.creation;
    statistics = verbum.statistics;
  }
  return *this;
}

bool Parola::operator<(const Parola &verbum) const {
  if (idw < verbum.idw) {
    return true;
  }
  return false;
}

bool Parola::operator==(const Parola &verbum) const {
  if (kotoba.compare(verbum.kotoba) == 0) {
    if (furigana.compare(verbum.furigana) == 0) {
      if (itariago.compare(verbum.itariago) == 0) {
	if (wtype == verbum.wtype) {
	  return true;
	}
      }
    }
  }
  return false;
}

bool Parola::operator!=(const Parola &verbum) const {
  if (kotoba.compare(verbum.kotoba) != 0 || furigana.compare(verbum.furigana) != 0 || itariago.compare(verbum.itariago) != 0 || wtype != verbum.wtype) {
    return true;
  }
  return false;
}

bool Parola::has_kanji(void) const {
  Glib::ustring::const_iterator iter = kotoba.begin();
  while (iter != kotoba.end()) {
    if (is_kanji(iter)) {
      return true;
    }
    iter++;
  }
  return false;
}

std::list<Glib::ustring> Parola::get_kanji_list(void) const {
  std::list<Glib::ustring> lista;
  Glib::ustring::const_iterator iter = kotoba.cbegin();
  while (iter != kotoba.cend()) {
    if (is_kanji(iter)) {
      lista.push_back(Glib::ustring(1, *iter));
    }
    iter++;
  }
  return lista;
}

void Parola::get_forme_list(std::list<Glib::ustring> &lista) const {
  Glib::ustring str { "" };
  switch (wtype) {
  case 1:	// nome
    lista.push_back(kotoba);
    break;
  case 2:	// aggettivo i
    lista.push_back(kotoba);
    lista.push_back(kotoba.substr(0, kotoba.size() - 1) + "く");
    lista.push_back(kotoba.substr(0, kotoba.size() - 1) + "かった");
    break;
  case 3:	// aggettivo na
    lista.push_back(kotoba + "な");
    lista.push_back(kotoba + "に");
    lista.push_back(kotoba);
    break;
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    for (char n = 1; n <= 5; n++) {
      str = get_verbo_base(n);
      lista.push_back(str);
    }
    str = get_verbo_in_te();
    lista.push_back(str);
    break;
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    lista.push_back(kotoba.substr(0, kotoba.size() - 1));
    str = get_verbo_in_te();
    lista.push_back(str);
    break;
  }
}

Glib::ustring Parola::get_conjugation(VbConjug vc, bool style, bool negative) const {
  switch (vc) {
  case VbConjug::presente:
    return get_presente(style, negative);
  case VbConjug::passato:
    return get_passato(style, negative);
  case VbConjug::te:
    return get_verbo_in_te();
  case VbConjug::desiderativo:
    return get_desiderativo(negative);
  case VbConjug::volitivo:
    return get_volitivo(style, negative);
  case VbConjug::esortativo:
    return get_esortativo(style);
  case VbConjug::imperativo:
    return get_imperativo(negative);
  case VbConjug::obbligo:
    return get_obbligo(style);
  case VbConjug::potenziale:
    return get_potenziale(style);
  case VbConjug::passivo:
    return get_passivo(style);
  case VbConjug::causativo:
    return get_causativo(style);
  case VbConjug::condizionale:
    return get_condizionale();
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_conjugation(AjConjug ac, bool style, bool negative) const {
  switch (ac) {
  case AjConjug::presente:
    return get_presente(style, negative);
  case AjConjug::passato:
    return get_passato(style, negative);
  case AjConjug::sospensivo:
    return get_sospensivo(style, negative);
  case AjConjug::causale:
    return get_causale();
  case AjConjug::avverbiale:
    return get_avverbiale();
  case AjConjug::diventare:
    return get_diventare(style, negative);
  case AjConjug::rendere:
    return get_rendere(style, negative);
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_presente(bool piana, bool negativa) const {
  switch (wtype) {
  case 2:	// aggettivo i
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くない");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くないです");
      }
    } else {
      if (piana) {
	return kotoba;
      } else {
	return kotoba + Glib::ustring("です");
      }
    }
  case 3:       // aggettivo na
    if (negativa) {
      if (piana) {
	return kotoba + Glib::ustring("ではない");
      } else {
	return kotoba + Glib::ustring("ではありません");
      }
    } else {
      if (piana) {
	return kotoba + Glib::ustring("だ");
      } else {
	return kotoba + Glib::ustring("です");
      }
    }
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    if (negativa) {
      if (piana) {
	return get_verbo_base(1) + Glib::ustring("ない");
      } else {
	return get_verbo_base(2) + Glib::ustring("ません");
      }
    } else {
      if (piana) {
	return kotoba;
      } else {
	return get_verbo_base(2) + Glib::ustring("ます");
      }
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ない");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ません");
      }
    }
    else {
      if (piana) {
	return kotoba;
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ます");
      }
    }
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_passato(bool piana, bool negativa) const {
  switch (wtype) {
  case 2:	// aggettivo i
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなかった");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなかったです");
      }
    } else {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("かった");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("かったです");
      }
    }
  case 3: // aggettivo na
    if (negativa) {
      if (piana) {
	return kotoba + Glib::ustring("ではなかった");
      } else {
	return kotoba + Glib::ustring("ではありませんでした");	
      }
    } else {
      if (piana) {
	return kotoba + Glib::ustring("だった");
      } else {
	return kotoba + Glib::ustring("でした");
      }
    }
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    if (negativa) {
      if (piana) {
	return get_verbo_base(1) + Glib::ustring("なかった");
      } else {
	return get_verbo_base(2) + Glib::ustring("ませんでした");
      }
    } else {
      if (piana) {
	return get_verbo_in_te(true);;
      } else {
	return get_verbo_base(2) + Glib::ustring("ました");
      }
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("なかった");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ませんでした");
      }
    } else {
      if (piana) {
	return get_verbo_in_te(true);;
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ました");
      }
    }
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_sospensivo(bool piana, bool negativa) const {
  switch (wtype) {
  case 2:	// aggettivo i
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなくて");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなく");	
      }
    } else {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くて");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("く");
      }
    }
  case 3: // aggettivo na
    return kotoba + Glib::ustring("で");
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    return get_verbo_base(2);
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return get_verbo_in_te();
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_causale(void) const {
  switch (wtype) {
  case 2:	// aggettivo i
    return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くて");
  case 3: // aggettivo na
    return kotoba + Glib::ustring("で");
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return get_verbo_in_te();
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_avverbiale(void) const {
  switch (wtype) {
  case 2:	// aggettivo i
    return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("く");
  case 3: // aggettivo na
    return kotoba + Glib::ustring("に");
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return get_verbo_in_te();
    break;
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_diventare(bool piana, bool negativa ) const {
  switch (wtype) {
  case 2:	// aggettivo i
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くならない");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなりません");
      }
    } else {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなる");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなります");
      }
    }
  case 3: // aggettivo na
    if (negativa) {
      if (piana) {
	return kotoba + Glib::ustring("にならない");
      } else {
	return kotoba + Glib::ustring("になりません");
      }
    } else {
      if (piana) {
	return kotoba + Glib::ustring("になる");
      } else {
	return kotoba + Glib::ustring("になります");
      }
    }
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_rendere(bool piana, bool negativa) const {
  switch (wtype) {
  case 2:	// aggettivo i
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くしない");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くしません");
      }
    } else {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くする");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くします");
      }
    }
    break;
  case 3: // aggettivo na
    if (negativa) {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("にしない");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("にしません");
      }
    } else {
      if (piana) {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("にする");
      } else {
	return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("にします");
      }
    }
    break;
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_desiderativo(bool negativa) const {
  Glib::ustring ausiliare = negativa ? Glib::ustring("たくない") : Glib::ustring("たい");
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    return get_verbo_base(2) + ausiliare;
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return kotoba.substr(0, kotoba.size() - 1) + ausiliare;
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_volitivo(bool piana, bool negativa) const {
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    if (negativa) {
      Glib::ustring ausiliare { "まい" };
      if (wtype == 8) {
	return get_verbo_base(1) + ausiliare;
      } else {
	return get_verbo_base(3) + ausiliare;
      }
    } else {
      if (piana) {
	return get_verbo_base(5) + Glib::ustring("う");
      } else {
	return get_verbo_base(2) + Glib::ustring("ましょう");
      }
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    if (negativa) {
      return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("まい");
    } else {
      return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("よう") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ましょう");
    }
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_esortativo(bool piana) const {
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    if (piana) {
      return get_verbo_base(5) + Glib::ustring("うか");
    } else {
      return get_verbo_base(2) + Glib::ustring("ましょうか");
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ようか") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("ましょうか");
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_imperativo(bool negativa) const {
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
    if (negativa) {
      return get_verbo_base(3) + Glib::ustring("な");
    } else {
      return get_verbo_base(4);
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    if (negativa) {
      return kotoba + Glib::ustring("な");
    } else {
      return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("よ");
    }
  case 8:	// verbo irregolare
    if (negativa) {
      return kotoba.compare("来る") == 0  ? Glib::ustring("来るな") : kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("するな");
    } else {
      return kotoba.compare("来る") == 0  ? Glib::ustring("来い") : kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("せよ");
    }
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_obbligo(bool piana) const {
  switch (wtype) {
  case 2:	// aggettivo i
    return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなくてわならない") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("くなくてわなりません");
  case 3: // aggettivo na
    return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring ("でなくてわならない") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("でなくてわなりません");
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
  case 8:	// verbo irregolare
    if (piana) {
      return get_verbo_base(1) + Glib::ustring("なければならない");
    } else {
      return get_verbo_base(1) + Glib::ustring("なければなりません");
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("なければならない") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("なければなりません");
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_potenziale(bool piana ) const {
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
    if (piana) {
      return get_verbo_base(4) + Glib::ustring("る");
    } else {
      return get_verbo_base(4) + Glib::ustring("ます");
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("られる") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("られます");
  case 8:	// verbo irregolare
    return kotoba.compare("来る") == 0  ? Glib::ustring("来られる") : kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("できる");
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_passivo(bool piana) const {
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
    if (piana) {
      return get_verbo_base(1) + Glib::ustring("れる");
    } else {
      return get_verbo_base(1) + Glib::ustring("れます");
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("られる") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("られます");
  case 8:	// verbo irregolare
    return kotoba.compare("来る") == 0  ? Glib::ustring("来られる") : kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("される");
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_causativo(bool piana ) const {
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
    if (piana) {
      return get_verbo_base(1) + Glib::ustring("せる");
    } else {
      return get_verbo_base(1) + Glib::ustring("せます");
    }
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return piana ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("させる") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("させます");
  case 8:	// verbo irregolare
    return kotoba.compare("来る") == 0 ? Glib::ustring("来させる") : kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("させる");
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_condizionale(void) const {
  switch (wtype) {
  case 4:	// verbo godan transitivo
  case 5:	// verbo godan intransitivo
    return get_verbo_base(4) + Glib::ustring("ば");
  case 6:	// verbo ichidan transitivo
  case 7:	// verbo ichidan intransitivo
    return kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("れば");
  case 8:	// verbo irregolare
    return kotoba.compare("来る") == 0 ? Glib::ustring("来れば") : kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("すれば");
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_verbo_base(short index) const {
  Glib::ustring::size_type pos;
  switch (wtype) {
  case 4:
  case 5:
    pos = godan_table.find(kotoba.substr(kotoba.size() - 1));
    if (pos != Glib::ustring::npos) {
      return kotoba.substr(0, kotoba.size() - 1) + godan_table[pos + index];
    }
    break;
  case 8:
    if (kotoba.compare(Glib::ustring("来る")) == 0) {
      return Glib::ustring("来");
    } else {
      switch(index) {
      case 1:
      case 2:
	return kotoba.substr(0, kotoba.size() - 2) + "し";
      case 3:
	return kotoba;
      case 4:
	return kotoba.substr(0, kotoba.size() - 2) + "すれ";
      case 5:
	return kotoba.substr(0, kotoba.size() - 2) + "しろ";
      }
    }
  }
  return Glib::ustring();
}

Glib::ustring Parola::get_verbo_in_te(bool ta) const {
  Glib::ustring suffisso = kotoba.substr(kotoba.size() - 1);
  switch (wtype) {
  case 4:
  case 5:
    if (suffisso.compare("う") == 0 || suffisso.compare("つ") == 0 || suffisso.compare("る") == 0) {
      return ta ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("った") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("って");
    }
    if (suffisso.compare("む") == 0 || suffisso.compare("ぶ") == 0 || suffisso.compare("ぬ") == 0 ) {
      return ta ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("んだ") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("んで");
    }
    if (suffisso.compare("く") == 0) {
      return ta ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("いた") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("いて");
    }
    if (kotoba.compare("行く") == 0 ) {
      return ta ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("った") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("って");
    }
    if (suffisso.compare("ぐ") == 0) {
      return ta ? kotoba.substr( 0, kotoba.size() - 1) + Glib::ustring("いだ") : kotoba.substr( 0, kotoba.size() - 1) + Glib::ustring("いで");
    }
  if (suffisso.compare("す") == 0) {
    return ta ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("した") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("して");
  }
  break;
  case 6:
  case 7:
    return ta ? kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("た") : kotoba.substr(0, kotoba.size() - 1) + Glib::ustring("て");
  case 8:
    if (kotoba.compare("来る") == 0) {
      return ta ? Glib::ustring("来た") : Glib::ustring("来て");
    }
    if (kotoba.substr(kotoba.size() - 2).compare("する") == 0) {
      return ta ? kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("した") : kotoba.substr(0, kotoba.size() - 2) + Glib::ustring("して");
    }
    break;
  }
  return Glib::ustring();
}
