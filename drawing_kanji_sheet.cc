/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * Nihongo
 * Copyright (C) Massimiliano Maniscalco 2011, 2012 <massi.neptune@yahoo.com>
 * 
Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawing_kanji_sheet.hh"

DrawingKanjiSheet::DrawingKanjiSheet() : left_margin (10.0), right_margin (10.0), up_margin (10.0), down_margin (10.0), cWidth (40.0), cHeight (40.0), pWidth (580.0), pHeight (820.0) {
	set_has_window ( true );
}

void DrawingKanjiSheet::
get_preferred_width_vfunc(int& minimum_width, int& natural_width) const {
	minimum_width = 400;
	natural_width = 560;
}

void DrawingKanjiSheet::get_preferred_height_for_width_vfunc(int /* width */,
   int& minimum_height, int& natural_height) const {
	minimum_height = 400;
	natural_height = 768;
}

void DrawingKanjiSheet::get_preferred_height_vfunc(int& minimum_height, int& natural_height) const {
	minimum_height = 400;
	natural_height = 768;
}

void DrawingKanjiSheet::get_preferred_width_for_height_vfunc(int /* height */,
   int& minimum_width, int& natural_width) const {
	minimum_width = 400;
	natural_width = 560;
}

void DrawingKanjiSheet::on_size_allocate(Gtk::Allocation& allocation) {
	set_allocation(allocation);
	if(ref_window) {
		ref_window->move_resize(allocation.get_x(), allocation.get_y(), allocation.get_width(), allocation.get_height());
	}
}

void DrawingKanjiSheet::on_realize() {
	set_realized();
	if(!ref_window) {
		GdkWindowAttr attributes;
		memset(&attributes, 0, sizeof(attributes));
		Gtk::Allocation allocation { get_allocation() };
		attributes.x = allocation.get_x();
		attributes.y = allocation.get_y();
		attributes.width = allocation.get_width();
		attributes.height = allocation.get_height();
		attributes.event_mask = get_events () | Gdk::EXPOSURE_MASK;
		attributes.window_type = GDK_WINDOW_CHILD;
		attributes.wclass = GDK_INPUT_OUTPUT;
		ref_window = Gdk::Window::create(get_parent_window(), &attributes, GDK_WA_X | GDK_WA_Y);
		set_window(ref_window);
		override_background_color(Gdk::RGBA("white"));
		override_color(Gdk::RGBA("black"));
		ref_window->set_user_data(gobj());
	}
}

bool DrawingKanjiSheet::on_draw (const Cairo::RefPtr<Cairo::Context>& cr) {
	draw_kanji_table(cr);
	return true;
}

void DrawingKanjiSheet::draw_kanji_table (const Cairo::RefPtr<Cairo::Context>& cr) {
	int pagine = count_pagine();
	set_up_kanji_table(cr);
	for (int nrpg = 0; nrpg < pagine; nrpg++) {
		double dy = pHeight * nrpg;
		draw_kanji_grid(cr, dy);
		draw_kanji_at_page(cr, nrpg, true);
	}
}
