/*
 * oboeru_sheet.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "oboeru_sheet.hh"

OboeruSheet::OboeruSheet() {
  cWidth = 80.0;
  cHeight = 100.0;
}

int OboeruSheet::count_pagine(void) {
  return (kanji.size() / KNJXPAGE) + ((kanji.size() % KNJXPAGE) > 0);
}

void OboeruSheet::set_up_kanji_table(const Cairo::RefPtr<Cairo::Context>& cr) {
  cr->select_font_face("UmeMincho", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
  cr->set_font_size(12.0);
  cr->set_source_rgb(0.0, 0.0, 0.0);
}

void OboeruSheet::draw_kanji_grid(const Cairo::RefPtr<Cairo::Context>& cr, double dy) {
  cr->set_line_width(2.0);
  cr->move_to(left_margin, up_margin + dy);
  cr->line_to(left_margin, pHeight - down_margin + dy);
  cr->line_to(pWidth - right_margin, pHeight - down_margin + dy);
  cr->line_to(pWidth - right_margin, up_margin + dy);
  cr->line_to(left_margin, up_margin + dy);
  for (short i = 1; i < 8; i++) {
    cr->move_to(left_margin, up_margin + (cHeight * i) + dy);
    cr->line_to(pWidth - right_margin, up_margin + (cHeight * i) + dy);
  }
  for (short i = 1; i < 7; i++) {
    cr->move_to(left_margin + (cWidth * i), up_margin + dy);
    cr->line_to(left_margin + (cWidth * i), pHeight - down_margin + dy);
  }
  cr->stroke();
  cr->set_line_width(0.5);
  double unita = cHeight / 7.0;
  for (short i = 0; i < 8; i++) {
    cr->move_to(left_margin, up_margin + unita + (cHeight * i) + dy);
    cr->line_to(pWidth - right_margin, up_margin + unita + (cHeight * i) + dy);
    cr->move_to(left_margin, up_margin + (5.0 * unita) + (cHeight * i) + dy);
    cr->line_to(pWidth - right_margin, up_margin + (5.0 * unita) + (cHeight * i) + dy);
    cr->move_to(left_margin, up_margin + (6.0 * unita) + (cHeight * i) + dy);
    cr->line_to(pWidth - right_margin, up_margin + (6.0 * unita) + (cHeight * i) + dy);
  }
  cr->stroke();
}

void OboeruSheet::draw_kanji_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int pagina, bool pages) {
  if (kanji.size() > pagina * KNJXPAGE) {
    double dy { pages ? pHeight * pagina : 0.0 };
    double unita = cHeight / 7.0;
    Cairo::TextExtents te;
    std::vector<Kanji>::iterator itknj { kanji.begin() + (pagina * KNJXPAGE) };
    short nk = 1;
    while (itknj != kanji.end()) {
      short int riga = (nk / 7) + ((nk % 7) > 0) - 1;
      short int colonna = nk - (riga * 7) - 1;
      Glib::ustring label = get_first_word(itknj->get_significato());
      cr->get_text_extents(label.raw(), te);
      double x = left_margin + (colonna * cWidth) + ((cWidth - te.x_advance) / 2.0);
      double y = up_margin + dy + (riga * cHeight) + te.height + ((unita - te.height) / 2.0);
      cr->move_to(x, y);
      cr->show_text(label.raw());
      label = get_first_word(itknj->get_kunyomi());
      cr->get_text_extents(label.raw(), te);
      x = left_margin + (colonna * cWidth) + ((cWidth - te.x_advance) / 2.0 );
      y = up_margin + dy + (riga * cHeight) + (5.0 * unita) + te.height + ((unita - te.height) / 2.0);
      cr->move_to(x, y);
      cr->show_text(label.raw());
      label = get_first_word(itknj->get_onyomi());
      cr->get_text_extents(label.raw(), te);
      x = left_margin + (colonna * cWidth) + ((cWidth - te.x_advance) / 2.0);
      y = up_margin + dy + (riga * cHeight) + (6.0 * unita) + te.height + ((unita - te.height) / 2.0);
      cr->move_to(x, y);
      cr->show_text(label.raw());
      itknj++;
      nk++;
      if (nk > KNJXPAGE) {
	break;
      }
    }
  }
}

Glib::ustring OboeruSheet::get_first_word(const Glib::ustring &str) {
  Glib::ustring trovato;
  size_t _pos = str.find_first_of(" ,　、", 0);
  trovato = _pos != Glib::ustring::npos ? str.substr(0, _pos) : str;
  return trovato;
}
