/*
 * test_inserimento_parole.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TESTINSERIMENTOPAROLE_HH_
#define _TESTINSERIMENTOPAROLE_HH_

#include "test_inserimento.hh"
#include <vector>
#include "database.hh"

class TestInserimentoParole: public TestInserimento 
{
public:
  TestInserimentoParole(Glib::RefPtr<Gtk::Builder> bldr, Database* db, const std::vector<long long int>& vec);
  inline ~TestInserimentoParole() {}
  void run_test(const TestOptions& opts);
protected:
  static AjConjug get_random_adjective_conjugation(void);
  static VbConjug get_random_verb_conjugation(void);
  void update_window(void);
  void on_risposta(short tasto = -1);
  void next_question(void);
private:
  Database* database;
  std::vector<long long int> content;
  std::vector<long long int>::iterator current;
  Parola current_word;
  VbConjug curr_vconj;
  AjConjug curr_aconj;
  bool curr_negative;
  bool curr_plain;
};

#endif // _TESTINSERIMENTOPAROLE_HH_
