/*
 * test_engine.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_engine.hh"

std::minstd_rand0 TestEngine::rnd_generator = std::minstd_rand0(std::chrono::system_clock::now().time_since_epoch().count());

void TestEngine::update_random_seed(void) {
  rnd_generator = std::minstd_rand0(std::chrono::system_clock::now().time_since_epoch().count());
}

int TestEngine::random(int min, int max) {
  std::uniform_int_distribution<int> distribution(min, max);
  return distribution(rnd_generator);
}

bool TestEngine::random_bool(void) {
  return random(0, 1) == 1; 
}

void TestEngine::init_test_engine(Glib::RefPtr<Gtk::Builder> bldr) {
  timer = 0;
  nr_quest = 0;
  options.tst_type = TestType::mem_wrd_sel;
  options.verso = 0;
  paused = false;
}

void TestEngine::connect_toolbar_signals(void) {
  main_win_connection = main_win->signal_hide().connect(sigc::mem_fun(*this, &TestEngine::on_window_closed));
  skip_connection = skip_toolbutton->signal_clicked().connect(sigc::mem_fun(*this, &TestEngine::on_skip_clicked));
  pause_connection = pause_toolbutton->signal_clicked().connect(sigc::mem_fun(*this, &TestEngine::on_pause_clicked));
}

bool TestEngine::on_timer_fired(void) {
  timer++;
  time_label->set_text(Glib::ustring::compose("%1 : %2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), timer / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), timer % 60)));
  return true;
}

void TestEngine::on_skip_clicked(void) {
  timer_connection.disconnect();
  next_question();
}

void TestEngine::on_pause_clicked(void) {
  if (paused) {
    continue_test();
    timer_connection = Glib::signal_timeout().connect_seconds(sigc::mem_fun(*this, &TestEngine::on_timer_fired), 1);
  } else {
    pause_test();
  }
}

