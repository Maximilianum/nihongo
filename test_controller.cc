/*
 * test_controller.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_controller.hh"
#include "lezione.hh"
#include "kakikata_sheet.hh"
#include "oboeru_sheet.hh"
#include "test_scelta_mult_parole.hh"
#include "test_scelta_mult_kanji.hh"
#include "test_inserimento_parole.hh"
#include "test_inserimento_numeri.hh"

std::vector<Glib::ustring> TestController::parola_types = { "Qualunque tipo" };
std::vector<Glib::ustring> TestController::test_objectives = { "memorizzazione parole", "lettura parole", "coniugazione" };
std::vector<Glib::ustring> TestController::conjugation_strings = { "Tutte le coniugazioni", "Presente", "Passato", "Sospensivo", "Causale", "Avverbiale", "Diventare", "Rendere", "Tutte le coniugazioni", "Presente", "Passato", "Forma in て", "Desiderativo", "Volitivo", "Esortativo", "Imperativo", "Obbligo", "Potenziale", "Passivo", "Causativo", "Condizionale" };
std::vector<Glib::ustring> TestController::verso_strings = { "giapponese -> italiano", "italiano -> giapponese", "kanji -> hiragana", "forma base -> forma coniugata", "kanji -> significato", "significato -> kanji", "kanji -> lettura", "da cifre a hiragana", "da hiragana a cifre" };

TestController::TestController(Glib::RefPtr<Gtk::Builder> bldr) : builder (bldr), test_engine (nullptr), kanji_sheet (nullptr) {
  woptions.tst_type = TestType::mem_wrd_sel;
  woptions.conj_style = 0;
  woptions.conj_negative = 0;
  woptions.vconj = VbConjug::null;
  woptions.aconj = AjConjug::all;
  koptions.tst_type = TestType::mem_knj_sel;
  koptions.tstk_mode = TestKanjiMode::memorization;
  noptions.tst_type = TestType::numb_ins;
  noptions.tstn_mode = TestNumbersMode::cardinal;
  noptions.max_val = 0;
  noptions.quest_nr = 10;
  main_page = 0;
  std::vector<Glib::ustring> types { Parola::get_types() };
  parola_types.insert(parola_types.end(), types.begin(), types.end());
  bldr->get_widget("test_selection_window", main_win);
  bldr->get_widget("test_notebook", test_notebook);
  bldr->get_widget("test_statusbar", main_status);
  stts_id = main_status->get_context_id(Glib::ustring("TEST_SEL"));
  // toolbar
  bldr->get_widget("main_toolbar", main_toolbar);
  bldr->get_widget("start_test_toolbutton", start_test_toolbutton);
  // page parole
  parole_liststore = Gtk::ListStore::create(parole_tmcr);
  bldr->get_widget("test_parole_treeview", parole_treeview);
  parole_treeview->set_model(parole_liststore);
  bldr->get_widget("test_parole_type_combobox", test_parole_type_combobox);
  test_parole_type_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_parole_type_combobox->set_model(test_parole_type_liststore);
  load_data_combobox(parola_types.begin(), parola_types.end(), test_parole_type_liststore);
  test_parole_type_combobox->set_active(0);
  test_wtype = 0;
  bldr->get_widget("test_parole_date_combobox", test_parole_date_combobox);
  test_date_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_parole_date_combobox->set_model(test_date_liststore);
  std::vector<Glib::ustring> dateopts { "non considerare la data", "più di tre giorni", "più di una settimana", "più di 15 giorni" };
  load_data_combobox(dateopts.begin(), dateopts.end(), test_date_liststore);
  test_parole_date_combobox->set_active(0);
  bldr->get_widget("test_parole_lezione_checkbutton", test_parole_lezione_checkbutton);
  bldr->get_widget("test_parole_lezione_combobox", test_parole_lezione_combobox);
  test_parole_lezione_liststore = Gtk::ListStore::create(lezioni_tmcr);
  test_parole_lezione_combobox->set_model(test_parole_lezione_liststore);
  bldr->get_widget("test_parole_stats_checkbutton", test_parole_stats_checkbutton);
  bldr->get_widget("test_parole_stats_scale", test_parole_stats_scale);
  // test
  bldr->get_widget("test_parole_ttype_combobox", test_parole_ttype_combobox);
  test_parole_ttype_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_parole_ttype_combobox->set_model (test_parole_ttype_liststore);
  std::vector<Glib::ustring> teststr { "test a risposta multipla", "inserimento testo" };
  load_data_combobox(teststr.begin(), teststr.end(), test_parole_ttype_liststore);
  test_parole_ttype_combobox->set_active(0);
  bldr->get_widget("test_parole_objective_combobox", test_parole_objective_combobox);
  test_parole_objective_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_parole_objective_combobox->set_model (test_parole_objective_liststore);
  load_data_combobox(test_objectives.begin(), test_objectives.end(), test_parole_objective_liststore);
  test_parole_objective_combobox->set_active(0);
  bldr->get_widget("test_parole_verso_combobox", test_parole_verso_combobox);
  test_parole_verso_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_parole_verso_combobox->set_model (test_parole_verso_liststore);
  load_data_combobox(verso_strings.begin(), verso_strings.begin() + 2, test_parole_verso_liststore);
  test_parole_verso_combobox->set_active(0);
  bldr->get_widget("test_parole_conjugation_combobox", test_parole_conjugation_combobox);
  test_parole_conjugation_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_parole_conjugation_combobox->set_model(test_parole_conjugation_liststore);
  load_data_combobox(conjugation_strings.begin(), conjugation_strings.end() - 21, test_parole_conjugation_liststore);
  // options
  bldr->get_widget("test_parole_options_frame", test_parole_options_frame);
  bldr->get_widget("test_negative_radiobutton1", test_negative_radiobutton1);
  bldr->get_widget("test_negative_radiobutton2", test_negative_radiobutton2);
  bldr->get_widget("test_negative_radiobutton3", test_negative_radiobutton3);
  test_negative_radiobutton1->set_active(true);
  bldr->get_widget("test_style_radiobutton1", test_style_radiobutton1);
  bldr->get_widget("test_style_radiobutton2", test_style_radiobutton2);
  bldr->get_widget("test_style_radiobutton3", test_style_radiobutton3);
  test_style_radiobutton1->set_active(true);
  bldr->get_widget("test_parole_type_radiobutton1", test_parole_type_radiobutton1);
  bldr->get_widget("test_parole_type_radiobutton2", test_parole_type_radiobutton2);
  test_parole_type_radiobutton1->set_active(true);
  // page kanji
  kanji_liststore = Gtk::ListStore::create(kanji_tmcr);
  bldr->get_widget("test_kanji_treeview", kanji_treeview);
  kanji_treeview->set_model(kanji_liststore);
  bldr->get_widget("test_kanji_memo_radiobutton", test_kanji_memo_radiobutton);
  bldr->get_widget("test_kanji_reading_radiobutton", test_kanji_reading_radiobutton);
  test_kanji_memo_radiobutton->set_active(true);
  bldr->get_widget("test_kanji_verso_combobox", test_kanji_verso_combobox);
  test_kanji_verso_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_kanji_verso_combobox->set_model (test_kanji_verso_liststore);
  load_data_combobox(verso_strings.begin() + 4, verso_strings.begin() + 6, test_kanji_verso_liststore);
  test_kanji_verso_combobox->set_active(0);
  bldr->get_widget("test_kanji_lezione_combobox", test_kanji_lezione_combobox);
  test_kanji_lezione_liststore = Gtk::ListStore::create(lezioni_tmcr);
  test_kanji_lezione_combobox->set_model (test_kanji_lezione_liststore);
  bldr->get_widget("test_kanji_lezione_checkbutton", test_kanji_lezione_checkbutton);
  bldr->get_widget("test_kanji_date_combobox", test_kanji_date_combobox);
  test_kanji_date_combobox->set_model(test_date_liststore);
  bldr->get_widget("test_kanji_stats_checkbutton", test_kanji_stats_checkbutton);
  bldr->get_widget("test_kanji_stats_scale", test_kanji_stats_scale);
  test_kanji_date_combobox->set_active(0);
  bldr->get_widget("kakikata_button", kakikata_button);
  bldr->get_widget("oboeteiru_button", oboeteiru_button);
  // page numeri
  bldr->get_widget("test_numbers_mode_radiobutton1", test_numbers_mode_radiobutton1);
  bldr->get_widget("test_numbers_mode_radiobutton2", test_numbers_mode_radiobutton2);
  bldr->get_widget("test_numbers_mode_radiobutton3", test_numbers_mode_radiobutton3);
  test_numbers_mode_radiobutton1->set_active(true);
  bldr->get_widget("test_numbers_verso_combobox", test_numbers_verso_combobox);
  test_numbers_verso_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_numbers_verso_combobox->set_model(test_numbers_verso_liststore);
  load_data_combobox(verso_strings.begin() + 7, verso_strings.begin() + 9, test_numbers_verso_liststore);
  test_numbers_verso_combobox->set_active(0);
  bldr->get_widget("test_numbers_totquest_entry", test_numbers_totquest_entry);
  test_numbers_totquest_entry->set_text("10");
  bldr->get_widget("test_numbers_maxvalue_combobox", test_numbers_maxvalue_combobox);
  test_numbers_maxvalue_liststore = Gtk::ListStore::create(combobox_tmcr);
  test_numbers_maxvalue_combobox->set_model (test_numbers_maxvalue_liststore);
  std::vector<Glib::ustring> maxvals { "10", "99", "999", "9.999", "99.999.999" };
  load_data_combobox(maxvals.begin(), maxvals.end(), test_numbers_maxvalue_liststore);
  test_numbers_maxvalue_combobox->set_active(0);
	
  connect_signals();
}

void TestController::init_window(void) {
  load_lezioni_combobox();
  load_kanji_in_treeview();
}

Glib::ustring TestController::get_estimate_time_string(int ni) {
  Glib::ustring str;
  if (ni > 0) {
    int secondi { (ni*5) % 60 };
    int minuti { (ni*5) / 60 };
    if (minuti > 0) {
      str.append(Glib::ustring::format(minuti));
      if (minuti == 1) {
	str.append(" minuto");
      } else {
	str.append(" minuti");
      }
    }
    if (secondi > 0) {
      if (minuti > 0) {
	str.append(" e ");
      }
      str.append(Glib::ustring::format(secondi));
      if (secondi == 1) {
	str.append(" secondo");
      } else {
	str.append(" secondi");
      }
    }
  }
  return str;
}

void TestController::update_statusbar(int ni) {
  main_status->pop(stts_id);
  if (ni > 0) {
    Glib::ustring tempo_str { get_estimate_time_string(ni) };
    Glib::ustring str;
    switch (main_page) {
    case PAGE_WORD:
      str = ni > 1 ? "parole" : "parola";
      break;
    case PAGE_KANJI:
      str.assign("kanji");
      break;
    case PAGE_NUMBERS:
      str = ni > 1 ? "numeri" : "numero";
      break;
    }
    main_status->push(Glib::ustring::compose("Test su %1 %2, tempo necessario stimato: %3", Glib::ustring::format(ni), str, tempo_str), stts_id);
  } else {
    main_status->push(Glib::ustring("Test non eseguibile"), stts_id);
  }
}

void TestController::connect_signals(void) {
  // main toolbar
  start_test_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &TestController::on_start_clicked));
  // main win
  main_win->signal_focus_in_event().connect(sigc::mem_fun(this, &TestController::on_main_win_focus_in));
  main_win->signal_focus_out_event().connect(sigc::mem_fun(this, &TestController::on_main_win_focus_out));
  test_notebook->signal_switch_page().connect(sigc::mem_fun(this, &TestController::on_main_page_switch));
  // page parole
  test_parole_ttype_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_test_ttype_changed));
  test_parole_objective_connection = test_parole_objective_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_objective_changed));
  test_parole_type_radiobutton1->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_conjugation_type_radiobutton_toggled), test_parole_type_radiobutton1));
  test_parole_type_radiobutton2->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_conjugation_type_radiobutton_toggled), test_parole_type_radiobutton2));
  test_style_radiobutton1->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_polite_radiobutton_toggled), test_style_radiobutton1));
  test_style_radiobutton2->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_polite_radiobutton_toggled), test_style_radiobutton2));
  test_style_radiobutton3->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_polite_radiobutton_toggled), test_style_radiobutton3));
  test_negative_radiobutton1->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_negative_radiobutton_toggled), test_negative_radiobutton1));
  test_negative_radiobutton2->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_negative_radiobutton_toggled), test_negative_radiobutton2));
  test_negative_radiobutton3->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_negative_radiobutton_toggled), test_negative_radiobutton3));
  test_parole_conjugation_connection = test_parole_conjugation_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_conjugation_changed));
  // filtro parole
  test_parole_type_connection = test_parole_type_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_type_changed));
  test_parole_date_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::load_parole_in_treeview));
  test_parole_lezione_connection = test_parole_lezione_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::load_parole_in_treeview));
  test_parole_lezione_checkbutton->signal_toggled().connect(sigc::mem_fun(this, &TestController::on_parole_enable_lezione));
  test_parole_stats_checkbutton->signal_toggled().connect(sigc::mem_fun(this, &TestController::on_parole_enable_stats));
  test_parole_stats_scale->signal_value_changed().connect(sigc::mem_fun(this, &TestController::load_parole_in_treeview));
  // page kanji
  test_kanji_memo_radiobutton->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun(this, &TestController::on_kanji_mode_radiobutton_toggled), test_kanji_memo_radiobutton));
  test_kanji_reading_radiobutton->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun (this, &TestController::on_kanji_mode_radiobutton_toggled), test_kanji_reading_radiobutton));
  test_kanji_lezione_connection = test_kanji_lezione_combobox->signal_changed().connect(sigc::mem_fun (this, &TestController::load_kanji_in_treeview));
  test_kanji_lezione_checkbutton->signal_toggled().connect(sigc::mem_fun(this, &TestController::on_kanji_enable_lezione));
  test_kanji_date_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::load_kanji_in_treeview));
  test_kanji_stats_checkbutton->signal_toggled().connect(sigc::mem_fun(this, &TestController::on_kanji_enable_stats));
  test_kanji_stats_scale->signal_value_changed().connect(sigc::mem_fun(this, &TestController::load_kanji_in_treeview));
  kakikata_button->signal_clicked().connect(sigc::bind<Gtk::Button*>(sigc::mem_fun(this, &TestController::on_kanji_kaku_clicked), kakikata_button));
  oboeteiru_button->signal_clicked().connect(sigc::bind<Gtk::Button*>(sigc::mem_fun(this, &TestController::on_kanji_kaku_clicked), oboeteiru_button));
  // page numeri
  test_numbers_mode_radiobutton1->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun (this, &TestController::on_numbers_mode_radiobutton_toggled), test_numbers_mode_radiobutton1));
  test_numbers_mode_radiobutton2->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun (this, &TestController::on_numbers_mode_radiobutton_toggled), test_numbers_mode_radiobutton2));
  test_numbers_mode_radiobutton3->signal_toggled().connect(sigc::bind<Gtk::RadioButton*>(sigc::mem_fun (this, &TestController::on_numbers_mode_radiobutton_toggled), test_numbers_mode_radiobutton3));
  test_numbers_totquest_entry->signal_activate().connect(sigc::mem_fun (this, &TestController::on_numbers_totquest_changed));
}

void TestController::on_main_page_switch(Gtk::Widget *page, guint page_num) {
  main_page = page_num;
  switch (main_page) {
  case PAGE_WORD:
    update_statusbar(parole_liststore->children().size());
    break;
  case PAGE_KANJI:
    update_statusbar(kanji_liststore->children().size());
    break;
  case 2:
    update_statusbar(atoi(test_numbers_totquest_entry->get_text().c_str()));
    break;
  }
}

bool TestController::on_main_win_focus_in(GdkEventFocus *event) {
  main_toolbar->set_sensitive(true);
  switch (main_page) {
  case PAGE_WORD:
    parole_treeview->set_sensitive(true);
    load_parole_in_treeview();
    break;
  case PAGE_KANJI:
    kanji_treeview->set_sensitive(true);
    load_kanji_in_treeview();
    break;
  }
  return false;
}

bool TestController::on_main_win_focus_out(GdkEventFocus *event) {
  main_toolbar->set_sensitive(false);
  switch (main_page) {
  case PAGE_WORD:
    parole_treeview->set_sensitive(false);
    break;
  case PAGE_KANJI:
    kanji_treeview->set_sensitive(false);
    break;
  }
  return false;
}

void TestController::on_window_closed(void) {
  win_closed_connection.disconnect();
  delete test_engine;
  test_engine = nullptr;
  present_window();
}

void TestController::load_lezioni_combobox(void) {
  test_parole_lezione_liststore->clear();
  test_kanji_lezione_liststore->clear();
  std::vector<Lezione> lezioni;
  try {
    lezioni = database->get_lezioni();
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestController::load_lezioni_combobox() " << dbe.get_message() << std::endl;
  }
  std::vector<Lezione>::const_iterator itl { lezioni.cbegin() };
  while (itl != lezioni.cend()) {
    Gtk::TreeModel::Row row1 { *(test_parole_lezione_liststore->append()) };
    Gtk::TreeModel::Row row2 { *(test_kanji_lezione_liststore->append()) };
    row1[lezioni_tmcr.idl] = Glib::ustring::format(itl->get_idl());
    row2[lezioni_tmcr.idl] = Glib::ustring::format(itl->get_idl());
    row1[lezioni_tmcr.name] = itl->get_name();
    row2[lezioni_tmcr.name] = itl->get_name();
    itl++;
  }
  test_parole_lezione_combobox->set_active(0);
  test_kanji_lezione_combobox->set_active(0);
}

void TestController::on_start_clicked(void) {
  TestOptions* opts { nullptr };
  main_win->hide();
  if (main_page == PAGE_WORD) {
    Glib::ustring sqlite_parole { get_parole_sqlite_string() };
    woptions.verso = test_parole_verso_combobox->get_active_row_number();
    std::vector<long long int> words;
    try {
      words = database->get_id_parole(sqlite_parole);
    } catch (const Database::DBError& dbe) {
      std::cerr << "Controller::on_start_clicked() " << dbe.get_message() << std::endl;
    } catch (const Database::DBEmptyResultException& dbere) { }
    switch (woptions.tst_type) {
    case TestType::mem_wrd_sel:
    case TestType::cnj_sel:
    case TestType::read_wrd_sel:
      test_engine = new TestSceltaMultParole(builder, database, words);
      break;
    case TestType::cnj_ins:
    case TestType::read_wrd_ins:
      test_engine = new TestInserimentoParole(builder, database, words);
      break;
    }
    opts = &woptions;
  } else if (main_page == PAGE_KANJI) {
    Glib::ustring sqlite_kanji { get_kanji_sqlite_string() };
    koptions.verso = test_kanji_verso_combobox->get_active_row_number();
    std::vector<long long int> kanji;
    try {
      kanji = database->get_id_kanji(sqlite_kanji);
    } catch (const Database::DBError& dbe) {
      std::cerr << "Controller::on_start_clicked() " << dbe.get_message() << std::endl;
    } catch (const Database::DBEmptyResultException& dbere) { }
    if (koptions.tst_type == TestType::mem_knj_sel) {
      test_engine = new TestSceltaMultKanji(builder, database, kanji);
    }
    opts = &koptions;
  } else {
    noptions.verso = test_numbers_verso_combobox->get_active_row_number();
    test_engine = new TestInserimentoNumeri(builder);
    opts = &noptions;
  }
  win_closed_connection = test_engine->signal_window_closed().connect(sigc::mem_fun(this, &TestController::on_window_closed));
  test_engine->run_test(*opts);
}

void TestController::load_parole_in_treeview(void) {
  Glib::ustring sqlite_parole { get_parole_sqlite_string() };
  std::vector<Parola> words;
  parole_liststore->clear();
  try {
    words = database->get_parole(sqlite_parole);
  } catch (const Database::DBError& dbe) {
    std::cerr << "Controller::load_parole_in_treeview() " << dbe.get_message() << std::endl;
  } catch (const Database::DBEmptyResultException& dbere) { }
  std::vector<Parola>::const_iterator iter = words.cbegin();
  while (iter != words.cend()) {
    Gtk::TreeModel::iterator iter_tree = parole_liststore->append();
    Gtk::TreeModel::Row row = *iter_tree;
    row[parole_tmcr.idw] = iter->get_idw();
    row[parole_tmcr.wtype] = Parola::get_type_string(iter->get_wtype());
    row[parole_tmcr.kotoba] = iter->get_kotoba();
    row[parole_tmcr.furigana] = iter->get_furigana();
    row[parole_tmcr.itariago] = iter->get_itariago();
    row[parole_tmcr.fcolour] = iter->get_record(woptions.tst_type).error ? Gdk::RGBA("red") : Gdk::RGBA("black");
    iter++;
  }
  update_statusbar(words.size());
}

Glib::ustring TestController::get_parole_sqlite_string(void) {
  Glib::ustring sqlite_parole { Glib::ustring("where ") };
  std::vector<Glib::ustring> opts;
  opts.push_back(Glib::ustring::compose("RECORD.RTYPE='%1'", Test::get_test_type_id(woptions.tst_type)));
  if (woptions.tst_type == TestType::cnj_sel || woptions.tst_type == TestType::cnj_ins) { // conjugation
    if (test_parole_type_radiobutton1->get_active()) { // adjectives
      if (test_wtype == 0) {
	opts.push_back(Glib::ustring("(WTYPE BETWEEN 2 AND 3)"));
      } else if (test_wtype > 0) {
	opts.push_back(Glib::ustring::compose("WTYPE='%1'",Glib::ustring::format(test_wtype + 1)));
      }
    } else { // verbs
      if (test_wtype == 0) {
	opts.push_back(Glib::ustring("(WTYPE BETWEEN 4 AND 8)"));
      } else if (test_wtype > 0) {
	opts.push_back(Glib::ustring::compose("WTYPE='%1'",Glib::ustring::format(test_wtype + 3)));
      }
    }
  } else { // dictionary
    if (test_wtype > 0) {
      opts.push_back(Glib::ustring::compose("WTYPE='%1'",Glib::ustring::format(test_wtype)));
    }
    if (woptions.tst_type == TestType::read_wrd_sel || woptions.tst_type == TestType::read_wrd_ins) {
      opts.push_back(Glib::ustring("YOMIKATA!=''"));
    }
  }
  int date_row { test_parole_date_combobox->get_active_row_number() };
  if (date_row > 0) {
    time_t tref = time(nullptr);
    switch (date_row) {
    case 1:
      tref -= 86400 * 3;
      break;
    case 2:
      tref -= 86400 * 7;
      break;
    case 3:
      tref -= 86400 * 15;
    }
    opts.push_back(Glib::ustring::compose("RECORD.DATE<%1", Glib::ustring::format(tref)));
  }
  if (test_parole_stats_checkbutton->get_active()) {
    opts.push_back(Glib::ustring::compose("RECORD.TOTAL>0 AND (RECORD.CORRECT*1.0)/RECORD.TOTAL*100.0<=%1", Glib::ustring::format(test_parole_stats_scale->get_value())));
  }
  if (test_parole_lezione_checkbutton->get_active()) {
    Gtk::TreeModel::Row row { *(test_parole_lezione_combobox->get_active()) };
    long long int idl { atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str()) };
    opts.push_back(Glib::ustring::compose("LWORD.RIFFL=%1 ", Glib::ustring::format(idl)));
  }
  std::vector<Glib::ustring>::iterator iter { opts.begin() };
  while (iter != opts.end()) {
    if (iter != opts.begin()) {
      sqlite_parole.append(Glib::ustring(" AND "));
    }
    sqlite_parole.append(*iter);
    iter++;
  }
  return sqlite_parole;
}

void TestController::on_parole_test_ttype_changed(void) {
  test_parole_objective_connection.disconnect();
  test_parole_objective_liststore->clear();
  switch (test_parole_ttype_combobox->get_active_row_number()) {
  case 0:
    load_data_combobox(test_objectives.begin(), test_objectives.end(), test_parole_objective_liststore);
    break;
  case 1:
    load_data_combobox(test_objectives.begin() + 1, test_objectives.end(), test_parole_objective_liststore);
    break;
  }
  test_parole_objective_connection = test_parole_objective_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_objective_changed));
  test_parole_objective_combobox->set_active(0);
}

void TestController::on_parole_objective_changed(void) {
  test_parole_type_connection.disconnect();
  test_parole_conjugation_connection.disconnect();
  Gtk::TreeModel::Row row { *(test_parole_objective_combobox->get_active()) };
  Glib::ustring obj { row[combobox_tmcr.column] };
  if (obj.compare(test_objectives[0]) == 0) {
    woptions.tst_type = TestType::mem_wrd_sel;
    load_data_combobox(verso_strings.begin(), verso_strings.begin() + 2, test_parole_verso_liststore);
  } else if (obj.compare(test_objectives[1]) == 0) {
    if (test_parole_ttype_combobox->get_active_row_number() == 0) {
      woptions.tst_type = TestType::read_wrd_sel;
    } else {
      woptions.tst_type = TestType::read_wrd_ins;
    }
    load_data_combobox(verso_strings.begin() + 2, verso_strings.begin() + 3, test_parole_verso_liststore);
  } else if (obj.compare(test_objectives[2]) == 0) {
    if (test_parole_ttype_combobox->get_active_row_number() == 0) {
      woptions.tst_type = TestType::cnj_sel;
    } else {
      woptions.tst_type = TestType::cnj_ins;
    }
    load_data_combobox(verso_strings.begin() + 3, verso_strings.begin() + 4, test_parole_verso_liststore);
  }
  test_parole_conjugation_liststore->clear();
  if (woptions.tst_type == TestType::cnj_sel || woptions.tst_type == TestType::cnj_ins) {
    test_parole_options_frame->set_visible(true);
    on_conjugation_type_radiobutton_toggled(test_parole_type_radiobutton1->get_active() ? test_parole_type_radiobutton1 : test_parole_type_radiobutton2);
    test_parole_type_connection.disconnect();
    test_parole_conjugation_connection.disconnect();
  } else {
    test_parole_options_frame->set_visible(false);
    load_data_combobox(parola_types.begin(), parola_types.end(), test_parole_type_liststore);
  }
  test_parole_type_combobox->set_active(0);
  test_parole_conjugation_combobox->set_active(0);
  test_parole_verso_combobox->set_active(0);
  test_parole_type_connection = test_parole_type_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_type_changed));
  test_parole_conjugation_connection = test_parole_conjugation_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_conjugation_changed));
  load_parole_in_treeview();
}

void TestController::on_conjugation_type_radiobutton_toggled(Gtk::RadioButton *button) {
  if (button->get_active()) {
    test_parole_conjugation_connection.disconnect();
    test_parole_type_connection.disconnect();
    bool isVerb = button == test_parole_type_radiobutton2;
    test_parole_conjugation_liststore->clear();
    test_parole_type_liststore->clear();
    if (isVerb) {
      woptions.vconj = VbConjug::all;
      woptions.aconj = AjConjug::null;
      load_data_combobox(conjugation_strings.begin() + 8, conjugation_strings.end(), test_parole_conjugation_liststore);
      load_data_combobox(parola_types.begin() + 4, parola_types.begin() + 9, test_parole_type_liststore);
    } else {
      woptions.vconj = VbConjug::null;
      woptions.aconj = AjConjug::all;
      load_data_combobox(conjugation_strings.begin(), conjugation_strings.begin() + 8, test_parole_conjugation_liststore);
      load_data_combobox(parola_types.begin() + 2, parola_types.begin() + 4, test_parole_type_liststore);
    }
    test_parole_conjugation_combobox->set_active(0);
    test_parole_type_combobox->set_active(0);
    test_parole_conjugation_connection = test_parole_conjugation_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_conjugation_changed));
    test_parole_type_connection = test_parole_type_combobox->signal_changed().connect(sigc::mem_fun(this, &TestController::on_parole_type_changed));
    load_parole_in_treeview();
  }
}

void TestController::on_polite_radiobutton_toggled(Gtk::RadioButton *button) {
  if (button->get_active()) {
    if (button == test_style_radiobutton1) {
      woptions.conj_style = 0;
    } else if (button == test_style_radiobutton2) {
      woptions.conj_style = 1;
    } else {
      woptions.conj_style = -1;
    }
  }
}

void TestController::on_negative_radiobutton_toggled(Gtk::RadioButton *button) {
  if (button->get_active()) {
    if (button == test_negative_radiobutton1) {
      woptions.conj_negative = 0;
    } else if (button == test_negative_radiobutton2) {
      woptions.conj_negative = 1;
    } else {
      woptions.conj_negative = -1;
    }
  }
}

void TestController::on_parole_conjugation_changed(void) {
  int mn = test_parole_conjugation_combobox->get_active_row_number();
  if (woptions.vconj != VbConjug::null) {
    switch (mn) {
    case 0:
      woptions.vconj = VbConjug::all;
      break;
    case 1:
      woptions.vconj = VbConjug::presente;
      break;
    case 2:
      woptions.vconj = VbConjug::passato;
      break;
    case 3:
      woptions.vconj = VbConjug::te;
      break;
    case 4:
      woptions.vconj = VbConjug::desiderativo;
      break;
    case 5:
      woptions.vconj = VbConjug::volitivo;
      break;
    case 6:
      woptions.vconj = VbConjug::esortativo;
      break;
    case 7:
      woptions.vconj = VbConjug::imperativo;
      break;
    case 8:
      woptions.vconj = VbConjug::obbligo;
      break;
    case 9:
      woptions.vconj = VbConjug::potenziale;
      break;
    case 10:
      woptions.vconj = VbConjug::passivo;
      break;
    case 11:
      woptions.vconj = VbConjug::causativo;
      break;
    case 12:
      woptions.vconj = VbConjug::condizionale;
      break;
    }
  } else if (woptions.aconj != AjConjug::null) {
    switch (mn) {
    case 0:
      woptions.aconj = AjConjug::all;
      break;
    case 1:
      woptions.aconj = AjConjug::presente;
      break;
    case 2:
      woptions.aconj = AjConjug::passato;
      break;
    case 3:
      woptions.aconj = AjConjug::sospensivo;
      break;
    case 4:
      woptions.aconj = AjConjug::causale;
      break;
    case 5:
      woptions.aconj = AjConjug::avverbiale;
      break;
    case 6:
      woptions.aconj = AjConjug::diventare;
      break;
    case 7:
      woptions.aconj = AjConjug::rendere;
      break;
    }
  }
}

void TestController::on_parole_enable_lezione(void) {
  test_parole_lezione_combobox->set_sensitive(test_parole_lezione_checkbutton->get_active());
  load_parole_in_treeview();
}

void TestController::on_parole_type_changed(void) {
  short prima = test_wtype;
  test_wtype = test_parole_type_combobox->get_active_row_number();
  if (test_wtype >= 0 && prima != test_wtype) {
    load_parole_in_treeview();
  }
}

void TestController::on_parole_enable_stats(void) {
  test_parole_stats_scale->set_sensitive(test_parole_stats_checkbutton->get_active());
  load_parole_in_treeview();
}

void TestController::load_kanji_in_treeview(void) {
  Glib::ustring sqlite_kanji { get_kanji_sqlite_string() };
  std::vector<Kanji> kanji;
  kanji_liststore->clear();
  try {
    kanji = database->get_kanji(sqlite_kanji);
  } catch (const Database::DBError& dbe) {
    std::cerr << "Controller::load_kanji_in_treeview() " << dbe.get_message() << std::endl;
  } catch (const Database::DBEmptyResultException& dbere) { }
  std::vector<Kanji>::const_iterator iter = kanji.cbegin();
  while (iter != kanji.cend()) {
    Gtk::TreeModel::iterator iter_tree = kanji_liststore->append();
    Gtk::TreeModel::Row row = *iter_tree;
    row[kanji_tmcr.kanji] = iter->get_kanji();
    row[kanji_tmcr.tratti] = iter->get_tratti();
    row[kanji_tmcr.radicale] = iter->get_radicale();
    row[kanji_tmcr.fcolour] = iter->get_record(koptions.tst_type).error ? Gdk::RGBA("red") : Gdk::RGBA("black");
    iter++;
  }
  update_statusbar(kanji.size());
}

Glib::ustring TestController::get_kanji_sqlite_string(void) {
  Glib::ustring sqlite_kanji { Glib::ustring("where ") };
  std::vector<Glib::ustring> opts;
  opts.push_back(Glib::ustring::compose("RECORD.RTYPE='%1'", Test::get_test_type_id(koptions.tst_type)));
  int date_row { test_kanji_date_combobox->get_active_row_number() };
  if (date_row > 0) {
    time_t tref = time(nullptr);
    switch (date_row) {
    case 1:
      tref -= 86400 * 3;
      break;
    case 2:
      tref -= 86400 * 7;
      break;
    case 3:
      tref -= 86400 * 15;
    }
    opts.push_back(Glib::ustring::compose("RECORD.DATE<%1", Glib::ustring::format(tref)));
  }
  if (test_kanji_stats_checkbutton->get_active()) {
    opts.push_back(Glib::ustring::compose("RECORD.TOTAL>0 AND (RECORD.CORRECT*1.0)/RECORD.TOTAL*100.0<=%1", Glib::ustring::format(test_kanji_stats_scale->get_value())));
  }
  if (test_kanji_lezione_checkbutton->get_active()) {
    Gtk::TreeModel::Row row { *(test_kanji_lezione_combobox->get_active()) };
    long long int idl { atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str()) };
    opts.push_back(Glib::ustring::compose("LKANJI.RIFFL=%1 ", Glib::ustring::format(idl)));
  }
  std::vector<Glib::ustring>::iterator iter { opts.begin() };
  while (iter != opts.end()) {
    if (iter != opts.begin()) {
      sqlite_kanji.append(Glib::ustring(" AND "));
    }
    sqlite_kanji.append(*iter);
    iter++;
  }
  return sqlite_kanji;
}

void TestController::on_kanji_mode_radiobutton_toggled(Gtk::RadioButton* button) {
  if (button->get_active()) {
    TestKanjiMode prima = koptions.tstk_mode;
    if (button == test_kanji_memo_radiobutton) {
      koptions.tstk_mode = TestKanjiMode::memorization;
    }
    if (button == test_kanji_reading_radiobutton) {
      koptions.tstk_mode = TestKanjiMode::reading;
    }
    test_kanji_verso_liststore->clear();
    if (koptions.tstk_mode == TestKanjiMode::memorization) {
      load_data_combobox(verso_strings.begin() + 4, verso_strings.begin() + 6, test_kanji_verso_liststore);
    } else {
      load_data_combobox(verso_strings.begin() + 6, verso_strings.begin() + 7, test_kanji_verso_liststore);
    }
    test_kanji_verso_combobox->set_active(0);
  }
}

void TestController::on_kanji_enable_lezione(void) {
  test_kanji_lezione_combobox->set_sensitive(test_kanji_lezione_checkbutton->get_active());
  load_kanji_in_treeview();
}

void TestController::on_kanji_enable_stats(void) {
  test_kanji_stats_scale->set_sensitive(test_kanji_stats_checkbutton->get_active());
  load_kanji_in_treeview();
}

void TestController::on_kanji_kaku_clicked(Gtk::Button* button) {
  Glib::ustring sqlite_kanji { get_kanji_sqlite_string() };
  std::vector<Kanji> kanji;
  try {
    kanji = database->get_kanji(sqlite_kanji);
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestController::on_kanji_kaku_clicked() " << dbe.get_message() << std::endl;
  } catch (const Database::DBEmptyResultException& dbere) {}
  if (button == kakikata_button) {
    kanji_sheet = new KakikataSheet();
  } else {
    kanji_sheet = new OboeruSheet();
  }
  int pagine = (kanji.size() / 40) + ((kanji.size() % 40) > 0);
  kanji_sheet->set_size_request(560, 768 * pagine);
  kanji_sheet->set_kanji(kanji);
  print_or_preview(Gtk::PRINT_OPERATION_ACTION_PRINT_DIALOG);
}

void TestController::print_or_preview(Gtk::PrintOperationAction print_action) {
  Glib::RefPtr<PrintKanjiTableOperation> print { PrintKanjiTableOperation::create() };
  print->set_kaku_area(kanji_sheet);
  print->set_track_print_status();
  print->set_default_page_setup(m_ref_pagesetup);
  print->set_print_settings(m_ref_printsettings);
  print->signal_done().connect(sigc::bind(sigc::mem_fun(*this, &TestController::on_printOperation_done), print));
  try {
    print->run(print_action, *main_win);
  } catch (const Gtk::PrintError& ex) {
    std::cerr << "An error occurred while trying to run a print operation:" << ex.what() << std::endl;
  }
}

void TestController::on_printOperation_status_changed(const Glib::RefPtr<PrintKanjiTableOperation>& operation) {
  Glib::ustring status_msg { operation->is_finished() ? "Print job completed." : operation->get_status_string() };
  std::cout << status_msg << std::endl;
}

void TestController::on_printOperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr<PrintKanjiTableOperation>& operation) {
  if (result == Gtk::PRINT_OPERATION_RESULT_ERROR) {
    Gtk::MessageDialog err_dialog (*main_win, "Error printing kanji table", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
    err_dialog.run();
  } else if (result == Gtk::PRINT_OPERATION_RESULT_APPLY) {
    m_ref_printsettings = operation->get_print_settings();
  }
  if (!operation->is_finished()) {
    operation->signal_status_changed().connect(sigc::bind(sigc::mem_fun(*this, &TestController::on_printOperation_status_changed), operation));
  } else {
    delete kanji_sheet;
    kanji_sheet = nullptr;
  }
}

void TestController::on_numbers_mode_radiobutton_toggled(Gtk::RadioButton* button) {
  if (button->get_active()) {
    if (button == test_numbers_mode_radiobutton1) {
      noptions.tstn_mode = TestNumbersMode::cardinal;
    } else if (button == test_numbers_mode_radiobutton2) {
      noptions.tstn_mode = TestNumbersMode::quantity;
    } else if (button == test_numbers_mode_radiobutton3) {
      noptions.tstn_mode = TestNumbersMode::calendar;
    }
    test_numbers_verso_liststore->clear();
    switch (noptions.tstn_mode) {
    case TestNumbersMode::cardinal:
      load_data_combobox(verso_strings.begin() + 7, verso_strings.begin() + 9, test_numbers_verso_liststore);
      test_numbers_maxvalue_combobox->set_sensitive(true);
      test_numbers_totquest_entry->set_sensitive(true);
      break;
    case TestNumbersMode::quantity:
      load_data_combobox(verso_strings.begin() + 7, verso_strings.begin() + 9, test_numbers_verso_liststore);
      test_numbers_maxvalue_combobox->set_active(0);
      test_numbers_maxvalue_combobox->set_sensitive(false);
      test_numbers_totquest_entry->set_text("10");
      test_numbers_totquest_entry->set_sensitive(false);
      break;
    case TestNumbersMode::calendar:
      load_data_combobox(verso_strings.begin() + 2, verso_strings.begin() + 3, test_numbers_verso_liststore);
      test_numbers_maxvalue_combobox->set_sensitive(false);
      test_numbers_totquest_entry->set_text("31");
      test_numbers_totquest_entry->set_sensitive(true);
      break;
    }
    test_numbers_verso_combobox->set_active(0);
    update_statusbar(atoi(test_numbers_totquest_entry->get_text().c_str()));
  }
}

void TestController::on_numbers_totquest_changed(void) {
  noptions.quest_nr = atoi(test_numbers_totquest_entry->get_text().c_str());
  update_statusbar(noptions.quest_nr);
}
