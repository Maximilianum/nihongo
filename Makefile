#
# Makefile
#


# compilatore da usare
CPP = g++

TARGET ?= debug
ifeq ($(TARGET),debug)
 CPPFLAGS = -std=c++14 -O0 -g `pkg-config --cflags gtkmm-3.0`
endif
ifeq ($(TARGET),release)
 CPPFLAGS = -std=c++14 -O3 `pkg-config --cflags gtkmm-3.0`
endif

# nome dell'eseguibile
EXE = nihongo

# lista delle librerie da utilizzare separate dagli spazi
# ogni libreria dovrà avere il prefisso -l
LIBS = `pkg-config --libs gtkmm-3.0` -lsqlite3

# automated creation of object files list from all files with .cc suffix
OBJS := $(patsubst %.cc,%.o,$(wildcard *.cc))

$(EXE): $(OBJS) Makefile
	$(CPP) $(CPPFLAGS) -o $@ $(OBJS) $(LIBS)

# automated dependencies
#$(OBJS): $(HDRS) Makefile

main.o : main.cc controller.hh

controller.o : controller.cc controller.hh ctrl_app_engine.hh ctrl_engine.hh test.hh parola.hh kanji.hh forma.hh lezione.hh database.hh combobox_tmcr.hh parole_tmcr.hh kanji_tmcr.hh forme_tmcr.hh lezioni_tmcr.hh test_controller.hh

ctrl_app_engine.o : ctrl_app_engine.cc ctrl_app_engine.hh ctrl_engine.hh

ctrl_engine.o : ctrl_engine.cc ctrl_engine.hh

db_engine.o : db_engine.cc db_engine.hh

database.o : database.cc database.hh db_engine.hh test.hh parola.hh kanji.hh forma.hh lezione.hh

drawing_kanji_sheet.o : drawing_kanji_sheet.cc drawing_kanji_sheet.hh

forma.o : forma.cc forma.hh test.hh sintagma.hh parola.hh

kakikata_sheet.o : kakikata_sheet.cc kakikata_sheet.hh drawing_kanji_sheet.hh

kanji.o : kanji.cc kanji.hh test.hh

lezione.o : lezione.cc lezione.hh

oboeru_sheet.o : oboeru_sheet.cc oboeru_sheet.hh drawing_kanji_sheet.hh

parola.o : parola.cc parola.hh test.hh

print_kanji_table_operation.o : print_kanji_table_operation.cc print_kanji_table_operation.hh

sintagma.o : sintagma.cc sintagma.hh parola.hh

test_controller.o : test_controller.cc test_controller.hh ctrl_engine.hh combobox_tmcr.hh parole_tmcr.hh kanji_tmcr.hh forma.hh lezioni_tmcr.hh kanji.hh parola.hh test.hh lezione.hh database.hh test_inserimento_parole.hh test_scelta_mult_parole.hh test_scelta_mult_kanji.hh test_inserimento_numeri.hh drawing_kanji_sheet.hh kakikata_sheet.hh oboeru_sheet.hh print_kanji_table_operation.hh

test_engine.o : test_engine.cc test_engine.hh parola.hh kanji.hh test.hh

test_inserimento.o : test_inserimento.cc test_inserimento.hh test_engine.hh

test_inserimento_numeri.o : test_inserimento_numeri.cc test_inserimento_numeri.hh test_inserimento.hh test_engine.hh

test_inserimento_parole.o : test_inserimento_parole.cc test_inserimento_parole.hh test_inserimento.hh test_engine.hh parola.hh test.hh

test_scelta_mult.o : test_scelta_mult.cc test_scelta_mult.hh test_engine.hh

test_scelta_mult_kanji.o : test_scelta_mult_kanji.cc test_scelta_mult_kanji.hh test_scelta_mult.hh test_engine.hh kanji.hh test.hh

test_scelta_mult_parole.o : test_scelta_mult_parole.cc test_scelta_mult_parole.hh test_scelta_mult.hh test_engine.hh parola.hh test.hh

test.o : test.cc test.hh

# pulizia
.PHONY: clean
clean:
	rm -f core $(EXE) *.o

install:
	mkdir -p $(DESTDIR)/usr/bin
	strip --strip-unneeded --remove-section=.comment --remove-section=.note $(EXE)
	install -m 0755 $(EXE) $(DESTDIR)/usr/bin/$(EXE)
	mkdir -p $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 nihongo.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 icons/256x256/$(EXE).png $(DESTDIR)/usr/share/$(EXE)/ui/$(EXE).png
	install -m 0644 $(EXE).desktop $(DESTDIR)/usr/share/applications
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/48x48/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/48x48/apps
	install -m 0644 icons/48x48/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/48x48/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/64x64/apps
	install -m 0644 icons/64x64/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/64x64/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/72x72/apps
	install -m 0644 icons/72x72/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/72x72/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/96x96/apps
	install -m 0644 icons/96x96/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/96x96/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/128x128/apps
	install -m 0644 icons/128x128/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/128x128/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/192x192/apps
	install -m 0644 icons/192x192/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/192x192/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/256x256/apps
	install -m 0644 icons/256x256/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/256x256/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/512x512/apps
	install -m 0644 icons/512x512/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/512x512/apps
	gtk-update-icon-cache /usr/share/icons/hicolor

uninstall:
	rm -f $(DESTDIR)/usr/bin/$(EXE)
	rm -f -R $(DESTDIR)/usr/share/$(EXE)
	rm -f $(DESTDIR)/usr/share/applications/$(EXE).desktop
	rm -f $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/64x64/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/72x72/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/96x96/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/192x192/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/256x256/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/512x512/apps/$(EXE).png
