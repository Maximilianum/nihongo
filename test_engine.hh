/*
 * test_engine.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TESTENGINE_HH_
#define _TESTENGINE_HH_
#include <gtkmm.h>
#include <random>
#include "parola.hh"
#include "kanji.hh"

enum class TestKanjiMode { memorization, reading };
enum class TestNumbersMode { cardinal, quantity, calendar };

typedef struct {
  TestType tst_type;
  TestKanjiMode tstk_mode;
  TestNumbersMode tstn_mode;
  short verso;
  short conj_style;
  short conj_negative;
  VbConjug vconj;
  AjConjug aconj;
  short max_val;
  short quest_nr;
} TestOptions;

class TestEngine {
 public:
  static void update_random_seed(void);
  static int random(int min, int max);
  static bool random_bool(void);
  template<class Type>
  static void shuffle(std::vector<Type>* vec);
  inline ~TestEngine() {}
  virtual void run_test(const TestOptions& opts) = 0;
  typedef sigc::signal<void> Type_signal_window_closed;
  inline Type_signal_window_closed signal_window_closed(void) { return window_closed; }
 protected:
  static std::minstd_rand0 rnd_generator;
  Gtk::Window* main_win;
  sigc::connection main_win_connection;
  sigc::connection timer_connection;
  int timer;
  unsigned int nr_quest;
  TestOptions options;
  bool paused;
  Type_signal_window_closed window_closed;
  // toolbar
  Gtk::Label *question_nr_label;
  Gtk::Label *question_tot_label;
  Gtk::ToolButton *skip_toolbutton;
  sigc::connection skip_connection;
  Gtk::ToolButton *pause_toolbutton;
  sigc::connection pause_connection;
  Gtk::Label *time_label;
  // main
  Gtk::Label *question_label;
  Gtk::Label *upquestion_label;
  
  inline TestEngine() {}
  void init_test_engine(Glib::RefPtr<Gtk::Builder> bldr);
  void connect_toolbar_signals(void);
  bool on_timer_fired(void);
  void on_skip_clicked(void);
  void on_pause_clicked(void);
  virtual void connect_signals(void) = 0;
  virtual void on_window_closed(void) = 0;
  virtual void update_window(void) = 0;
  virtual void on_risposta(short tasto = -1) = 0;
  virtual void pause_test(void) = 0;
  virtual void continue_test(void) = 0;
  virtual void next_question(void) = 0;
};

template<class Type>
void TestEngine::shuffle(std::vector<Type>* vec) {
  update_random_seed();
  long unsigned int half { vec->size() / 2 };
  long unsigned int max { vec->size() - 1 };
  for (long unsigned int i = 0; i < half; i++) {
    int src { random(0, half - 1) };
    int dst { random(half, max)};
    Type item { vec->at(dst) };
    vec->at(dst) = vec->at(src);
    vec->at(src) = item;
  }
}

#endif // _TESTENGINE_HH_
