/*
 * kakikata_sheet.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _KAKIKATASHEET_HH_
#define _KAKIKATASHEET_HH_

#include "drawing_kanji_sheet.hh"

class KakikataSheet: public DrawingKanjiSheet {
 public:
  KakikataSheet();
  inline int count_pagine(void) { return (kanji.size() / 40) + ((kanji.size() % 40) > 0); }
  void set_up_kanji_table(const Cairo::RefPtr<Cairo::Context>& cr);
  void draw_kanji_grid(const Cairo::RefPtr<Cairo::Context>& cr, double dy = 0.0);
  void draw_kanji_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int pagina, bool pages = false);
};

#endif // _KAKIKATASHEET_HH_
