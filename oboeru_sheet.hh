/*
 * oboeru_sheet.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OBOERUSHEET_H_
#define _OBOERUSHEET_H_

#include "drawing_kanji_sheet.hh"

#define KNJXPAGE 56

class OboeruSheet: public DrawingKanjiSheet {
public:
  OboeruSheet();
  inline int count_pagine( void );
  void set_up_kanji_table(const Cairo::RefPtr<Cairo::Context>& cr);
  void draw_kanji_grid(const Cairo::RefPtr<Cairo::Context>& cr, double dy = 0.0);
  void draw_kanji_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int pagina, bool pages = false);
private:
  Glib::ustring get_first_word(const Glib::ustring &str);

};

#endif // _OBOERUSHEET_H_
