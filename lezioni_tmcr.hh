/*
 * lezioni_tmcr.hh
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LEZIONI_TREE_MODEL_COLUMN_RECORD_HH_
#define _LEZIONI_TREE_MODEL_COLUMN_RECORD_HH_
#include <gtkmm.h>

class LezioniTreeModelColumnRecord : public Gtk::TreeModelColumnRecord {
 public:
  LezioniTreeModelColumnRecord() { add (idl);
    add(name);
    add(parole);
    add(kanji);
    add(patterns);
    add(editable);
    add(type);
    add(fsize); }
  Gtk::TreeModelColumn<Glib::ustring> idl;
  Gtk::TreeModelColumn<Glib::ustring> name;
  Gtk::TreeModelColumn<Glib::ustring> parole;
  Gtk::TreeModelColumn<Glib::ustring> kanji;
  Gtk::TreeModelColumn<Glib::ustring> patterns;
  Gtk::TreeModelColumn<bool> editable;
  Gtk::TreeModelColumn<char> type;
  Gtk::TreeModelColumn<double> fsize;
};

#endif // _LEZIONI_TREE_MODEL_COLUMN_RECORD_HH_
