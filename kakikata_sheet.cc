/*
 * kakikata_sheet.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kakikata_sheet.hh"

KakikataSheet::KakikataSheet() {
  left_margin = 10.0;
  right_margin = 10.0;
  up_margin = 10.0;
  down_margin = 10.0;
  cWidth = 40.0;
  cHeight = 40.0;
  pWidth = 580.0;
  pHeight = 820.0;
  set_has_window(true);
}

void KakikataSheet::set_up_kanji_table(const Cairo::RefPtr<Cairo::Context>& cr) {
  cr->select_font_face("UmeMincho", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_BOLD);
  cr->set_font_size(28.0);
  cr->set_source_rgb(0.0, 0.0, 0.0);
}

void KakikataSheet::draw_kanji_grid(const Cairo::RefPtr<Cairo::Context>& cr, double dy) {
  cr->set_line_width(2.0);
  cr->unset_dash();
  cr->move_to(left_margin, up_margin + dy);
  cr->line_to(left_margin, pHeight - down_margin + dy);
  cr->line_to(pWidth - right_margin, pHeight - down_margin + dy);
  cr->line_to(pWidth - right_margin, up_margin + dy);
  cr->line_to(left_margin, up_margin + dy);
  for (short i = 1; i < 21; i++) {
    cr->move_to(left_margin, up_margin + (cHeight * i) + dy);
    cr->line_to(pWidth - right_margin, up_margin + (cHeight * i) + dy);
  }
  for (short i = 1; i < 15; i++) {
    cr->move_to(left_margin + (cWidth * i), up_margin + dy);
    cr->line_to(left_margin + (cWidth * i), pHeight - down_margin + dy);
  }
  cr->stroke();
  cr->set_line_width(0.1);
  cr->set_dash(std::vector<double>{ 2.5, 5.0 }, 1.0);
  for (short i = 1; i < 21; i++) {
    cr->move_to(left_margin, up_margin + (cHeight * i) - (cHeight / 2.0) + dy);
    cr->line_to(pWidth - right_margin, up_margin + (cHeight * i) - (cHeight / 2.0) + dy);
  }
  for (short i = 1; i < 15; i++) {
    cr->move_to(left_margin + (cWidth * i) - (cWidth / 2.0), up_margin + dy);
    cr->line_to(left_margin + (cWidth * i) - (cWidth / 2.0), pHeight - down_margin + dy);
  }
  cr->stroke();
}

void KakikataSheet::draw_kanji_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int pagina, bool pages) {
  double dy { pages ? pHeight * pagina : 0.0 };
  Cairo::TextExtents te;
  std::vector<Kanji>::iterator itknj { kanji.begin() };
  for (int nk = 0; nk < pagina * 40; nk++) {
    itknj++;
  }
  for (short nk = 1; nk < 41; nk++) {
    if (itknj != kanji.end()) {
      cr->get_text_extents(itknj->get_kanji().raw(), te);
      int x = nk < 21 ? left_margin + ((cWidth - te.x_advance) / 2.0) : left_margin + (cWidth * 7) + ((cWidth - te.x_advance) / 2.0);
      int y = nk < 21 ? up_margin + (cHeight * nk) - ((cHeight - te.height) / 2.0) + dy : up_margin + (cHeight * (nk - 20)) - ((cHeight - te.height) / 2.0) + dy;
      cr->move_to(x, y);
      cr->show_text(itknj->get_kanji().raw());
      itknj++;
    }
    if (itknj == kanji.end()) {
      break;
    }
  }
}
