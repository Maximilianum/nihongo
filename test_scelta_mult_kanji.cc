/*
 * test_scelta_mult_kanji.cc
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_scelta_mult_kanji.hh"

TestSceltaMultKanji::TestSceltaMultKanji(Glib::RefPtr<Gtk::Builder> bldr, Database* db, const std::vector<long long int>& vec) : database (db), content (vec) {
  shuffle<long long int>(&content);
  current = content.begin();
  init_test_scelta_mult(bldr);
  connect_signals();
}

void TestSceltaMultKanji::run_test(const TestOptions& opts) {
  question_tot_label->set_text(Glib::ustring::format(content.size()));
  main_win->present();
  timer = 0;
  nr_quest = 0;
  options= opts;
  update_window();
}

void TestSceltaMultKanji::update_window(void) {
  Glib::ustring question;
  try {
    current_kanji = database->get_kanji(*current);
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestSceltaMultKanji::update_window() " << dbe.get_message() << std::endl;
  }
  question_nr_label->set_text(Glib::ustring::format(nr_quest + 1));
  timer_connection = Glib::signal_timeout().connect_seconds(sigc::mem_fun(*this, &TestSceltaMultKanji::on_timer_fired), 1);
  time_label->set_text("00 : 00");
  switch (options.tstk_mode) {
  case TestKanjiMode::memorization:
    if (options.verso == KNJ_SIGN) {
      question = current_kanji.get_kanji();
    } else {
      question = current_kanji.get_significato();
    }
    break;
  case TestKanjiMode::reading:
    question = current_kanji.get_kanji();
    break;
  }
  question_label->set_text(question);
  update_answers();
}

void TestSceltaMultKanji::update_answers(void) {
  short right = TestEngine::random(0, 5);
  right_answers.clear();
  right_answers.insert(right_answers.begin(), right);
  Glib::ustring risposte[6];
  Kanji knj;
  for (short n = 0; n < 6; n++) {
    if (right_answers.count(n) > 0) {
      knj = current_kanji;
    } else {
      try {
	knj = database->get_kanji_random();
      } catch (const Database::DBError& dbe) {
	std::cerr << "TestSceltaMultKanji::update_answers() " << dbe.get_message() << std::endl;
      }
      if (knj == current_kanji) {
	right_answers.insert(right_answers.begin(), n);
      }
    }
    risposte[n] = create_label(knj);
  }
  answer1_button->set_label(risposte[0]);
  answer2_button->set_label(risposte[1]);
  answer3_button->set_label(risposte[2]);
  answer4_button->set_label(risposte[3]);
  answer5_button->set_label(risposte[4]);
  answer6_button->set_label(risposte[5]);
  risposta_esatta.assign(risposte[*(right_answers.begin())]);
}

Glib::ustring TestSceltaMultKanji::create_label(const Kanji &knj) {
  switch (options.tstk_mode) {
  case TestKanjiMode::memorization:
    if (options.verso == KNJ_SIGN) {
      return knj.get_significato();
    } else {
      return knj.get_kanji();
    }
    break;
  case TestKanjiMode::reading:
    return Glib::ustring::compose("%1\n%2", knj.get_onyomi(), knj.get_kunyomi());
  }
  return Glib::ustring();
}

void TestSceltaMultKanji::on_risposta(short tasto) {
  bool risultato = right_answers.count(tasto) > 0;
  timer_connection.disconnect();
  try {
    Record rec { current_kanji.test(options.tst_type, risultato) };
    database->update(rec, current_kanji.get_idv(), options.tst_type);
  } catch (const Database::DBError& dbe) {
    std::cerr << "TestSceltaMultKanji::on_risposta() " << dbe.get_message() << std::endl;
  }
  if (!risultato) {
    Glib::ustring msg { "Sbagliato, la risposta giusta è\n<b><big>" + risposta_esatta + "</big></b>" };
    Gtk::MessageDialog *alert { new Gtk::MessageDialog(*main_win, "", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
    alert->set_message(msg, true);
    int res_id = alert->run();
    delete alert;
  }
  next_question();
}

void TestSceltaMultKanji::next_question(void) {
  timer = 0;
  nr_quest++;
  current++;
  if (current == content.end()) {
    main_win->hide();
  } else {
    update_window();
  }
}

