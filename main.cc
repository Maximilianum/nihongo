/*
 * main.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtkmm.h>
#include <iostream>
#include "controller.hh"

#ifdef ENABLE_NLS
#include <libintl.h>
#endif

int main (int argc, char *argv[]) {
  Glib::ustring appname { APP_FILENAME };
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(Glib::ustring("org.maniscalco.") + appname, Gio::APPLICATION_FLAGS_NONE);
  Glib::ustring main_dir_path (Glib::get_home_dir() + Glib::ustring("/.local/share/") + appname);
  Glib::RefPtr<Gio::File> main_dir = Gio::File::create_for_path(main_dir_path);
  // try to create the main directory in .local/share/
  try {
    main_dir->make_directory();
  } catch (const Gio::Error& error) {
    if (error.code() != Gio::Error::EXISTS) {
      std::cerr << error.what() << std::endl;
    }
  }
  Glib::ustring ui_filename { Glib::ustring::compose("/usr/share/%1/ui/%1.ui", appname) };
  for (int i = 1; i < argc; i++) {
    Glib::ustring option { argv[i] };
    if (option.compare(Glib::ustring("--devel")) == 0 || option.compare(Glib::ustring("-d")) == 0) {
      ui_filename.assign(Glib::ustring::compose("%1.ui", appname));
    }
  }
  //Load the Glade file and instiate its widgets:
  Glib::RefPtr<Gtk::Builder> builder;
  try {
    builder = Gtk::Builder::create_from_file(ui_filename);
  } catch (const Glib::FileError & ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  Controller* ctrl = new Controller(builder, app, main_dir_path);
  if (ctrl) {
    Gtk::Window* win {ctrl->get_main_window()};
    if (win) {
      app->run(*win);
    }
  }
  delete ctrl;
  return 0;
}
