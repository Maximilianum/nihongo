/*
 * kanji_tmcr.hh
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _KANJI_TREE_MODEL_COLUMN_RECORD_HH_
#define _KANJI_TREE_MODEL_COLUMN_RECORD_HH_
#include <gtkmm.h>

class KanjiTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
  KanjiTreeModelColumnRecord() { add(idk);
    add(kanji);
    add(tratti);
    add(radicale);
    add(trattir);
    add(kunyomi);
    add(onyomi);
    add(significato);
    add(fcolour);
    add(fsize); }
  Gtk::TreeModelColumn<long long int> idk;
  Gtk::TreeModelColumn<Glib::ustring> kanji;
  Gtk::TreeModelColumn<int> tratti;
  Gtk::TreeModelColumn<Glib::ustring> radicale;
  Gtk::TreeModelColumn<int> trattir;
  Gtk::TreeModelColumn<Glib::ustring> kunyomi;
  Gtk::TreeModelColumn<Glib::ustring> onyomi;
  Gtk::TreeModelColumn<Glib::ustring> significato;
  Gtk::TreeModelColumn<Gdk::RGBA> fcolour;
  Gtk::TreeModelColumn<double> fsize;
};

#endif // _KANJI_TREE_MODEL_COLUMN_RECORD_HH_
