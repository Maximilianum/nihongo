/*
 * database.cc
 * Copyright (C) 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.hh"
#include <chrono>
#include <random>

Database::Database(const Glib::ustring& db_path) {
  // try to open an existing database
  int result = sqlite3_open_v2(db_path.c_str(), &sqlt_db, SQLITE_OPEN_READWRITE, NULL);
  if (result != SQLITE_OK) {
    // create a new database
    result = sqlite3_open_v2(db_path.c_str(), &sqlt_db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE, NULL);
    if (result == SQLITE_OK) {
      origin = DATABASE_ORIGIN::created;
      // create SQL TABLE TEST
      std::stringstream ss;
      ss << "CREATE TABLE " << Test::get_sqlite_test_name() << " " << Test::get_sqlite_test_table();
      std::string sql { ss.str() };
      int rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE RECORD
      ss.str("");
      ss << "CREATE TABLE " << Test::get_sqlite_record_name() << " " << Test::get_sqlite_record_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE WORD
      ss.str("");
      ss << "CREATE TABLE " << Parola::get_sqlite_word_name() << " " << Parola::get_sqlite_word_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE KAJI
      ss.str("");
      ss << "CREATE TABLE " << Kanji::get_sqlite_kanji_name() << " " << Kanji::get_sqlite_kanji_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE PATTERN
      ss.str("");
      ss << "CREATE TABLE " << Forma::get_sqlite_pattern_name() << " " << Forma::get_sqlite_pattern_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE LESSON
      ss.str("");
      ss << "CREATE TABLE " << Lezione::get_sqlite_lesson_name() << " " << Lezione::get_sqlite_lesson_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE LWORD
      ss.str("");
      ss << "CREATE TABLE " << Lezione::get_sqlite_lword_name() << " " << Lezione::get_sqlite_lword_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE LKANJI
      ss.str("");
      ss << "CREATE TABLE " << Lezione::get_sqlite_lkanji_name() << " " << Lezione::get_sqlite_lkanji_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE LPATTERN
      ss.str("");
      ss << "CREATE TABLE " << Lezione::get_sqlite_lpattern_name() << " " << Lezione::get_sqlite_lpattern_table();
      sql = ss.str();
      rc = Database::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
    } else {
      std::cerr << "Can't open database: " << sqlite3_errmsg(sqlt_db) << std::endl;
    }
  } else {
    origin = DATABASE_ORIGIN::opened;
  }
}

/*
 * test
 * record
 */

Test Database::get_test(const long long id) {
  std::string table_name { Test::get_sqlite_test_name() };
  std::vector<Test> voci;
  std::stringstream ss;
  ss << "SELECT * from " << table_name << " where IDV='" << id << "' ORDER BY IDV ASC;";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, Test::retrieve_test, &voci) == SQLITE_OK) {
    if (voci.size() > 0) {
      return voci.front();
    } else {
      throw DBEmptyResultException { std::string { "Database::get_test() couldn't find voce" } };
    }
  } else {
    throw DBError { std::string { "Database::get_test() exec failed!" } };
  }
}

std::map<TestType,Record> Database::get_records(const long long int id) {
  std::map<TestType,Record> records;
  std::stringstream ss;
  ss << "SELECT * from " << Test::get_sqlite_record_name() << " where RIFFT=" << id << " ORDER BY IDR ASC;";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, Test::retrieve_record, &records) == SQLITE_OK) {
    return records;
  } else {
    throw DBError { std::string { "Database::get_records() exec failed!" } };
  }
}

long long int Database::insert(const Test& vc, ItemType it) {
  std::stringstream ss;
  ss << "INSERT INTO " << Test::get_sqlite_test_name() << " (TDATE) VALUES ('" << time(nullptr) << "');";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert(Test) exec failed!" } };
  }
  sqlite3_int64 idv = sqlite3_last_insert_rowid(sqlt_db);
  TestType *types;
  int nt;
  if (it == ItemType::word) {
    TestType tps[5] { TestType::mem_wrd_sel, TestType::cnj_sel, TestType::cnj_ins, TestType::read_wrd_sel, TestType::read_wrd_ins };
    types = tps;
    nt = 5;
  } else if (it == ItemType::kanji) {
    TestType tps[1] { TestType::mem_knj_sel };
    types = tps;
    nt = 1;
  } else if (it == ItemType::pattern) {
    nt = 0;
  }
  for (short i = 0; i < nt; i++) {
    Record rec { 0, 0, 0, false };
    insert(rec, idv, types[i]);
  }
  return idv;
}

void Database::insert(const Record& rec, long long int id, TestType tt) {
  std::stringstream ss;
  ss << "INSERT INTO " << Test::get_sqlite_record_name() << " (RIFFT,RTYPE,CORRECT,TOTAL,DATE,ERROR) VALUES ('";
  ss << id << "','" << Test::get_test_type_id(tt) << "','" << rec.correct << "','" << rec.total << "','" << rec.date << "','" << rec.error << "');";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert(Record) exec failed!" } };
  }
}

void Database::update(const Record& rec, long long int id, TestType tt) {
  std::stringstream ss;
  ss << "UPDATE " << Test::get_sqlite_record_name() << " set CORRECT=" << rec.correct << ", TOTAL=" << rec.total;
  ss << ", DATE=" << rec.date << ", ERROR=" << rec.error << " where RIFFT=" << id << " and RTYPE='" << Test::get_test_type_id(tt) << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update(Record) exec failed!" } };
  }
}

void Database::delete_record(const long long int id, TestType tt) {
  std::stringstream ss;
  ss << "DELETE from " << Test::get_sqlite_record_name() << " where RIFFT=" << id << " and RTYPE='" << Test::get_test_type_id(tt).raw() << "'; ";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_record() exec failed!" } };
  }
}

void Database::delete_test(const long long int id) {
  std::stringstream ss;
  ss << "DELETE from " << Test::get_sqlite_record_name() << " where RIFFT='" << id << "'; ";
  ss << "DELETE from " << Test::get_sqlite_test_name() << " where IDV='" << id << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_test() exec failed!" } };
  }
}

/*
 * parole
 */

std::vector<Parola> Database::get_parole(const Glib::ustring& where) {
  std::vector<Parola> parole;
  Glib::ustring sql { Glib::ustring("SELECT "			\
				    "WORD.IDW, "			\
				    "WORD.WTYPE, "			\
				    "WORD.KOTOBA, "			\
				    "WORD.YOMIKATA, "			\
				    "WORD.ITARIAGO, "			\
				    "WORD.STATS") };
  bool has_record { where.find(Test::get_sqlite_record_name()) != Glib::ustring::npos };
  bool has_lword { where.find(Lezione::get_sqlite_lword_name()) != Glib::ustring::npos };
  if (has_record) {
    sql.append(Glib::ustring(", "		\
			     "RECORD.RIFFT, "				\
			     "RECORD.RTYPE, "				\
			     "RECORD.CORRECT, "				\
			     "RECORD.TOTAL, "				\
			     "RECORD.DATE, "				\
			     "RECORD.ERROR"));
  }
  if (has_lword) {
    sql.append(Glib::ustring(", "					\
			      "LWORD.RIFFL, "				\
			      "LWORD.RIFFW"));
  }
  sql.append(Glib::ustring(" from WORD "));
  if (has_record) {
    sql.append("inner join RECORD on WORD.STATS = RECORD.RIFFT ");
  }
  if (has_lword) {
    sql.append("inner join LWORD on WORD.IDW = LWORD.RIFFW ");
  }
  sql.append(Glib::ustring::compose("%1 ORDER BY IDW ASC;", where));
  if (Database::exec(sqlt_db, sql, Parola::retrieve_parola, &parole) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_parole() exec failed!" } };
  }
  return parole;
}

std::vector<long long int> Database::get_id_parole(const Glib::ustring& where) {
  std::vector<long long int> parole;
  Glib::ustring sql { Glib::ustring("SELECT WORD.IDW from WORD inner join RECORD on WORD.STATS = RECORD.RIFFT ") };
  if (where.find(Lezione::get_sqlite_lword_name()) != Glib::ustring::npos) {
    sql.append(Glib::ustring("inner join LWORD on WORD.IDW = LWORD.RIFFW "));
  }
  sql.append(Glib::ustring::compose("%1 ORDER BY IDW ASC;", where));
  if (Database::exec(sqlt_db, sql, Parola::retrieve_id, &parole) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_id_parole() exec failed!" } };
  }
  return parole;
}

Parola Database::get_parola(const long long int id) {
  std::vector<Parola> parole;
  Glib::ustring sql { Glib::ustring::compose("SELECT "			\
					     "WORD.IDW, "		\
					     "WORD.WTYPE, "		\
					     "WORD.KOTOBA, "		\
					     "WORD.YOMIKATA, "		\
					     "WORD.ITARIAGO, "		\
					     "WORD.STATS, "		\
					     "RECORD.RIFFT, "		\
					     "RECORD.RTYPE, "		\
					     "RECORD.CORRECT, "		\
					     "RECORD.TOTAL, "		\
					     "RECORD.DATE, "		\
					     "RECORD.ERROR from WORD "	\
					     "inner join RECORD on WORD.STATS = RECORD.RIFFT " \
					     "where WORD.IDW=%1 ORDER BY WORD.IDW ASC;", Glib::ustring::format(id)) };
  if (Database::exec(sqlt_db, sql, Parola::retrieve_parola, &parole) == SQLITE_OK) {
    if (parole.size() > 0) {
      return parole.front();
    } else {
      throw DBEmptyResultException { std::string { "Database::get_parola() couldn't find parola" } };
    }
  } else {
    throw DBError { std::string { "Database::get_parola() exec failed!" } };
  }
}

Parola Database::get_parola_random(const Glib::ustring& where) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::minstd_rand0 generator (seed);
  std::vector<Parola> words { get_parole(where) };
  std::uniform_int_distribution<int> distribution(0, words.size() - 1);
  return words[distribution(generator)];
}

void Database::insert(const Parola& wrd) {
  std::stringstream ss;
  ss << "INSERT INTO " << Test::get_sqlite_test_name() << " (TDATE) VALUES ('" << time(nullptr) << "');";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert(Test) exec failed!" } };
  }
  sqlite3_int64 idv = sqlite3_last_insert_rowid(sqlt_db);
  if (idv > 0) {
    std::vector<TestType> types { TestType::mem_wrd_sel };
    if (wrd.is_verbo() || wrd.is_aggettivo()) {
      types.insert(types.end(), { TestType::cnj_sel, TestType::cnj_ins });
    }
    if (wrd.has_kanji()) {
      types.insert(types.end(), { TestType::read_wrd_sel, TestType::read_wrd_ins });
    }
    std::vector<TestType>::const_iterator iter { types.cbegin() };
    while (iter != types.cend()) {
      Record rec { 0, 0, 0, false };
      insert(rec, idv, *iter);
      iter++;
    }
    ss.str("");
    ss << "INSERT INTO WORD (WTYPE,KOTOBA,YOMIKATA,ITARIAGO,STATS) VALUES (";
    ss << '\'' << Glib::ustring::format(wrd.get_wtype()) << "','" << get_sqlite_safe_string(wrd.get_kotoba()) << "','";
    ss << get_sqlite_safe_string(wrd.get_furigana()) << "','" << get_sqlite_safe_string(wrd.get_itariago()) << "','" << Glib::ustring::format(idv) << "');";
    Glib::ustring sql { ss.str() };
    if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
      throw DBError { std::string { "Database::insert(Parola) exec failed!" } };
    }
  }
}

void Database::update(const Parola& wrd) {
  std::stringstream ss;
  ss << "UPDATE WORD set WTYPE='" << wrd.get_wtype() << "', KOTOBA='" << get_sqlite_safe_string(wrd.get_kotoba()) << "', YOMIKATA='" << get_sqlite_safe_string(wrd.get_furigana()) << "', ITARIAGO='" << get_sqlite_safe_string(wrd.get_itariago()) << "' where IDW ='" << wrd.get_idw() << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update(Parola) exec failed!" } };
  }
  long long int idv = wrd.get_idv();
  std::map<TestType,Record> records = get_records(idv);
  if (records.find(TestType::cnj_sel) == records.end()) {
    if (wrd.is_verbo() || wrd.is_aggettivo()) {
      Record rec { 0, 0, 0, false };
      insert(rec, idv, TestType::cnj_sel);
      insert(rec, idv, TestType::cnj_ins);
    }
  } else {
    if (!wrd.is_verbo() && !wrd.is_aggettivo()) {
      delete_record(idv, TestType::cnj_sel);
      delete_record(idv, TestType::cnj_ins);
    }
  }
  if (records.find(TestType::read_wrd_sel) == records.end()) {
    if (wrd.has_kanji()) {
      Record rec { 0, 0, 0, false };
      insert(rec, idv, TestType::read_wrd_sel);
      insert(rec, idv, TestType::read_wrd_ins);
    }
  } else {
    if (!wrd.has_kanji()) {
      delete_record(idv, TestType::read_wrd_sel);
      delete_record(idv, TestType::read_wrd_ins);
    }
  }
}

void Database::delete_parola(const long long int id) {
  Parola wrd { get_parola(id) };
  delete_test(wrd.get_idv());
  std::stringstream ss;
  ss << "DELETE from WORD where IDW='" << id << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_parola() exec failed!" } };
  }
}

void Database::reset_parola(const long long int id) {
  Parola wrd { get_parola(id) };
  TestType tps[5] { TestType::mem_wrd_sel, TestType::cnj_sel, TestType::cnj_ins, TestType::read_wrd_sel, TestType::read_wrd_ins };
  wrd.reset_statistics();
  for (int i = 0; i < 5; i++) {
    Record rec { wrd.get_record(tps[i]) };
    update(rec, wrd.get_idv(), tps[i]);
  }
}

/*
 * kanji
 */

std::vector<Kanji> Database::get_kanji(const Glib::ustring& where) {
  std::vector<Kanji> kanji;
  Glib::ustring sql { Glib::ustring("SELECT "				\
				    "KANJI.IDK, "			\
				    "KANJI.KANJI, "			\
				    "KANJI.ONYOMI, "			\
				    "KANJI.KUNYOMI, "			\
				    "KANJI.MEANING, "			\
				    "KANJI.RADICAL, "			\
				    "KANJI.STROKE, "			\
				    "KANJI.STROKER, "			\
				    "KANJI.STATS") };
  bool has_record { where.find(Test::get_sqlite_record_name()) != Glib::ustring::npos };
  bool has_lkanji { where.find(Lezione::get_sqlite_lkanji_name()) != Glib::ustring::npos };
  if (has_record) {
    sql.append(Glib::ustring(", "		\
			     "RECORD.RIFFT, "				\
			     "RECORD.RTYPE, "				\
			     "RECORD.CORRECT, "				\
			     "RECORD.TOTAL, "				\
			     "RECORD.DATE, "				\
			     "RECORD.ERROR"));
  }
  if (has_lkanji) {
    sql.append(Glib::ustring(", "					\
			      "LKANJI.RIFFL, "				\
			      "LKANJI.RIFFK"));
  }
  sql.append(Glib::ustring(" from KANJI "));
  if (has_record) {
    sql.append("inner join RECORD on KANJI.STATS = RECORD.RIFFT ");
  }
  if (has_lkanji) {
    sql.append("inner join LKANJI on KANJI.IDK = LKANJI.RIFFK ");
  }
  sql.append(Glib::ustring::compose("%1 ORDER BY IDK ASC;", where));
  if (Database::exec(sqlt_db, sql, Kanji::retrieve_kanji, &kanji) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_kanji() exec failed!" } };
  }
  return kanji;
}

std::vector<long long int> Database::get_id_kanji(const Glib::ustring& where) {
  std::vector<long long int> kanji;
  Glib::ustring sql { Glib::ustring("SELECT KANJI.IDK from KANJI inner join RECORD on KANJI.STATS = RECORD.RIFFT ") };
  if (where.find(Lezione::get_sqlite_lword_name()) != Glib::ustring::npos) {
    sql.append(Glib::ustring("inner join LKANJI on KANJI.IDK = LKANJI.RIFFK "));
  }
  sql.append(Glib::ustring::compose("%1 ORDER BY IDK ASC;", where));
  if (Database::exec(sqlt_db, sql, Kanji::retrieve_id, &kanji) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_id_kanji() exec failed!" } };
  }
  return kanji;
}

Kanji Database::get_kanji(const long long int id) {
  std::vector<Kanji> kanji;
  Glib::ustring sql { Glib::ustring::compose("SELECT "			\
					     "KANJI.IDK, "		\
					     "KANJI.KANJI, "		\
					     "KANJI.ONYOMI, "		\
					     "KANJI.KUNYOMI, "		\
					     "KANJI.MEANING, "		\
					     "KANJI.RADICAL, "		\
					     "KANJI.STROKE, "		\
					     "KANJI.STROKER, "		\
					     "KANJI.STATS, "		\
					     "RECORD.RIFFT, "		\
					     "RECORD.RTYPE, "		\
					     "RECORD.CORRECT, "		\
					     "RECORD.TOTAL, "		\
					     "RECORD.DATE, "		\
					     "RECORD.ERROR from KANJI "	\
					     "inner join RECORD on KANJI.STATS = RECORD.RIFFT " \
					     "where KANJI.IDK=%1 ORDER BY KANJI.IDK ASC;", Glib::ustring::format(id)) };
  if (Database::exec(sqlt_db, sql, Kanji::retrieve_kanji, &kanji) == SQLITE_OK) {
    if (kanji.size() > 0) {
      return kanji.front();
    } else {
      throw DBEmptyResultException { std::string { "Database::get_kanji() couldn't find kanji" } };
    }
  } else {
    throw DBError { std::string { "Database::get_kanji() exec failed!" } };
  }
}

Kanji Database::get_kanji_random(void) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::minstd_rand0 generator (seed);
  unsigned max = count_rows("KANJI");
  std::uniform_int_distribution<int> distribution(1,max);
  return get_kanji(distribution(generator));
}

void Database::insert(const Kanji& knj) {
  Test vox;
  sqlite3_int64 idv = insert(vox, ItemType::kanji);
  if (idv > 0) {
    std::stringstream ss;
    ss << "INSERT INTO KANJI (KANJI,ONYOMI,KUNYOMI,MEANING,RADICAL,STROKE,STROKER,STATS) VALUES (";
    ss << '\'' << get_sqlite_safe_string(knj.get_kanji()) << "','" << get_sqlite_safe_string(knj.get_onyomi()) << "','" << get_sqlite_safe_string(knj.get_kunyomi()) << "','";
    ss << get_sqlite_safe_string(knj.get_significato()) << "','" << get_sqlite_safe_string(knj.get_radicale()) << "','" << knj.get_tratti() << "','";
    ss << knj.get_tratti_radicale() << "','" << idv << "');";
    Glib::ustring sql { ss.str() };
    if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
      throw DBError { std::string { "Database::insert(Kanji) exec failed!" } };
    }
  }
}

void Database::update(const Kanji& knj) {
  std::stringstream ss;
  ss << "UPDATE KANJI set KANJI='" << get_sqlite_safe_string(knj.get_kanji()) << "', ONYOMI='" << get_sqlite_safe_string(knj.get_onyomi()) << "', KUNYOMI='" << get_sqlite_safe_string(knj.get_kunyomi()) << "', MEANING='" << get_sqlite_safe_string(knj.get_significato());
  ss << "', RADICAL ='" << get_sqlite_safe_string(knj.get_radicale()) << "', STROKE='" << knj.get_tratti() << "', STROKER='" << knj.get_tratti_radicale() << "' where IDK='" << knj.get_idk() << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update_kanji() exec failed!" } };
  }
}

void Database::delete_kanji(const long long int id) {
  Kanji knj { get_kanji(id) };
  delete_test(knj.get_idv());
  std::stringstream ss;
  ss << "DELETE from KANJI where IDK='" << id << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_kanji() exec failed!" } };
  }
}

void Database::reset_kanji(const long long int id) {
  Kanji knj { get_kanji(id) };
  TestType tps[1] { TestType::mem_knj_sel };
  knj.reset_statistics();
  for (int i = 0; i < 1; i++) {
    Record rec { knj.get_record(tps[i]) };
    update(rec, knj.get_idv(), tps[i]);
  }
}

/*
 * forme
 */

std::vector<Forma> Database::get_forme(const Glib::ustring& where) {
  std::vector<Forma> forme;
  Glib::ustring sql { Glib::ustring("SELECT "				\
				    "PATTERN.IDF, "			\
				    "PATTERN.FNAME, "			\
				    "PATTERN.TOKENS, "			\
				    "PATTERN.NOTE, "			\
				    "PATTERN.STATS") };
  bool has_record { where.find(Test::get_sqlite_record_name()) != Glib::ustring::npos };
  //bool has_lword { where.find(Lezione::get_sqlite_lword_name()) != Glib::ustring::npos };
  if (has_record) {
    sql.append(Glib::ustring(", "		\
			     "RECORD.RIFFT, "				\
			     "RECORD.RTYPE, "				\
			     "RECORD.CORRECT, "				\
			     "RECORD.TOTAL, "				\
			     "RECORD.DATE, "				\
			     "RECORD.ERROR"));
  }
  /*if (has_lword) {
    sql.append(Glib::ustring(", "					\
			      "LWORD.RIFFL, "				\
			      "LWORD.RIFFW"));
			      }*/
  sql.append(Glib::ustring(" from PATTERN "));
  if (has_record) {
    sql.append("inner join RECORD on PATTERN.STATS = RECORD.RIFFT ");
  }
  /*if (has_lword) {
    sql.append("inner join LWORD on WORD.IDW = LWORD.RIFFW ");
    }*/
  sql.append(Glib::ustring::compose("%1 ORDER BY IDF ASC;", where));
  if (Database::exec(sqlt_db, sql, Forma::retrieve_forma, &forme) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_forme() exec failed!" } };
  }
  return forme;
}

Forma Database::get_forma(const long long id) {
  std::vector<Forma> forme;
  /*Glib::ustring sql { Glib::ustring::compose("SELECT "		\
					     "PATTERN.IDF, "		\
					     "PATTERN.FNAME, "		\
					     "PATTERN.TOKENS, "		\
					     "PATTERN.NOTE, "		\
					     "PATTERN.STATS, "		\
					     "RECORD.RIFFT, "		\
					     "RECORD.RTYPE, "		\
					     "RECORD.CORRECT, "		\
					     "RECORD.TOTAL, "		\
					     "RECORD.DATE, "		\
					     "RECORD.ERROR from PATTERN " \
					     "inner join RECORD on PATTERN.STATS = RECORD.RIFFT " \
					     "where IDF=%1 ORDER BY IDF ASC;", Glib::ustring::format(id)) };*/
  Glib::ustring sql { Glib::ustring::compose("SELECT "			\
					     "PATTERN.IDF, "		\
					     "PATTERN.FNAME, "		\
					     "PATTERN.TOKENS, "		\
					     "PATTERN.NOTE, "		\
					     "PATTERN.STATS from PATTERN " \
					     "where IDF=%1 ORDER BY IDF ASC;", Glib::ustring::format(id)) };
  if (Database::exec(sqlt_db, sql, Forma::retrieve_forma, &forme) == SQLITE_OK) {
    if (forme.size() > 0) {
      return forme.front();
    } else {
      throw DBEmptyResultException { std::string { "Database::get_forma() couldn't find forma" } };
    }
  } else {
    throw DBError { std::string { "Database::get_forma() exec failed!" } };
  }
}

void Database::insert(const Forma& frm) {
  Test vox;
  sqlite3_int64 idv = insert(vox, ItemType::pattern);
  if (idv > 0) {
    std::stringstream ss;
    ss << "INSERT INTO PATTERN (FNAME,TOKENS,NOTE,STATS) VALUES (";
    ss << '\'' << get_sqlite_safe_string(frm.get_name()) << "','" << get_sqlite_safe_string(frm.get_pattern_string()) << "','";
    ss << get_sqlite_safe_string(frm.get_note()) << "','" << Glib::ustring::format(idv) << "');";
    Glib::ustring sql { ss.str() };
    if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
      throw DBError { std::string { "Database::insert(Forma) exec failed!" } };
    }
  }
}

void Database::update(const Forma& frm) {
  std::stringstream ss;
  ss << "UPDATE PATTERN set FNAME='" << get_sqlite_safe_string(frm.get_name()) << "', TOKENS='" << frm.get_pattern_string() << "', NOTE='" << get_sqlite_safe_string(frm.get_note()) << "' where IDF='" << frm.get_idf() << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update(Forma) exec failed!" } };
  }
}

void Database::delete_forma(const long long int id) {
  Forma frm { get_forma(id) };
  delete_test(frm.get_idv());
  std::stringstream ss;
  ss << "DELETE from PATTERN where IDF='" << id << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_forma() exec failed!" } };
  }
}

/*
 * lezioni
 */

std::vector<Lezione> Database::get_lezioni(const Glib::ustring& where) {
  std::vector<Lezione> lezioni;
  Glib::ustring sql { Glib::ustring::compose("SELECT * from LESSON %1 ORDER BY IDL ASC;", where) };
  if (Database::exec(sqlt_db, sql, Lezione::retrieve_lezione, &lezioni) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_lezioni() exec failed!" } };
  }
  std::vector<Lezione>::iterator iter { lezioni.begin() };
  while (iter != lezioni.end()) {
    std::set<long long int> links;
    sql = Glib::ustring::compose("SELECT RIFFW from LWORD where RIFFL=%1 ORDER BY RIFFW ASC;", Glib::ustring::format(iter->get_idl()));
    if (Database::exec(sqlt_db, sql, Lezione::retrieve_links, &links) == SQLITE_OK) {
      if (links.size() > 0) {
	iter->set_parole(links);
      }
    } else {
      throw DBError { std::string { "Database::get_lezioni() exec failed!" } };
    }
    links.clear();
    sql = Glib::ustring::compose("SELECT RIFFK from LKANJI where RIFFL=%1 ORDER BY RIFFK ASC;", Glib::ustring::format(iter->get_idl()));
    if (Database::exec(sqlt_db, sql, Lezione::retrieve_links, &links) == SQLITE_OK) {
      if (links.size() > 0) {
	iter->set_kanji(links);
      }
    } else {
      throw DBError { std::string { "Database::get_lezioni() exec failed!" } };
    }
    links.clear();
    sql = Glib::ustring::compose("SELECT RIFFP from LPATTERN where RIFFL=%1 ORDER BY RIFFP ASC;", Glib::ustring::format(iter->get_idl()));
    if (Database::exec(sqlt_db, sql, Lezione::retrieve_links, &links) == SQLITE_OK) {
      if (links.size() > 0) {
	iter->set_patterns(links);
      }
    } else {
      throw DBError { std::string { "Database::get_lezioni() exec failed!" } };
    }
    iter++;
  }
  return lezioni;
}

Lezione Database::get_lezione(const long long int id) {
  std::vector<Lezione> lezioni;
  Glib::ustring sql { Glib::ustring::compose("SELECT * from LESSON where LESSON.IDL=%1 ORDER BY LESSON.IDL ASC;", Glib::ustring::format(id)) };
  if (Database::exec(sqlt_db, sql, Lezione::retrieve_lezione, &lezioni) == SQLITE_OK) {
    if (lezioni.size() > 0) {
      Lezione lsn { lezioni.front() };
      std::set<long long int> links;
      sql = Glib::ustring::compose("SELECT RIFFW from LWORD where RIFFL=%1 ORDER BY RIFFW ASC;", Glib::ustring::format(id));
      if (Database::exec(sqlt_db, sql, Lezione::retrieve_links, &links) == SQLITE_OK) {
	if (links.size() > 0) {
	  lsn.set_parole(links);
	}
      } else {
	throw DBError { std::string { "Database::get_lezione() exec failed!" } };
      }
      links.clear();
      sql = Glib::ustring::compose("SELECT RIFFK from LKANJI where RIFFL=%1 ORDER BY RIFFK ASC;", Glib::ustring::format(id));
      if (Database::exec(sqlt_db, sql, Lezione::retrieve_links, &links) == SQLITE_OK) {
	if (links.size() > 0) {
	  lsn.set_kanji(links);
	}
      } else {
	throw DBError { std::string { "Database::get_lezione() exec failed!" } };
      }
      links.clear();
      sql = Glib::ustring::compose("SELECT RIFFP from LPATTERN where RIFFL=%1 ORDER BY RIFFP ASC;", Glib::ustring::format(id));
      if (Database::exec(sqlt_db, sql, Lezione::retrieve_links, &links) == SQLITE_OK) {
	if (links.size() > 0) {
	  lsn.set_patterns(links);
	}
      } else {
	throw DBError { std::string { "Database::get_lezione() exec failed!" } };
      }
      return lsn;
    } else {
      throw DBEmptyResultException { std::string { "Database::get_lezione() couldn't find lezione" } };
    }
  } else {
    throw DBError { std::string { "Database::get_lezione() exec failed!" } };
  }
}

std::vector<Parola> Database::get_parole_della_lezione(long long int id) {
  std::vector<Parola> words;
  Glib::ustring sql { Glib::ustring::compose("SELECT "			\
					     "WORD.IDW, "		\
					     "WORD.WTYPE, "		\
					     "WORD.KOTOBA, "		\
					     "WORD.YOMIKATA, "		\
					     "WORD.ITARIAGO, "		\
					     "LWORD.RIFFL, "		\
					     "LWORD.RIFFW from WORD "	\
					     "inner join LWORD on WORD.IDW = LWORD.RIFFW " \
					     "where LWORD.RIFFL=%1 ORDER BY WORD.IDW ASC;", Glib::ustring::format(id)) };
  if (Database::exec(sqlt_db, sql, Parola::retrieve_parola, &words) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_parole_della_lezione() exec failed!" } };
  }
  return words;
}

std::vector<Kanji> Database::get_kanji_della_lezione(long long int id) {
  std::vector<Kanji> kanji;
  Glib::ustring sql { Glib::ustring::compose("SELECT "			\
					     "KANJI.IDK, "		\
					     "KANJI.KANJI, "		\
					     "KANJI.ONYOMI, "		\
					     "KANJI.KUNYOMI, "		\
					     "KANJI.MEANING, "		\
					     "KANJI.RADICAL, "		\
					     "KANJI.STROKE, "		\
					     "KANJI.STROKER, "		\
					     "LKANJI.RIFFL, "		\
					     "LKANJI.RIFFK from KANJI "	\
					     "inner join LKANJI on KANJI.IDK = LKANJI.RIFFK " \
					     "where LKANJI.RIFFL=%1 ORDER BY KANJI.IDK ASC;", Glib::ustring::format(id)) };
  if (Database::exec(sqlt_db, sql, Kanji::retrieve_kanji, &kanji) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_kanji_della_lezione() exec failed!" } };
  }
  return kanji;
}

std::vector<Forma> Database::get_patterns_della_lezione(long long int id) {
  std::vector<Forma> forme;
  Glib::ustring sql { Glib::ustring::compose("SELECT "			\
					     "PATTERN.IDF, "		\
					     "PATTERN.FNAME, "		\
					     "PATTERN.TOKENS, "		\
					     "PATTERN.NOTE, "		\
					     "LPATTERN.RIFFL, "		\
					     "LPATTERN.RIFFP from PATTERN "	\
					     "inner join LPATTERN on PATTERN.IDF = LPATTERN.RIFFP " \
					     "where LPATTERN.RIFFL=%1 ORDER BY PATTERN.IDF ASC;", Glib::ustring::format(id)) };
  if (Database::exec(sqlt_db, sql, Forma::retrieve_forma, &forme) != SQLITE_OK) {
    throw DBError { std::string { "Database::get_patterns_della_lezione() exec failed!" } };
  }
  return forme;
}

long long int Database::insert(const Lezione& lsn) {
  std::stringstream ss;
  ss << "INSERT INTO LESSON (NAME) VALUES ('" << get_sqlite_safe_string(lsn.get_name()) << "');";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert(Lezione) exec failed!" } };
  }
  return sqlite3_last_insert_rowid(sqlt_db);
}

void Database::insert_lword(long long int idl, long long int idw) {
  std::stringstream ss;
  ss << "INSERT INTO LWORD (RIFFL,RIFFW) VALUES (" << idl << "," << idw << ");";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert_lword() exec failed!" } };
  }
}

void Database::insert_lkanji(long long int idl, long long int idk) {
  std::stringstream ss;
  ss << "INSERT INTO LKANJI (RIFFL,RIFFK) VALUES (" << idl << "," << idk << ");";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert_lkanji() exec failed!" } };
  }
}

void Database::insert_lpattern(long long int idl, long long int idf) {
  std::stringstream ss;
  ss << "INSERT INTO LPATTERN (RIFFL,RIFFP) VALUES (" << idl << "," << idf << ");";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert_lpattern() exec failed!" } };
  }
}

void Database::update(const Lezione& lsn) {
  std::stringstream ss;
  ss << "UPDATE LESSON set NAME='" << get_sqlite_safe_string(lsn.get_name()) << "' where IDL=" << lsn.get_idl() << ";";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update(lezione) exec failed!" } };
  }
}

void Database::delete_lezione(const long long int id) {
  std::stringstream ss;
  ss << "DELETE from LESSON where IDL='" << id << "';";
  ss << "DELETE from LWORD where RIFFL='" << id << "';";
  ss << "DELETE from LKANJI where RIFFL='" << id << "';";
  ss << "DELETE from LPATTERN where RIFFL='" << id << "';";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_lezione() exec failed!" } };
  }
}

void Database::delete_lword(long long int idl, long long int idw) {
  std::stringstream ss;
  ss << "DELETE from LWORD where RIFFL=" << idl << " and RIFFW=" << idw << ";";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_lword() exec failed!" } };
  }
}

void Database::delete_lkanji(long long int idl, long long int idk) {
  std::stringstream ss;
  ss << "DELETE from LKANJI where RIFFL=" << idl << " and RIFFK=" << idk << ";";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_lkanji() exec failed!" } };
  }
}

void Database::delete_lpattern(long long int idl, long long int idf) {
  std::stringstream ss;
  ss << "DELETE from LPATTERN where RIFFL=" << idl << " and RIFFP=" << idf << ";";
  Glib::ustring sql { ss.str() };
  if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_lpattern() exec failed!" } };
  }
}
