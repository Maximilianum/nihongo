/*
 * test_inserimento.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TESTINSERIMENTO_HH_
#define _TESTINSERIMENTO_HH_

#include "test_engine.hh"

class TestInserimento: public TestEngine {
 public:
  ~TestInserimento();
 protected:
  Gtk::Entry *answer_entry;
  sigc::connection answer_connection;

  inline TestInserimento() {}
  void init_test_inserimento(Glib::RefPtr<Gtk::Builder> bldr);
  void connect_signals(void);
  void pause_test(void);
  void continue_test(void);
  void on_window_closed(void);
};

#endif // _TESTINSERIMENTO_HH_
