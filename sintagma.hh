/*
 * sintagma.hh
 *
 * Copyright (C) 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SINTAGMA_HH_
#define _SINTAGMA_HH_

#include "parola.hh"

enum class SyntType { nominale, aggettivale, verbale };

class Sintagma {
  friend std::ostream& operator<<(std::ostream& os, const Sintagma& snt);
  friend std::istream& operator>>(std::istream& is, const Sintagma& snt);
public:
  inline Sintagma() : type (SyntType::nominale), vform (VbConjug::null), aform (AjConjug::null) {}
  inline Sintagma(const Sintagma& sn) : type (sn.type), label (sn.label), vform (sn.vform), aform (sn.aform), part (sn.part) {}
  inline Sintagma(SyntType st, const Glib::ustring& lb, VbConjug vc, AjConjug ac, const Glib::ustring& pr) : type (st), label (lb), vform (vc), aform (ac), part (pr) {}
  Sintagma& operator=(const Sintagma& sn);
  bool operator<(const Sintagma& sn) const;
  bool operator==(const Sintagma& sn) const;
  inline void set_type(SyntType st) { type = st; }
  inline SyntType get_type(void) const { return type; }
  inline void set_label(const Glib::ustring& str) { label = str; }
  inline Glib::ustring get_label(void) const { return label; }
  inline void set_verb(VbConjug vc) { vform = vc; }
  inline VbConjug get_verb(void) const { return vform; }
  inline void set_adjective(AjConjug ac) { aform = ac; }
  inline AjConjug get_adjective(void) const { return aform; }
  inline void set_particle(const Glib::ustring& str) { part = str; }
  inline Glib::ustring get_particle(void) const { return part; }
  Glib::ustring print(void) const;
private:
  SyntType type;
  Glib::ustring label;
  VbConjug vform;
  AjConjug aform;
  Glib::ustring part;
};

#endif // _SINTAGMA_HH_
