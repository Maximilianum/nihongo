/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * nihongo
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "controller.hh"
#include <iomanip>

Controller::Controller(Glib::RefPtr<Gtk::Builder> bldr, Glib::RefPtr<Gtk::Application> app, const Glib::ustring& main_path) {
	CtrlAppEngine::app_filename = APP_FILENAME;
	CtrlAppEngine::app_name = APP_NAME;
	CtrlAppEngine::version = VERSION;
	CtrlAppEngine::build_number = BUILD_NUMBER;
	CtrlAppEngine::copyright = COPYRIGHT;
	CtrlAppEngine::project_website = APP_WEBSITE;
	this_app = app;
	app_status = APP_STATUS::starting;
	font_size = 12.0;
	font_scale = 1.0;
	page_setup_path = main_path + Glib::ustring("/printer.page_setup");
	database = new Database(main_path + Glib::ustring("/nihongo.db"));
	bldr->get_widget("main_window", main_win);
	bldr->get_widget("main_notebook", main_notebook);
	main_page = 0;
	bldr->get_widget("main_statusbar", main_status);
	stsi_sel_id = main_status->get_context_id(Glib::ustring("ITEM_SEL"));
	stsi_disk_id = main_status->get_context_id(Glib::ustring("ITEM_DISK"));
	bldr->get_widget("main_toolbar", main_toolbar);
	bldr->get_widget("add_toolbutton", add_toolbutton);
	bldr->get_widget("remove_toolbutton", rem_toolbutton);
	bldr->get_widget("info_toolbutton", info_toolbutton);
	bldr->get_widget("reset_toolbutton", reset_toolbutton);
	bldr->get_widget("test_toolbutton", test_toolbutton);
	bldr->get_widget("searchentry_toolitem", search_toolitem);
	bldr->get_widget("lesson_toolitem", lectio_toolitem);
	bldr->get_widget("lesson_combobox", lectio_combobox);
	lectio_liststore = Gtk::ListStore::create(lezioni_tmcr);
	lectio_combobox->set_model(lectio_liststore);
	// main menu
	bldr->get_widget("main_menubar", main_menu);
	bldr->get_widget("quit_MenuItem", quit_menuitem);
	bldr->get_widget("add_MenuItem", add_menuitem);
	bldr->get_widget("rem_MenuItem", rem_menuitem);
	bldr->get_widget("lect_MenuItem", lectio_menuitem);
	bldr->get_widget("reset_MenuItem", reset_menuitem);
	bldr->get_widget("info_MenuItem", info_menuitem);
	bldr->get_widget("font_size_up_menuitem", font_size_up_menuitem);
	bldr->get_widget("font_size_down_menuitem", font_size_down_menuitem);
	bldr->get_widget("show_settings_menuitem", show_settings_menuitem);
	bldr->get_widget("about_MenuItem", about_menuitem);
	// info window
	bldr->get_widget("info_window", info_window);
	bldr->get_widget("info_voce_id_label", info_voce_id_label);
	bldr->get_widget("info_voce_frame_label", info_voce_frame_label);
	bldr->get_widget("info_voce_label", info_voce_label);
	bldr->get_widget("info_date_label", info_date_label);
	bldr->get_widget("info_notebook", info_notebook);
	bldr->get_widget("info_details_textview", info_details_textview);
	info_stats_liststore = Gtk::ListStore::create(stats_tmcr);
	bldr->get_widget("info_stats_treeview", info_stats_treeview);
	info_stats_treeview->set_model(info_stats_liststore);
	// page parole
	parole_liststore = Gtk::ListStore::create(parole_tmcr);
	bldr->get_widget("parole_treeview", parole_treeview);
	parole_treeview->set_model(parole_liststore);
	parole_treeselection = parole_treeview->get_selection();
	parole_treeselection->set_mode(Gtk::SELECTION_MULTIPLE);
	// page kanji
	kanji_liststore = Gtk::ListStore::create(kanji_tmcr);
	bldr->get_widget("kanji_treeview", kanji_treeview);
	kanji_treeview->set_model(kanji_liststore);
	kanji_treeselection = kanji_treeview->get_selection();
	kanji_treeselection->set_mode(Gtk::SELECTION_MULTIPLE);
	// page forme
	forme_liststore = Gtk::ListStore::create(forme_tmcr);
	bldr->get_widget("forme_treeview", forme_treeview);
	forme_treeview->set_model(forme_liststore);
	forme_treeselection = forme_treeview->get_selection();
	forme_treeselection->set_mode(Gtk::SELECTION_MULTIPLE);
	// page lezioni
	lezioni_treestore = Gtk::TreeStore::create(lezioni_tmcr);
	bldr->get_widget("lesson_treeview", lezioni_treeview);
	lezioni_treeview->set_model(lezioni_treestore);
	lezioni_treeselection = lezioni_treeview->get_selection();
	lezioni_name_treeviewcolumn = lezioni_treeview->get_column(1);
	lezioni_name_cellrenderertext = (Gtk::CellRendererText*)lezioni_name_treeviewcolumn->get_first_cell();
	// add parola win
	bldr->get_widget("word_win", parola_window);
	tipi_parola_liststore = Gtk::ListStore::create(combobox_tmcr);
	bldr->get_widget("kotoba_entry", kotoba_entry);
	bldr->get_widget("furigana_entry", furigana_entry);
	bldr->get_widget("italian_entry", traduzione_entry);
	bldr->get_widget("word_type_combobox", tipo_parola_combobox);
	tipo_parola_combobox->set_model(tipi_parola_liststore);
	std::vector<Glib::ustring> type_strings { Parola::get_types() };
	load_data_combobox(type_strings.begin(), type_strings.end(), tipi_parola_liststore);
	bldr->get_widget("word_confirm_button", parola_confirm_button);
	bldr->get_widget("word_cancel_button", parola_cancel_button);
	// add kanji win
	bldr->get_widget("kanji_win", kanji_window);
	bldr->get_widget("kanji_entry", kanji_entry);
	bldr->get_widget("radicale_entry", radicale_entry);
	bldr->get_widget("strokes_entry", tratti_entry);
	bldr->get_widget("rstrokes_entry", trattir_entry);
	bldr->get_widget("kunyomi_entry", kunyomi_entry);
	bldr->get_widget("onyomi_entry", onyomi_entry);
	bldr->get_widget("meaning_entry", significato_entry);
	bldr->get_widget("kanji_cancel_button", kanji_cancel_button);
	bldr->get_widget("kanji_confirm_button", kanji_confirm_button);
	// add forma
	bldr->get_widget("form_win", form_window);
	bldr->get_widget("form_name_entry", form_name_entry);
	bldr->get_widget("form_pattern_entry", form_pattern_entry);
	bldr->get_widget("form_note_entry", form_note_entry);
	bldr->get_widget("form_cancel_button", form_cancel_button);
	bldr->get_widget("form_confirm_button", form_confirm_button);
	// settings window
	bldr->get_widget("settings_win", settings_window);
	bldr->get_widget("treeview_fontsize_entry", treeview_fontsize_entry);
	bldr->get_widget("gui_scale_entry", gui_scale_entry);
	
	test_ctrl = new TestController(bldr);
	test_ctrl->set_database(database);
	connect_signals();
	load_lezioni_in_treeview();
}

Controller::~Controller(){
	delete database;
	delete test_ctrl;
}

void Controller::connect_signals(void) {
	// main toolbar
	add_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_add_action));
	rem_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_remove_action));
	lectio_connection = lectio_combobox->signal_changed().connect(sigc::mem_fun(this, &Controller::on_add_to_lectio));
	test_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_test_action));
	info_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_info_action));
	reset_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_reset_action));
	search_toolitem->signal_search_changed().connect(sigc::mem_fun(this, &Controller::on_search_changed));
	// main menu
	quit_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::quit));
	add_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_add_action));
	rem_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_remove_action));
	reset_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_reset_action));
	info_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_info_action));
	font_size_up_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_increase_font_size));
	font_size_down_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_decrease_font_size));
	show_settings_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::show_settings));
	about_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::about));
	// main win
	main_win->signal_focus_in_event().connect(sigc::mem_fun(this, &Controller::on_main_win_focus_in));
	main_win->signal_focus_out_event().connect(sigc::mem_fun(this, &Controller::on_main_win_focus_out));
	main_notebook->signal_switch_page().connect(sigc::mem_fun(this, &Controller::on_main_page_switch));
	// page parole
	parole_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(this, &Controller::on_parole_tree_double_clicked));
	parole_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_main_treeselection_changed));
	// page kanji
	kanji_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(this, &Controller::on_kanji_tree_double_clicked));
	kanji_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_main_treeselection_changed));
	// page forme
	forme_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(this, &Controller::forme_treeview_double_click));
	forme_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_main_treeselection_changed));
	// page lezioni
	lezioni_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_main_treeselection_changed));
	lezioni_name_cellrenderertext->signal_edited().connect(sigc::mem_fun(this, &Controller::on_lezione_name_edited));
	// add Parola win
	parola_confirm_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_add_parola_confirm_button));
	parola_cancel_button->signal_clicked().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &Controller::on_cancel_button_clicked), parola_window));
	// add Kanji win
	kanji_cancel_button->signal_clicked().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &Controller::on_cancel_button_clicked), kanji_window));
	kanji_confirm_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_add_kanji_confirm_button));
	// add Forma win
	form_cancel_button->signal_clicked().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &Controller::on_cancel_button_clicked), form_window));
	form_confirm_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_add_forma_confirm_button));
	// settings window
	treeview_fontsize_entry->signal_activate().connect(sigc::mem_fun(this, &Controller::on_treeview_fontsize_activate));
	gui_scale_entry->signal_activate().connect(sigc::mem_fun(this, &Controller::update_pango_attributes));
}

bool Controller::on_main_win_focus_in(GdkEventFocus *event) {
	if (app_status == APP_STATUS::starting) {
		set_up_printer();
		app_status = APP_STATUS::running;
	}
	main_menu->set_sensitive(true);
	main_toolbar->set_sensitive(true);
	switch (main_page) {
	case PAGE_WORD:
		parole_treeview->set_sensitive(true);
		load_parole_in_treeview();
		break;
	case PAGE_KANJI:
		kanji_treeview->set_sensitive(true);
		load_kanji_in_treeview();
		break;
	case PAGE_PATTERN:
		forme_treeview->set_sensitive(true);
		load_forme_in_treeview();
		break;
	case PAGE_LESSON:
		lezioni_treeview->set_sensitive(true);
		load_lezioni_in_treeview();
		break;
	}
	search_toolitem->grab_focus();
	return false;
}

bool Controller::on_main_win_focus_out(GdkEventFocus *event) {
	main_menu->set_sensitive(false);
	main_toolbar->set_sensitive(false);
	switch (main_page) {
	case PAGE_WORD:
		parole_treeview->set_sensitive(false);
		break;
	case PAGE_KANJI:
		kanji_treeview->set_sensitive(false);
		break;
	case PAGE_PATTERN:
		forme_treeview->set_sensitive(false);
		break;
	case PAGE_LESSON:
		lezioni_treeview->set_sensitive(false);
		break;
	}
	return false;
}

void Controller::on_main_page_switch(Gtk::Widget *page, guint page_num) {
	main_page = page_num;
	switch (main_page) {
	case PAGE_WORD:
		load_parole_in_treeview();
		break;
	case PAGE_KANJI:
		load_kanji_in_treeview();
		break;
	case PAGE_PATTERN:
		load_forme_in_treeview();
		break;
	case PAGE_LESSON:
		load_lezioni_in_treeview();
		break;
	}
}

void Controller::on_main_treeselection_changed(void) {
	Glib::ustring messaggio;
	int nr = 0;
	switch (main_page) {
	case PAGE_WORD:
		nr = parole_treeselection->count_selected_rows();
		if (nr == 0) {
			messaggio.append(Glib::ustring("Nessuna parola selezionata."));
		} else if (nr == 1) {
			messaggio.append(Glib::ustring("Una parola selezionata."));
		} else if (nr > 1) {
			messaggio.append(Glib::ustring::compose( "%1 parole selezionate.", Glib::ustring::format(nr)));
		}
		break;
	case PAGE_KANJI:
		nr = kanji_treeselection->count_selected_rows();
		if (nr == 0) {
			messaggio.append(Glib::ustring("Nessun kanji selezionato."));
		} else if (nr == 1) {
			messaggio.append(Glib::ustring("Un kanji selezionato."));
		} else if ( nr > 1) {
			messaggio.append(Glib::ustring::compose("%1 kanji selezionati.", Glib::ustring::format(nr)));
		}
		break;
	case PAGE_PATTERN:
		nr = forme_treeselection->count_selected_rows();
		if (nr == 0) {
			messaggio.append(Glib::ustring("Nessuna forma selezionata."));
		} else if (nr == 1) {
			messaggio.append(Glib::ustring("Una forma selezionata."));
		} else if (nr > 1) {
			messaggio.append(Glib::ustring::compose( "%1 forme selezionate.", Glib::ustring::format(nr)));
		}
		break;
	case PAGE_LESSON:
		nr = lezioni_treeselection->count_selected_rows();
		if (nr == 0) {
			messaggio.append(Glib::ustring("Nessuna lezione selezionata."));
		} else if (nr == 1) {
			messaggio.append(Glib::ustring("Una lezione selezionata."));
		} else if ( nr > 1) {
			messaggio.append(Glib::ustring::compose("%1 lezioni selezionate.", Glib::ustring::format(nr)));
		}
		break;
	}
	main_status->pop(stsi_sel_id);
	main_status->push(messaggio, stsi_sel_id);
	if (nr > 0) {
		if (!rem_toolbutton->get_sensitive()) {
			rem_toolbutton->set_sensitive(true);
			rem_menuitem->set_sensitive(true);
		}
		if (!info_toolbutton->get_sensitive()) {
			info_toolbutton->set_sensitive(true);
			info_menuitem->set_sensitive(true);
		}
		if (main_page < 3) {
			if (!lectio_combobox->get_sensitive()) {
				lectio_combobox->set_sensitive(true);
				lectio_menuitem->set_sensitive(true);
			}
		} else {
			if (lectio_combobox->get_sensitive()) {
				lectio_combobox->set_sensitive(false);
				lectio_menuitem->set_sensitive(false);
			}
		}
		if (main_page < 2) {
			if (!reset_toolbutton->get_sensitive()) {
				reset_toolbutton->set_sensitive(true);
				reset_menuitem->set_sensitive(true);
			}
		} else {
			if (reset_toolbutton->get_sensitive()) {
				reset_toolbutton->set_sensitive(false);
				reset_menuitem->set_sensitive(false);
			}
		}
	} else {
		if (rem_toolbutton->get_sensitive()) {
			rem_toolbutton->set_sensitive(false);
			rem_menuitem->set_sensitive(false);
		}
		if (info_toolbutton->get_sensitive()) {
			info_toolbutton->set_sensitive(false);
			info_menuitem->set_sensitive(false);
		}
		if (lectio_combobox->get_sensitive()) {
			lectio_combobox->set_sensitive(false);
			lectio_menuitem->set_sensitive(false);
		}
		if (reset_toolbutton->get_sensitive()) {
			reset_toolbutton->set_sensitive(false);
			reset_menuitem->set_sensitive(false);
		}
	}
}

void Controller::on_add_action(void) {
	switch (main_page) {
	case PAGE_WORD:
		parole_treeselection->unselect_all();
		kotoba_entry->set_text("");
		furigana_entry->set_text("");
		traduzione_entry->set_text("");
		tipo_parola_combobox->set_active(0);
		kotoba_entry->set_sensitive(true);
		furigana_entry->set_sensitive(true);
		traduzione_entry->set_sensitive(true);
		parola_window->present();
		kotoba_entry->grab_focus();
		break;
	case PAGE_KANJI:
		kanji_treeselection->unselect_all();
		kanji_entry->set_text("");
		radicale_entry->set_text("");
		tratti_entry->set_text("");
		trattir_entry->set_text("");
		kunyomi_entry->set_text("");
		onyomi_entry->set_text("");
		significato_entry->set_text("");
		kanji_entry->set_sensitive(true);
		tratti_entry->set_sensitive(true);
		kunyomi_entry->set_sensitive(true);
		onyomi_entry->set_sensitive(true);
		significato_entry->set_sensitive(true);
		kanji_window->present();
		kanji_entry->grab_focus();
		break;
	case PAGE_PATTERN:
		form_name_entry->set_text("");
		form_pattern_entry->set_text("");
		form_note_entry->set_text("");
		form_window->present();
		form_name_entry->grab_focus();
		break;
	case PAGE_LESSON: // page lezioni
		on_add_lezione();
		break;
	}
}

void Controller::on_remove_action(void) {
	Gtk::MessageDialog *alert = new Gtk::MessageDialog(*main_win, "Vuoi eliminare definitivamente gli elementi selezionati?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true);
	int res_id = alert->run();
	delete alert;
	if (res_id == Gtk::RESPONSE_YES) {
		std::vector<Gtk::TreePath> selected;
		switch (main_page) {
		case PAGE_WORD:
			selected = parole_treeselection->get_selected_rows();
			parole_remove_selected_rows(selected);
			break;
		case PAGE_KANJI:
			selected = kanji_treeselection->get_selected_rows();
			kanji_remove_selected_rows(selected);
			break;
		case PAGE_PATTERN:
			selected = forme_treeselection->get_selected_rows();
			forme_remove_selected_rows(selected);
			break;
		case PAGE_LESSON:
			selected = lezioni_treeselection->get_selected_rows ();
			lezioni_remove_selected_rows(selected);
			break;
		}
	}
}

void Controller::on_test_action(void) {
	test_ctrl->init_window();
	test_ctrl->present_window();
}

void Controller::on_info_action(void) {
	std::vector<Gtk::TreePath> selected;
	int nr_sel = 0;
	Glib::RefPtr<Gtk::TreeModel> treemodel;
	std::vector<Gtk::TreePath>::const_iterator iter;
	Gtk::TreeModel::iterator iterv;
	Gtk::TreeModel::Row row;
	switch (main_page) {
	case PAGE_WORD:
		selected = parole_treeselection->get_selected_rows();
		nr_sel = parole_treeselection->count_selected_rows();
		treemodel = parole_treeview->get_model();
		break;
	case PAGE_KANJI:
		selected = kanji_treeselection->get_selected_rows();
		nr_sel = kanji_treeselection->count_selected_rows();
		treemodel = kanji_treeview->get_model();
		break;
	case PAGE_PATTERN:
		selected = forme_treeselection->get_selected_rows();
		nr_sel = forme_treeselection->count_selected_rows();
		treemodel = forme_treeview->get_model();
		break;
	case PAGE_LESSON:
		selected = lezioni_treeselection->get_selected_rows();
		nr_sel = lezioni_treeselection->count_selected_rows();
		treemodel = lezioni_treeview->get_model();
		break;
	}
	if (nr_sel > 0) {
		iter = selected.begin();
		iterv = treemodel->get_iter(*iter);
		row = *iterv;
		switch (main_page) {
		case PAGE_WORD:
			try {
				Parola par { database->get_parola(row[parole_tmcr.idw]) };
				populate_parola_info(par);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::on_info_action() " << dbe.get_message() << std::endl;
			}
			break;
		case PAGE_KANJI:
			try {
				Kanji knj { database->get_kanji(row[kanji_tmcr.idk]) };
				populate_kanji_info(knj);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::on_info_action() " << dbe.get_message() << std::endl;
			}
			break;
		case PAGE_PATTERN:
			try {
				Forma frm { database->get_forma(row[forme_tmcr.idf]) };
				populate_forma_info(frm);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::on_info_action() " << dbe.get_message() << std::endl;
			}
			break;
		case PAGE_LESSON:
			try {
				Lezione lsn { database->get_lezione(atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str())) };
				populate_lezione_info(lsn);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::on_info_action() " << dbe.get_message() << std::endl;
			}
			break;
		}
		info_window->present();
	}
}

void Controller::on_reset_action(void) {
	Gtk::MessageDialog* alert { new Gtk::MessageDialog(*main_win, "Vuoi azzerare definitivamente le statistiche degli elementi selezionati?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true) };
	int res_id = alert->run();
	delete alert;
	if (res_id == Gtk::RESPONSE_YES) {
		std::vector<Gtk::TreePath> selected;
		switch (main_page) {
		case PAGE_WORD:
			selected = parole_treeselection->get_selected_rows();
			parole_reset_selected_rows(selected);
			break;
		case PAGE_KANJI:
			selected = kanji_treeselection->get_selected_rows();
			kanji_reset_selected_rows(selected);
			break;
		}
	}
}

void Controller::on_search_changed(void) {
	switch (main_page) {
	case PAGE_WORD:
		load_parole_in_treeview();
		break;
	case PAGE_KANJI:
		load_kanji_in_treeview();
		break;
	case PAGE_LESSON:
		load_lezioni_in_treeview();
		break;
	}
}

Glib::ustring Controller::get_search_sqlite(void) {
	std::stringstream ss;
	Glib::ustring str { search_toolitem->get_text() };
	if (str.compare(Glib::ustring()) != 0) {
		switch (main_page) {
		case PAGE_WORD:
			ss << "where KOTOBA like '%" << str << "%' ";
			ss << "or YOMIKATA like '%" << str << "%' ";
			ss << "or ITARIAGO like '%" << str << "%'";
			break;
		case PAGE_KANJI:
			ss << "where KANJI='" << str << "' ";
			ss << "or ONYOMI like '%" << str << "%' ";
			ss << "or KUNYOMI like '%" << str << "%' ";
			ss << "or MEANING like '%" << str << "%'";
			break;
		case PAGE_LESSON:
			ss << "where NAME like '%" << str << "%'";
		}
	}
	return Glib::ustring(ss.str());
}

void Controller::on_increase_font_size(void) {
	if (font_size < 100.0) {
		font_size += 1.0;
		load_parole_in_treeview();
		load_kanji_in_treeview();
		load_forme_in_treeview();
		load_lezioni_in_treeview();
	}
}

void Controller::on_decrease_font_size(void) {
	if (font_size > 8.0) {
		font_size -= 1.0;
		load_parole_in_treeview();
		load_kanji_in_treeview();
		load_forme_in_treeview();
		load_lezioni_in_treeview();
	}
}

void Controller::show_settings(void) {
	std::locale global("");
	std::stringstream ss;
	ss.imbue(global);
	ss << std::setprecision(2) << std::fixed << font_size;
	treeview_fontsize_entry->set_text(ss.str());
	ss.str("");
	ss << std::setprecision(2) << std::fixed << font_scale;
	gui_scale_entry->set_text(ss.str());
	settings_window->present();
}

void Controller::load_stats_in_treeview(const std::map<TestType,Record>& stats) {	
	info_stats_liststore->clear();
	std::map<TestType,Record>::const_iterator iter { stats.cbegin() };
	while (iter != stats.cend()) {
		Gtk::TreeModel::iterator iter_tree = info_stats_liststore->append();
		Gtk::TreeModel::Row row = *iter_tree;
		row[stats_tmcr.test] = Test::get_test_type(iter->first);
		row[stats_tmcr.correct] = iter->second.correct;
		row[stats_tmcr.total] = iter->second.total;
		row[stats_tmcr.last] = iter->second.total > 0 ? Test::get_date_string(iter->second.date) : Glib::ustring("mai sostenuto");
		iter++;
	}
}

void Controller::populate_parola_info(const Parola& par) {
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(4) << par.get_idw();
	info_voce_id_label->set_text(Glib::ustring(ss.str()));
	info_voce_frame_label->set_text(Glib::ustring("Parola"));
	info_voce_label->set_text(par.get_kotoba());
	ss.str("");
	ss << '\t' << Parola::get_type_string(par.get_wtype()) << "\n\t" << par.get_kotoba() << "\n\t" << par.get_furigana() << "\n\t" << par.get_itariago();
	info_details_textview->get_buffer()->set_text(ss.str());
	load_stats_in_treeview(par.get_statistics());
	try {
		Test vox { database->get_test(par.get_idv())};
		info_date_label->set_text(Test::get_date_string(vox.get_date()));
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::populate_parola_info() " << dbe.get_message() << std::endl;
	}	
}

void Controller::populate_kanji_info(const Kanji& knj) {
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(4) << knj.get_idk();
	info_voce_id_label->set_text(Glib::ustring(ss.str()));
	info_voce_frame_label->set_text(Glib::ustring("Kanji"));
	info_voce_label->set_text(knj.get_kanji());
	ss.str("");
	ss << "\tTratti: " << knj.get_tratti() << "\n\tOnyomi: " << knj.get_onyomi() << "\n\tKunyomi: " << knj.get_kunyomi() << "\n\t" << knj.get_significato() << "\n\tRadicale: " << knj.get_radicale();
	info_details_textview->get_buffer()->set_text(ss.str());
	load_stats_in_treeview(knj.get_statistics());
	try {
		Test vox { database->get_test(knj.get_idv()) };
		info_date_label->set_text(Test::get_date_string(vox.get_date()));
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::populate_kanji_info() " << dbe.get_message() << std::endl;
	}	
}

void Controller::populate_forma_info(const Forma& frm) {
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(4) << frm.get_idf();
	info_voce_id_label->set_text(Glib::ustring(ss.str()));
	info_voce_frame_label->set_text(Glib::ustring("Forma"));
	info_voce_label->set_text(frm.print_pattern());
	ss.str("");
	ss << '\t' << frm.get_name() << "\n\t" << frm.get_note() << "\n";
	info_details_textview->get_buffer()->set_text(ss.str());
	load_stats_in_treeview(frm.get_statistics());
	try {
		Test vox { database->get_test(frm.get_idv())};
		info_date_label->set_text(Test::get_date_string(vox.get_date()));
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::populate_forma_info() " << dbe.get_message() << std::endl;
	}
}

void Controller::populate_lezione_info(const Lezione& lsn)
{
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(4) << lsn.get_idl();
	info_voce_id_label->set_text(Glib::ustring(ss.str()));
	info_voce_frame_label->set_text(Glib::ustring("Lezione"));
	info_voce_label->set_text(lsn.get_name());
	ss.str("");
	info_date_label->set_text("");
	try {
		std::vector<Parola> parole { database->get_parole_della_lezione(lsn.get_idl()) };
		std::vector<Parola>::const_iterator iter { parole.cbegin() };
		while (iter != parole.cend()) {
			ss << '\t' << iter->get_idw() << "\t" << Parola::get_type_string(iter->get_wtype()) << "\t\t" << iter->get_kotoba() << "\t\t" << iter->get_furigana() << "\t\t" << iter->get_itariago() << "\n";
			iter++;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::populate_lezione_info() " << dbe.get_message() << std::endl;
	}	
	info_details_textview->get_buffer()->set_text(ss.str());
}

void Controller::load_parole_in_treeview(void) {
	parole_liststore->clear();
	Glib::ustring where { get_search_sqlite() };
	std::vector<Parola> words;
	try {
		words = database->get_parole(where);
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::load_parole_in_treeview() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbere) { }
	std::vector<Parola>::const_iterator iter = words.cbegin();
	while (iter != words.cend()) {
		Gtk::TreeModel::iterator iter_tree = parole_liststore->append();
		Gtk::TreeModel::Row row = *iter_tree;
		row[parole_tmcr.idw] = iter->get_idw();
		row[parole_tmcr.wtype] = Parola::get_type_string(iter->get_wtype());
		row[parole_tmcr.kotoba] = iter->get_kotoba();
		row[parole_tmcr.furigana] = iter->get_furigana();
		row[parole_tmcr.itariago] = iter->get_itariago();
		row[parole_tmcr.fcolour] = Gdk::RGBA("black");
		row[parole_tmcr.fsize] = font_size;
		iter++;
	}
}

void Controller::parole_remove_selected_rows(const std::vector<Gtk::TreePath>& sel) {
	Glib::ustring messaggio;
	Glib::RefPtr<Gtk::TreeModel> parole_treemodel { parole_treeview->get_model() };
	std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, parole_treemodel) };
	std::vector<Gtk::TreeRowReference>::iterator sr_iter { sel_ref.begin() };
	while (sr_iter != sel_ref.end()) {
		Gtk::TreeModel::iterator it_rem { parole_treemodel->get_iter(sr_iter->get_path()) };
		Gtk::TreeModel::Row row { *it_rem };
		try {
			database->delete_parola(row[parole_tmcr.idw]);
			parole_liststore->erase(it_rem);
		} catch (const Database::DBError& dbe) {
			std::cerr << "Controller::parole_treeview_remove_selected_rows() " << dbe.get_message() << std::endl;
		}
		sr_iter++;
	}
	main_status->pop(stsi_sel_id);
	if (sel.size() == 1) {
		messaggio.append(Glib::ustring("Una parola eliminata."));
	} else {
		messaggio.append(Glib::ustring::compose("%1 parole eliminate.", Glib::ustring::format(sel.size())));
	}
	main_status->push(messaggio, stsi_sel_id);
}

void Controller::on_parole_tree_double_clicked(GdkEventButton *event) {
	if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
		std::vector<Gtk::TreePath> selected { parole_treeselection->get_selected_rows() };
		int nr_sel = parole_treeselection->count_selected_rows();
		Glib::RefPtr<Gtk::TreeModel> parole_treemodel { parole_treeview->get_model() };
		if (nr_sel > 0) {
			std::vector<Gtk::TreePath>::const_iterator iter { selected.begin() };
			Gtk::TreeModel::iterator iter_view { parole_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_view };
			tipo_parola_combobox->set_active(Parola::get_type_index_from_name(row[parole_tmcr.wtype]));
			if (nr_sel > 1) {
				kotoba_entry->set_text("selezione multipla");
				kotoba_entry->set_sensitive(false);
				furigana_entry->set_text("selezione multipla");
				furigana_entry->set_sensitive(false);
				traduzione_entry->set_text("selezione multipla");
				traduzione_entry->set_sensitive(false);
			} else {
				kotoba_entry->set_text(row[parole_tmcr.kotoba]);
				kotoba_entry->set_sensitive(true);
				furigana_entry->set_text(row[parole_tmcr.furigana]);
				furigana_entry->set_sensitive(true);
				traduzione_entry->set_text(row[parole_tmcr.itariago]);
				traduzione_entry->set_sensitive(true);
			}
			parola_window->present();
		}
	}
}

void Controller::parole_reset_selected_rows(const std::vector<Gtk::TreePath>& sel) {
	Glib::ustring msg;
	Glib::RefPtr<Gtk::TreeModel> parole_treemodel { parole_treeview->get_model() };
	std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, parole_treemodel) };
	std::vector<Gtk::TreeRowReference>::iterator sriter { sel_ref.begin() };
	while (sriter != sel_ref.end()) {
		Gtk::TreeModel::iterator itrem { parole_treemodel->get_iter(sriter->get_path()) };
		Gtk::TreeModel::Row row { *itrem };
		try {
			database->reset_parola(row[parole_tmcr.idw]);
		} catch (const Database::DBError& dbe) {
			std::cerr << "Controller::parole_treeview_reset_selected_rows() " << dbe.get_message() << std::endl;
		}
		sriter++;
	}
	main_status->pop(stsi_sel_id);
	if (sel.size() == 1) {
		msg.append("Azzerate le statistiche di una parola.");
	} else {
		msg.append(Glib::ustring::compose("Azzerate le statistiche di %1 parole.", Glib::ustring::format(sel.size())));
	}
	main_status->push(msg, stsi_sel_id);
}

void Controller::on_add_parola_confirm_button(void) {
	int sel_nr = parole_treeselection->count_selected_rows();
	if (sel_nr == 0) {
		Glib::ustring kotoba { kotoba_entry->get_text() };
		Glib::ustring furigana { furigana_entry->get_text() };
		Glib::ustring parola { traduzione_entry->get_text() };
		int tipo = tipo_parola_combobox->get_active_row_number() + 1;
		Parola par { tipo, kotoba, furigana, parola };
		if (par.is_valid()) {
			try {
				database->insert(par);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::on_add_parola_confirm_button_clicked() " << dbe.get_message() << std::endl;
			}
		} else {
			Gtk::MessageDialog* alert_dialog { new Gtk::MessageDialog(*parola_window, "I campi <b>言葉</b> e <b>Traduzione</b> non possono essere vuoti!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
			alert_dialog->run();
			delete alert_dialog;
		}
	} else {
		std::vector<Gtk::TreePath> selected = parole_treeselection->get_selected_rows();
		Glib::RefPtr<Gtk::TreeModel> parole_treemodel = parole_treeview->get_model();
		std::vector<Gtk::TreePath>::const_iterator iter = selected.cbegin();
		unsigned char edit_wtype = tipo_parola_combobox->get_active_row_number() + 1;
		Glib::ustring edit_kotoba = kotoba_entry->get_text();
		Glib::ustring edit_furigana = furigana_entry->get_text();
		Glib::ustring edit_traduzione = traduzione_entry->get_text();
		while (iter != selected.cend()) {
			Gtk::TreeModel::iterator iter_view { parole_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_view };
			long long int idw = row[parole_tmcr.idw];
			if (sel_nr == 1) {
				try {
					Parola par { database->get_parola(idw) };
					Parola new_par { idw, edit_wtype, edit_kotoba, edit_furigana, edit_traduzione };
					if (new_par.is_valid()) {
						if (par != new_par) {
							new_par.set_idv(par.get_idv());
							database->update(new_par);
						}
					} else {
						Gtk::MessageDialog* alert_dialog { new Gtk::MessageDialog(*parola_window, "I campi <b>言葉</b> e <b>Traduzione</b> non possono essere vuoti!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
						alert_dialog->run();
						delete alert_dialog;
					}
				} catch (const Database::DBError& dbe) {
					std::cerr << "Controller::on_add_parola_confirm_button_clicked() " << dbe.get_message() << std::endl;
				} catch (const Database::DBEmptyResultException& dbere) {
					std::cerr << "Controller::on_add_parola_confirm_button_clicked() " << dbere.get_message() << std::endl;
				}
			}
			iter++;
		}
	}
	load_parole_in_treeview();
	parola_window->hide();
}

void Controller::load_kanji_in_treeview(void) {
	kanji_liststore->clear();
	Glib::ustring where { get_search_sqlite() };
	std::vector<Kanji> kanji;
	try {
		kanji = database->get_kanji(where);
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::load_kanji_in_treeview() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbere) { }
	std::vector<Kanji>::const_iterator iter = kanji.cbegin();
	while (iter != kanji.cend()) {
		Gtk::TreeModel::iterator iterTree = kanji_liststore->append();
		Gtk::TreeModel::Row row = *iterTree;
		row[kanji_tmcr.idk] = iter->get_idk();
		row[kanji_tmcr.kanji] = iter->get_kanji();
		row[kanji_tmcr.tratti] = iter->get_tratti();
		row[kanji_tmcr.radicale] = iter->get_radicale();
		row[kanji_tmcr.trattir] = iter->get_tratti_radicale();
		row[kanji_tmcr.kunyomi] = iter->get_kunyomi();
		row[kanji_tmcr.onyomi] = iter->get_onyomi();
		row[kanji_tmcr.significato] = iter->get_significato();
		row[kanji_tmcr.fsize] = font_size;
		iter++;
	}
}

void Controller::kanji_remove_selected_rows(const std::vector<Gtk::TreePath> &sel) {
	Glib::ustring msg;
	Glib::RefPtr<Gtk::TreeModel> kanji_treemodel { kanji_treeview->get_model() };
	std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, kanji_treemodel) };
	std::vector<Gtk::TreeRowReference>::iterator iter { sel_ref.begin() };
	while (iter != sel_ref.end()) {
		Gtk::TreeModel::iterator itr { kanji_treemodel->get_iter(iter->get_path()) };
		Gtk::TreeModel::Row row { *itr };
		try {
			database->delete_kanji(row[kanji_tmcr.idk]);
			kanji_liststore->erase(itr);
		} catch (const Database::DBError& dbe) {
			std::cerr << "Controller::kanji_treeview_remove_selected_rows() " << dbe.get_message() << std::endl;
		}
		iter++;
	}
	main_status->pop(stsi_sel_id);
	if (sel.size() == 1) {
		msg.append(Glib::ustring( "Un kanji eliminato."));
	} else {
		msg.append(Glib::ustring::compose("%1 kanji eliminati.", Glib::ustring::format(sel.size())));
	}
	main_status->push(msg, stsi_sel_id);
}

void Controller::on_kanji_tree_double_clicked(GdkEventButton *event) {
	if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
		std::vector<Gtk::TreePath> _selected = kanji_treeselection->get_selected_rows();
		int sel_nr = kanji_treeselection->count_selected_rows();
		Glib::RefPtr<Gtk::TreeModel> _kanjiListModel = kanji_treeview->get_model();
		if (sel_nr > 0) {
			std::vector<Gtk::TreePath>::const_iterator _iter = _selected.begin();
			Gtk::TreeModel::iterator _iterView = _kanjiListModel->get_iter(*_iter);
			Gtk::TreeModel::Row _row = *_iterView;
			radicale_entry->set_text(_row[kanji_tmcr.radicale]);
			std::stringstream trattiRadStr;
			trattiRadStr << static_cast<int>(_row[kanji_tmcr.trattir]);
			trattir_entry->set_text(trattiRadStr.str());
			if (sel_nr > 1) {
				kanji_entry->set_text("selezione multipla");
				kanji_entry->set_sensitive(false);
				tratti_entry->set_text("selezione multipla");
				tratti_entry->set_sensitive(false);
				kunyomi_entry->set_text("selezione multipla");
				kunyomi_entry->set_sensitive(false);
				onyomi_entry->set_text("selezione multipla");
				onyomi_entry->set_sensitive(false);
				significato_entry->set_text("selezione multipla");
				significato_entry->set_sensitive(false);
			} else {
				kanji_entry->set_text(_row[kanji_tmcr.kanji]);
				kanji_entry->set_sensitive(true);
				std::stringstream trattiStr;
				trattiStr << static_cast<int>(_row[kanji_tmcr.tratti]);
				tratti_entry->set_text(trattiStr.str());
				tratti_entry->set_sensitive(true);
				kunyomi_entry->set_text(_row[kanji_tmcr.kunyomi]);
				kunyomi_entry->set_sensitive(true);
				onyomi_entry->set_text(_row[kanji_tmcr.onyomi]);
				onyomi_entry->set_sensitive(true);
				significato_entry->set_text(_row[kanji_tmcr.significato]);
				significato_entry->set_sensitive(true);
			}
			kanji_window->present();
		}
	}
}

void Controller::kanji_reset_selected_rows(const std::vector<Gtk::TreePath>& sel) {
	Glib::ustring msg;
	Glib::RefPtr<Gtk::TreeModel> kanji_treemodel { kanji_treeview->get_model() };
	std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, kanji_treemodel) };
	std::vector<Gtk::TreeRowReference>::iterator sriter { sel_ref.begin() };
	while (sriter != sel_ref.end()) {
		Gtk::TreeModel::iterator itrem { kanji_treemodel->get_iter(sriter->get_path()) };
		Gtk::TreeModel::Row row { *itrem };
		try {
			database->reset_kanji(row[kanji_tmcr.idk]);
		} catch (const Database::DBError& dbe) {
			std::cerr << "Controller::kanji_treeview_reset_selected_rows() " << dbe.get_message() << std::endl;
		}
		sriter++;
	}
	main_status->pop (stsi_sel_id);
	if (sel.size() == 1) {
		msg.append ( "Azzerate le statistiche di un kanji." );
	} else {
		msg.append ( Glib::ustring::compose( "Azzerate le statistiche di %1 kanji.", Glib::ustring::format(sel.size())));
	}
	main_status->push(msg, stsi_sel_id);
}

void Controller::on_add_kanji_confirm_button(void) {
	Glib::ustring kanji_str { kanji_entry->get_text() };
	Glib::ustring radicale { radicale_entry->get_text() };
	unsigned short tratti = atoi(tratti_entry->get_text().c_str());
	unsigned short trattir = atoi(trattir_entry->get_text().c_str());
	Glib::ustring kunyomi { kunyomi_entry->get_text() };
	Glib::ustring onyomi { onyomi_entry->get_text() };
	Glib::ustring significato { significato_entry->get_text() };
	int sel_nr = kanji_treeselection->count_selected_rows();
	if (sel_nr == 0) {
		if (kanji_str.compare(Glib::ustring()) != 0) {
			if (Kanji::is_kanji(kanji_str.cbegin())) {
				Kanji knj { kanji_str, onyomi, kunyomi, significato, radicale, tratti, trattir };
				try {
					database->insert(knj);
				} catch (const Database::DBError& dbe) {
					std::cerr << "Controller::on_add_kanji_confirm_button_clicked() " << dbe.get_message() << std::endl;
				}
			} else {
				Gtk::MessageDialog *_alertDialog = new Gtk::MessageDialog ( *kanji_window, "Il campo <b>漢字</b> deve contenere un kanji!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true);
				int res_id = _alertDialog->run();
				delete _alertDialog;
			}
		} else {
			Gtk::MessageDialog *_alertDialog = new Gtk::MessageDialog ( *kanji_window, "Il campo <b>漢字</b> non può essere vuoto!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true);
			int res_id = _alertDialog->run();
			delete _alertDialog;
		}
	} else {
		std::vector<Gtk::TreePath> selected { kanji_treeselection->get_selected_rows() };
		Glib::RefPtr<Gtk::TreeModel> kanji_treemodel { kanji_treeview->get_model() };
		std::vector<Gtk::TreePath>::const_iterator iter { selected.cbegin() };
		while (iter != selected.cend()) {
			Gtk::TreeModel::iterator iter_view { kanji_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_view };
			long long int idk = row[kanji_tmcr.idk];
			if (sel_nr == 1) {
				try {
					Kanji knj { database->get_kanji(idk) };
					Kanji new_knj { idk, kanji_str, onyomi, kunyomi, significato, radicale, tratti, trattir };
					if (new_knj.is_valid()) {
						if (knj != new_knj) {
							database->update(new_knj);
						}
					} else {
						Gtk::MessageDialog *alert { new Gtk::MessageDialog(*kanji_window, "Il campo <b>漢字</b> non può essere vuoto!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
						int res_id = alert->run();
						delete alert;
					}
				} catch (const Database::DBError& dbe) {
					std::cerr << "Controller::on_add_kanji_confirm_button_clicked() " << dbe.get_message() << std::endl;
				} catch (const Database::DBEmptyResultException& dbere) {
					std::cerr << "Controller::on_add_kanji_confirm_button_clicked() " << dbere.get_message() << std::endl;
				}
			}
			iter++;
		}
	}
	load_kanji_in_treeview();
	kanji_window->hide();
}

void Controller::load_forme_in_treeview(void) {
	forme_liststore->clear();
	Glib::ustring where { get_search_sqlite() };
	std::vector<Forma> forme;
	try {
		forme = database->get_forme(where);
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::load_forme_in_treeview() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbere) { }
	std::vector<Forma>::const_iterator iter = forme.cbegin();
	while (iter != forme.cend()) {
		Gtk::TreeModel::iterator iter_tree = forme_liststore->append();
		Gtk::TreeModel::Row row = *iter_tree;
		row[forme_tmcr.idf] = iter->get_idf();
		row[forme_tmcr.name] = iter->get_name();
		row[forme_tmcr.pattern] = iter->get_pattern_string();
		row[forme_tmcr.descr] = iter->print_pattern();
		row[forme_tmcr.note] = iter->get_note();
		row[forme_tmcr.fcolour] = Gdk::RGBA("black");
		row[forme_tmcr.fsize] = font_size;
		iter++;
	}
}

void Controller::forme_treeview_double_click(GdkEventButton *event) {
	if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
		std::vector<Gtk::TreePath> selected { forme_treeselection->get_selected_rows() };
		int nr_sel = forme_treeselection->count_selected_rows();
		Glib::RefPtr<Gtk::TreeModel> forme_treemodel { forme_treeview->get_model() };
		if (nr_sel > 0) {
			std::vector<Gtk::TreePath>::const_iterator iter { selected.begin() };
			Gtk::TreeModel::iterator iter_view { forme_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_view };
			if (nr_sel > 1) {
				form_name_entry->set_text("selezione multipla");
				form_name_entry->set_sensitive(false);
				form_pattern_entry->set_text("selezione multipla");
				form_pattern_entry->set_sensitive(false);
				form_note_entry->set_text("selezione multipla");
				form_note_entry->set_sensitive(false);
			} else {
				form_name_entry->set_text(row[forme_tmcr.name]);
				form_name_entry->set_sensitive(true);
				form_pattern_entry->set_text(row[forme_tmcr.pattern]);
				form_pattern_entry->set_sensitive(true);
				form_note_entry->set_text(row[forme_tmcr.note]);
				form_note_entry->set_sensitive(true);
			}
			form_window->present();
		}
	}
}

void Controller::forme_remove_selected_rows(const std::vector<Gtk::TreePath> &sel) {
	Glib::ustring msg;
	Glib::RefPtr<Gtk::TreeModel> forme_treemodel { forme_treeview->get_model() };
	std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, forme_treemodel) };
	std::vector<Gtk::TreeRowReference>::iterator iter { sel_ref.begin() };
	while (iter != sel_ref.end()) {
		Gtk::TreeModel::iterator itr { forme_treemodel->get_iter(iter->get_path()) };
		Gtk::TreeModel::Row row { *itr };
		try {
			database->delete_forma(row[forme_tmcr.idf]);
			forme_liststore->erase(itr);
		} catch (const Database::DBError& dbe) {
			std::cerr << "Controller::forme_remove_selected_rows() " << dbe.get_message() << std::endl;
		} catch (const Database::DBEmptyResultException& dbere) {
			std::cerr << "Controller::forme_remove_selected_rows() " << dbere.get_message() << std::endl;
		}
		iter++;
	}
	main_status->pop(stsi_sel_id);
	if (sel.size() == 1) {
		msg.append(Glib::ustring( "Una forma eliminata."));
	} else {
		msg.append(Glib::ustring::compose("%1 forme eliminate.", Glib::ustring::format(sel.size())));
	}
	main_status->push(msg, stsi_sel_id);
}

void Controller::on_add_forma_confirm_button(void) {
	Glib::ustring name { form_name_entry->get_text() };
	Glib::ustring pattern { form_pattern_entry->get_text() };
	Glib::ustring note { form_note_entry->get_text() };
	if (pattern.compare(Glib::ustring()) != 0) {
		int sel_nr = forme_treeselection->count_selected_rows();
		if (sel_nr == 0) {
			Forma frm { name, pattern, note };
			try {
				database->insert(frm);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::on_add_form_confirm_button() " << dbe.get_message() << std::endl;
			}
		} else {
			std::vector<Gtk::TreePath> selected { forme_treeselection->get_selected_rows() };
			Glib::RefPtr<Gtk::TreeModel> forme_treemodel { forme_treeview->get_model() };
			std::vector<Gtk::TreePath>::const_iterator iter { selected.cbegin() };
			while (iter != selected.cend()) {
				Gtk::TreeModel::iterator iter_view { forme_treemodel->get_iter(*iter) };
				Gtk::TreeModel::Row row { *iter_view };
				long long int idf = row[forme_tmcr.idf];
				if (sel_nr == 1) {
					try {
						Forma frm { database->get_forma(idf) };
						Forma new_frm { idf, name, pattern, note };
						if (frm != new_frm) {
							database->update(new_frm);
						}
					} catch (const Database::DBError& dbe) {
						std::cerr << "Controller::on_add_forma_confirm_button() " << dbe.get_message() << std::endl;
					} catch (const Database::DBEmptyResultException& dbere) {
						std::cerr << "Controller::on_add_forma_confirm_button() " << dbere.get_message() << std::endl;
					}
				}
				iter++;
			}
		}
		load_forme_in_treeview();
	} else {
		Gtk::MessageDialog* alert { new Gtk::MessageDialog(*form_window, "Il campo <b>Forma</b> non può essere vuoto!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
		int res_id = alert->run();
		delete alert;
	}
	form_window->hide();
}

void Controller::load_lezioni_in_treeview(void) {
	lectio_connection.disconnect();
	lezioni_treestore->clear();
	lectio_liststore->clear();
	Gtk::TreeModel::Row row { *(lectio_liststore->append()) };
	row[lezioni_tmcr.idl] = Glib::ustring("0");
	row[lezioni_tmcr.name] = Glib::ustring("Aggiungi a lezione");
	Glib::ustring sqlite_where { get_search_sqlite() };
	std::vector<Lezione> lezioni;
	try {
		lezioni = database->get_lezioni(sqlite_where);
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::load_lezioni_in_treeview() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbere) { }
	std::vector<Lezione>::const_iterator iter { lezioni.cbegin() };
	while (iter != lezioni.cend()) {
		Gtk::TreeModel::Row row1 { *(lezioni_treestore->append()) };
		Gtk::TreeModel::Row row2 { *(lectio_liststore->append()) };
		row1[lezioni_tmcr.idl] = Glib::ustring::format(iter->get_idl());
		row2[lezioni_tmcr.idl] = Glib::ustring::format(iter->get_idl());
		row1[lezioni_tmcr.name] = iter->get_name();
		row2[lezioni_tmcr.name] = iter->get_name();
		row1[lezioni_tmcr.parole] = Glib::ustring::format(iter->count_parole());
		row1[lezioni_tmcr.kanji] = Glib::ustring::format(iter->count_kanji());
		row1[lezioni_tmcr.patterns] = Glib::ustring::format(iter->count_patterns());
		row1[lezioni_tmcr.editable] = true;
		row1[lezioni_tmcr.type] = 'l';
		row1[lezioni_tmcr.fsize] = font_size;
		if (iter->count_parole() > 0) {
			std::vector<Parola> words;
			try {
				words = database->get_parole_della_lezione(iter->get_idl());
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::load_lezioni_in_treeview() " << dbe.get_message() << std::endl;
			} catch (const Database::DBEmptyResultException& dbere) { }
			std::vector<Parola>::iterator itp { words.begin() };
			while (itp != words.end()) {
				Gtk::TreeModel::Row row3 { *(lezioni_treestore->append(row1.children())) };
				row3[lezioni_tmcr.idl] = Glib::ustring::format(itp->get_idw());
				row3[lezioni_tmcr.name] = itp->get_kotoba();
				row3[lezioni_tmcr.parole] = itp->get_furigana();
				row3[lezioni_tmcr.editable] = false;
				row3[lezioni_tmcr.type] = 'w';
				row3[lezioni_tmcr.fsize] = font_size;
				itp++;
			}
		}
		if (iter->count_kanji() > 0) {
			std::vector<Kanji> kanji;
			try {
				kanji = database->get_kanji_della_lezione(iter->get_idl());
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::load_lezioni_in_treeview() " << dbe.get_message() << std::endl;
			} catch (const Database::DBEmptyResultException& dbere) { }
			std::vector<Kanji>::iterator itk { kanji.begin() };
			while (itk != kanji.end()) {
				Gtk::TreeModel::Row row3 { *(lezioni_treestore->append(row1.children())) };
				row3[lezioni_tmcr.idl] = Glib::ustring::format(itk->get_idk());
				row3[lezioni_tmcr.name] = itk->get_kanji();
				row3[lezioni_tmcr.parole] = Glib::ustring::format(itk->get_tratti());
				row3[lezioni_tmcr.editable] = false;
				row3[lezioni_tmcr.type] = 'k';
				row3[lezioni_tmcr.fsize] = font_size;
				itk++;
			}
		}
		if (iter->count_patterns() > 0) {
			std::vector<Forma> forme;
			try {
				forme = database->get_patterns_della_lezione(iter->get_idl());
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::load_lezioni_in_treeview() " << dbe.get_message() << std::endl;
			} catch (const Database::DBEmptyResultException& dbere) { }
			std::vector<Forma>::iterator itp { forme.begin() };
			while (itp != forme.end()) {
				Gtk::TreeModel::Row row3 { *(lezioni_treestore->append(row1.children())) };
				row3[lezioni_tmcr.idl] = Glib::ustring::format(itp->get_idf());
				row3[lezioni_tmcr.name] = itp->get_name();
				row3[lezioni_tmcr.parole] = itp->print_pattern();
				row3[lezioni_tmcr.editable] = false;
				row3[lezioni_tmcr.type] = 'p';
				row3[lezioni_tmcr.fsize] = font_size;
				itp++;
			}
		}
		iter++;
	}
	lectio_combobox->set_active(0);
	lectio_connection = lectio_combobox->signal_changed().connect(sigc::mem_fun(this, &Controller::on_add_to_lectio));
}

void Controller::on_add_lezione(void) {
	Glib::ustring str { "nuova lezione" };
	Lezione lsn { str };
	unsigned int n { 1 };
	bool duplicated;
	do {
		duplicated = false;
		Gtk::TreeModel::iterator iter { lezioni_treestore->children().begin() };
		while (iter != lezioni_treestore->children().end())	{
			Gtk::TreeModel::Row row = *iter;
			if (Glib::ustring(row[lezioni_tmcr.name]).compare(lsn.get_name()) == 0) {
				lsn.set_name(str + Glib::ustring::format(n));
				n++;
				duplicated = true;
			}
			iter++;	
		}
	} while (duplicated);
	try {
		database->insert(lsn);
	} catch (const Database::DBError& dbe) {
		std::cerr << "Controller::on_add_lezione() " << dbe.get_message() << std::endl;
	}
	load_lezioni_in_treeview();
}

void Controller::on_lezione_name_edited(const Glib::ustring& path, const Glib::ustring& str) {
	Glib::RefPtr<Gtk::TreeModel> lezioni_treemodel { lezioni_treeview->get_model() };
	Gtk::TreeModel::Row row { *(lezioni_treemodel->get_iter(path)) };
	if (Glib::ustring(row[lezioni_tmcr.name]).compare(str) != 0) {
		long long int idl { atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str()) };
		Lezione lsn { idl, str };
		try {
			database->update(lsn);
			row[lezioni_tmcr.name] = str;
		} catch (Database::DBError& dbe) {
			std::cerr << "Controller::on_lezione_name_edited() " << dbe.get_message() << std::endl;
		}
		Gtk::TreeModel::iterator iter { lectio_liststore->children().begin() };
		while (iter != lectio_liststore->children().end())	{
			Gtk::TreeModel::Row row { *iter };
			if (atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str()) == idl) {
				row[lezioni_tmcr.name] = str;
				break;
			}
			iter++;
		}
	}
}

void Controller::lezioni_remove_selected_rows(const std::vector<Gtk::TreePath>& sel) {
	Glib::ustring msg;
	Glib::RefPtr<Gtk::TreeModel> lezioni_treemodel { lezioni_treeview->get_model() };
	std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, lezioni_treemodel) };
	std::vector<Gtk::TreeRowReference>::iterator it_trr { sel_ref.begin() };
	while (it_trr != sel_ref.end()) {
		Gtk::TreeModel::iterator itrem { lezioni_treemodel->get_iter(it_trr->get_path()) };
		Gtk::TreeModel::Row row { *itrem };
		if (row[lezioni_tmcr.type] == 'l') {
			try {
				Glib::ustring id { row[lezioni_tmcr.idl] };
				database->delete_lezione(atoll(id.c_str()));
				lezioni_treestore->erase(itrem);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::lezioni_tree_remove_selected_rows() " << dbe.get_message() << std::endl;
			}
		} else {
			Gtk::TreeModel::Path path { it_trr->get_path() };
			path.up();
			Gtk::TreeModel::iterator miter { lezioni_treemodel->get_iter(path) };
			Gtk::TreeModel::Row mrow { *miter };
			try {
				if (row[lezioni_tmcr.type] == 'w') {
					database->delete_lword(atoll(Glib::ustring(mrow[lezioni_tmcr.idl]).c_str()), atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str()));
				} else if (row[lezioni_tmcr.type] == 'k') {
					database->delete_lkanji(atoll(Glib::ustring(mrow[lezioni_tmcr.idl]).c_str()), atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str()));
				} else if (row[lezioni_tmcr.type] == 'p') {
					database->delete_lpattern(atoll(Glib::ustring(mrow[lezioni_tmcr.idl]).c_str()), atoll(Glib::ustring(row[lezioni_tmcr.idl]).c_str()));
				}
				lezioni_treestore->erase(itrem);
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::lezioni_tree_remove_selected_rows() " << dbe.get_message() << std::endl;
			}
		}
		it_trr++;
	}
	main_status->pop(stsi_sel_id);
	if (sel.size() == 1) {
		msg.append(Glib::ustring("Una lezione eliminata."));
	} else {
		msg.append(Glib::ustring::compose("%1 lezioni eliminate.", Glib::ustring::format(sel.size())));
	}
	main_status->push(msg, stsi_sel_id);
}

void Controller::on_add_to_lectio(void) {
	if (lectio_combobox->get_active_row_number() > 0) {
		Gtk::TreeModel::iterator sel { lectio_combobox->get_active() };
		Gtk::TreeModel::Row row { *sel };
		Glib::ustring idls { row[lezioni_tmcr.idl] };
		long long int idl { atoll(idls.c_str()) };
		std::vector<Gtk::TreePath> selected;
		Glib::RefPtr<Gtk::TreeModel> treemodel;
		switch (main_page) {
		case PAGE_WORD:
			selected = parole_treeselection->get_selected_rows();
			treemodel = parole_treeview->get_model();
			break;
		case PAGE_KANJI:
			selected = kanji_treeselection->get_selected_rows();
			treemodel = kanji_treeview->get_model();
			break;
		case PAGE_PATTERN:
			selected = forme_treeselection->get_selected_rows();
			treemodel = forme_treeview->get_model();
			break;
		}
		std::vector<Gtk::TreePath>::const_iterator iter { selected.cbegin() };
		while (iter != selected.cend()) {
			Gtk::TreeModel::Row row { *(treemodel->get_iter(*iter)) };
			long long id { 0 };
			try {
				switch (main_page) {
				case PAGE_WORD:
					id = row[parole_tmcr.idw];
					database->insert_lword(idl, id);
					break;
				case PAGE_KANJI:
					id = row[kanji_tmcr.idk];
					database->insert_lkanji(idl, id);
					break;
				case PAGE_PATTERN:
					id = row[forme_tmcr.idf];
					database->insert_lpattern(idl, id);
					break;
				}
			} catch (const Database::DBError& dbe) {
				std::cerr << "Controller::on_add_to_lectio() " << dbe.get_message() << std::endl;
			}
			iter++;
		}
		load_lezioni_in_treeview();
	}
}

void Controller::on_treeview_fontsize_activate(void) {
	font_size = atof(treeview_fontsize_entry->get_text().c_str());
	load_parole_in_treeview();
	load_kanji_in_treeview();
	load_forme_in_treeview();
	load_lezioni_in_treeview();
	settings_window->hide();
}

void Controller::update_pango_attributes(void) {
	Pango::AttrList atlst;
	double val { atof(gui_scale_entry->get_text().c_str()) };
	if (val > 0.24 && val < 4.01) {
		font_scale = val;
		Pango::AttrFloat attr { Pango::AttrFloat::create_attr_scale(font_scale) };
		atlst.insert(attr);
		kotoba_entry->set_attributes(atlst);
		furigana_entry->set_attributes(atlst);
		traduzione_entry->set_attributes(atlst);
		parola_window->queue_draw();
		kanji_entry->set_attributes(atlst);
		radicale_entry->set_attributes(atlst);
		tratti_entry->set_attributes(atlst);
		trattir_entry->set_attributes(atlst);
		kunyomi_entry->set_attributes(atlst);
		onyomi_entry->set_attributes(atlst);
		significato_entry->set_attributes(atlst);
		kanji_window->queue_draw();
		form_name_entry->set_attributes(atlst);
		form_pattern_entry->set_attributes(atlst);
		form_note_entry->set_attributes(atlst);
		form_window->queue_draw();
		settings_window->hide();
	}
}
