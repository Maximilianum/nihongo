/*
 * lezione.cc
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "lezione.hh"

std::string Lezione::sqlite_lesson_name { "LESSON" };
std::string Lezione::sqlite_lesson_table { "(IDL INTEGER PRIMARY KEY NOT NULL, " \
    "NAME TEXT NOT NULL);" };
std::string Lezione::sqlite_lword_name { "LWORD" };
std::string Lezione::sqlite_lword_table { "(IDLW INTEGER PRIMARY KEY NOT NULL, " \
    "RIFFL INTEGER NOT NULL, "						\
    "RIFFW INTEGER NOT NULL, "						\
    "FOREIGN KEY(RIFFL) REFERENCES LESSON(IDL), "			\
    "FOREIGN KEY(RIFFW) REFERENCES WORD(IDW));" };
std::string Lezione::sqlite_lkanji_name { "LKANJI" };
std::string Lezione::sqlite_lkanji_table { "(IDLK INTEGER PRIMARY KEY NOT NULL, " \
    "RIFFL INTEGER NOT NULL, "						\
    "RIFFK INTEGER NOT NULL, "						\
    "FOREIGN KEY(RIFFL) REFERENCES LESSON(IDL), "			\
    "FOREIGN KEY(RIFFK) REFERENCES KANJI(IDK));" };
std::string Lezione::sqlite_lpattern_name { "LPATTERN" };
std::string Lezione::sqlite_lpattern_table { "(IDLP INTEGER PRIMARY KEY NOT NULL, " \
    "RIFFL INTEGER NOT NULL, "						\
    "RIFFP INTEGER NOT NULL, "						\
    "FOREIGN KEY(RIFFL) REFERENCES LESSON(IDL), "			\
    "FOREIGN KEY(RIFFP) REFERENCES PATTERN(IDF));" };

int Lezione::retrieve_lezione(void *data, int argc, char **argv, char **azColName) {
  Lezione lsn;
  Lezione* plsn = &lsn;
  std::vector<Lezione>* lessons { static_cast<std::vector<Lezione>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("IDL") && argv[i]) {
      lsn.set_idl(argv[i]);
      if (lessons->size() > 0 && lsn.get_idl() == lessons->rbegin()->get_idl()) {
	plsn = &*(lessons->rbegin());
      }
    } else if (std::string(azColName[i]) == std::string("NAME") && argv[i]) {
      plsn->set_name(argv[i]);
    }
  }
  if (plsn == &lsn) {
    lessons->push_back(lsn);
  }
  return 0;
}

int Lezione::retrieve_links(void *data, int argc, char **argv, char **azColName) {
  std::set<long long int>* links { static_cast<std::set<long long int>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("RIFFW") && argv[i]) {
      links->insert(links->end(), atoll(argv[i]));
    } else if (std::string(azColName[i]) == std::string("RIFFK") && argv[i]) {
      links->insert(links->end(), atoll(argv[i]));
    } else if (std::string(azColName[i]) == std::string("RIFFP") && argv[i]) {
      links->insert(links->end(), atoll(argv[i]));
    }
  }
  return 0;
}

Lezione& Lezione::operator=(const Lezione &lsn) {
  if (this != &lsn) {
    idl = lsn.idl;
    name = lsn.name;
    words = lsn.words;
    kanji = lsn.kanji;
    patterns = lsn.patterns;
  }
  return *this;
}
