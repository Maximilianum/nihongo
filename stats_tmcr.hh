/*
 * stats_tmcr.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _STATS_TREE_MODEL_COLUMN_RECORD_HH_
#define _STATS_TREE_MODEL_COLUMN_RECORD_HH_

class StatsTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
  StatsTreeModelColumnRecord() { add(test);
    add(correct);
    add(total);
    add(last); }
  Gtk::TreeModelColumn<Glib::ustring> test;
  Gtk::TreeModelColumn<int> correct;
  Gtk::TreeModelColumn<int> total;
  Gtk::TreeModelColumn<Glib::ustring> last;
};

#endif // _STATS_TREE_MODEL_COLUMN_RECORD_HH_

