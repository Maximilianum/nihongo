/*
 * test_inserimento.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_inserimento.hh"

TestInserimento::~TestInserimento() {}

void TestInserimento::init_test_inserimento(Glib::RefPtr<Gtk::Builder> bldr) {
  init_test_engine(bldr);
  bldr->get_widget("test_ins_win", main_win);
  // toolbar
  bldr->get_widget("test_ins_quest_nr_label", question_nr_label);
  bldr->get_widget("test_ins_quest_tot_label", question_tot_label);
  bldr->get_widget("test_ins_time_label", time_label);
  bldr->get_widget("test_ins_skip_toolbutton", skip_toolbutton);
  bldr->get_widget("test_ins_pause_toolbutton", pause_toolbutton);
  // main
  bldr->get_widget("test_ins_quest_label", question_label);
  bldr->get_widget("test_ins_upquest_label", upquestion_label);
  bldr->get_widget("test_ins_entry", answer_entry);
}

void TestInserimento::connect_signals(void) {
  main_win_connection = main_win->signal_hide().connect(sigc::mem_fun(this, &TestInserimento::on_window_closed));
  skip_connection = skip_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &TestInserimento::on_skip_clicked));
  pause_connection = pause_toolbutton->signal_clicked().connect(sigc::mem_fun (this, &TestInserimento::on_pause_clicked));
  answer_connection = answer_entry->signal_activate().connect(sigc::bind<char>(sigc::mem_fun(this, &TestInserimento::on_risposta), 0));
}

void TestInserimento::pause_test(void) {
  paused = true;
  pause_toolbutton->set_stock_id(Gtk::StockID("gtk-media-play"));
  timer_connection.disconnect();
  question_label->set_visible(false);
  upquestion_label->set_visible(false);
  answer_entry->set_visible(false);
}

void TestInserimento::continue_test(void) {
  paused = false;
  pause_toolbutton->set_stock_id(Gtk::StockID("gtk-media-pause"));
  question_label->set_visible(true);
  upquestion_label->set_visible(true);
  answer_entry->set_visible(true);
}

void TestInserimento::on_window_closed(void) {
  main_win_connection.disconnect();
  skip_connection.disconnect();
  pause_connection.disconnect();
  answer_connection.disconnect();
  timer_connection.disconnect();
  if (paused) {
    continue_test();
  }
  window_closed.emit();
}
