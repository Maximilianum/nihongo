/*
 * test_inserimento_numeri.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TESTINSERIMENTONUMERI_HH_
#define _TESTINSERIMENTONUMERI_HH_

#include "test_inserimento.hh"

#define CIFRE_HIRAGANA		0

typedef struct {
  Glib::ustring question;
  Glib::ustring answer;
} Number;

class TestInserimentoNumeri: public TestInserimento {
 public:
  TestInserimentoNumeri(Glib::RefPtr<Gtk::Builder> bldr);
  inline ~TestInserimentoNumeri() {}
  void run_test(const TestOptions& opts);
protected:
  void update_window(void);
  void on_risposta(short tasto = -1);
  void next_question(void);
  std::vector<Number> get_numbers(void);
  static Glib::ustring to_hiragana(int val);
private:
  std::vector<Number> content;
  std::vector<Number>::iterator current;
};

#endif // _TESTINSERIMENTONUMERI_HH_
