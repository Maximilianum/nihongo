/*
 * test_scelta_mult.cc
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_scelta_mult.hh"

void TestSceltaMult::init_test_scelta_mult(Glib::RefPtr<Gtk::Builder> bldr) {
	init_test_engine(bldr);
	bldr->get_widget("test_multsel_win", main_win);
	// toolbar
	bldr->get_widget("test_multsel_quest_nr_label", question_nr_label);
	bldr->get_widget("test_multsel_quest_tot_label", question_tot_label);
	bldr->get_widget("test_multsel_time_label", time_label);
	bldr->get_widget("test_multsel_skip_toolbutton", skip_toolbutton);
	bldr->get_widget("test_multsel_pause_toolbutton", pause_toolbutton);
	// main
	bldr->get_widget("test_multsel_quest_label", question_label);
	bldr->get_widget("test_multsel_upquest_label", upquestion_label);
	bldr->get_widget("test_multsel_answ1_button", answer1_button);
	bldr->get_widget("test_multsel_answ1_label", answer1_label);
	bldr->get_widget("test_multsel_answ1up_label", answer1up_label);
	bldr->get_widget("test_multsel_answ2_button", answer2_button);
	bldr->get_widget("test_multsel_answ2_label", answer2_label);
	bldr->get_widget("test_multsel_answ2up_label", answer2up_label);
	bldr->get_widget("test_multsel_answ3_button", answer3_button);
	bldr->get_widget("test_multsel_answ3_label", answer3_label);
	bldr->get_widget("test_multsel_answ3up_label", answer3up_label);
	bldr->get_widget("test_multsel_answ4_button", answer4_button);
	bldr->get_widget("test_multsel_answ4_label", answer4_label);
	bldr->get_widget("test_multsel_answ4up_label", answer4up_label);
	bldr->get_widget("test_multsel_answ5_button", answer5_button);
	bldr->get_widget("test_multsel_answ5_label", answer5_label);
	bldr->get_widget("test_multsel_answ5up_label", answer5up_label);
	bldr->get_widget("test_multsel_answ6_button", answer6_button);
	bldr->get_widget("test_multsel_answ6_label", answer6_label);
	bldr->get_widget("test_multsel_answ6up_label", answer6up_label);
	risposta_esatta = "";
}

void TestSceltaMult::connect_signals(void) {
	connect_toolbar_signals();
	answer1_connection = answer1_button->signal_clicked().connect(sigc::bind<char>(sigc::mem_fun(*this, &TestSceltaMult::on_risposta), 0));
	answer2_connection = answer2_button->signal_clicked().connect(sigc::bind<char>(sigc::mem_fun(*this, &TestSceltaMult::on_risposta), 1));
	answer3_connection = answer3_button->signal_clicked().connect(sigc::bind<char>(sigc::mem_fun(*this, &TestSceltaMult::on_risposta), 2));
	answer4_connection = answer4_button->signal_clicked().connect(sigc::bind<char>(sigc::mem_fun(*this, &TestSceltaMult::on_risposta), 3));
	answer5_connection = answer5_button->signal_clicked().connect(sigc::bind<char>(sigc::mem_fun(*this, &TestSceltaMult::on_risposta), 4));
	answer6_connection = answer6_button->signal_clicked().connect(sigc::bind<char>(sigc::mem_fun(*this, &TestSceltaMult::on_risposta), 5));
}

void TestSceltaMult::pause_test(void) {
	paused = true;
	pause_toolbutton->set_stock_id(Gtk::StockID("gtk-media-play"));
	timer_connection.disconnect();
	question_label->set_visible(false);
	upquestion_label->set_visible(false);
	answer1_button->set_visible(false);
	answer2_button->set_visible(false);
	answer3_button->set_visible(false);
	answer4_button->set_visible(false);
	answer5_button->set_visible(false);
	answer6_button->set_visible(false);
}

void TestSceltaMult::continue_test(void) {
	paused = false;
	pause_toolbutton->set_stock_id(Gtk::StockID("gtk-media-pause"));
	question_label->set_visible(true);
	upquestion_label->set_visible(true);
	answer1_button->set_visible(true);
	answer2_button->set_visible(true);
	answer3_button->set_visible(true);
	answer4_button->set_visible(true);
	answer5_button->set_visible(true);
	answer6_button->set_visible(true);
}

void TestSceltaMult::on_window_closed(void) {
	main_win_connection.disconnect();
	skip_connection.disconnect();
	pause_connection.disconnect();
	answer1_connection.disconnect();
	answer2_connection.disconnect();
	answer3_connection.disconnect();
	answer4_connection.disconnect();
	answer5_connection.disconnect();
	answer6_connection.disconnect();
	timer_connection.disconnect();
	if (paused) {
		continue_test();
	}
	window_closed.emit();
}
