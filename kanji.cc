/*
 * kanji.cc
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "kanji.hh"

std::string Kanji::sqlite_kanji_name { "KANJI" };
std::string Kanji::sqlite_kanji_table { "(IDK INTEGER PRIMARY KEY NOT NULL, " \
    "KANJI TEXT NOT NULL, "						\
    "ONYOMI TEXT, "							\
    "KUNYOMI TEXT, "							\
    "MEANING TEXT NOT NULL, "						\
    "RADICAL TEXT NOT NULL, "						\
    "STROKE INTEGER NOT NULL, "						\
    "STROKER INTEGER NOT NULL, "					\
    "STATS INTEGER NOT NULL, "						\
    "FOREIGN KEY(STATS) REFERENCES TEST(IDV));" };

int Kanji::retrieve_kanji(void *data, int argc, char **argv, char **azColName) {
  Kanji knj;
  Kanji* pknj = &knj;
  Record rec;
  Glib::ustring rtype;
  std::vector<Kanji>* kanji { static_cast<std::vector<Kanji>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("IDK") && argv[i]) {
      knj.set_idk(argv[i]);
      if (kanji->size() > 0 && knj.get_idk() == kanji->rbegin()->get_idk()) {
	pknj = &*(kanji->rbegin());
      }
    } else if (std::string(azColName[i]) == std::string("KANJI") && argv[i]) {
      pknj->set_kanji(argv[i]);
    } else if (std::string(azColName[i]) == std::string("ONYOMI") && argv[i]) {
      pknj->set_onyomi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("KUNYOMI") && argv[i]) {
      pknj->set_kunyomi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("MEANING") && argv[i]) {
      pknj->set_significato(argv[i]);
    } else if (std::string(azColName[i]) == std::string("RADICAL") && argv[i]) {
      pknj->set_radicale(argv[i]);
    } else if (std::string(azColName[i]) == std::string("STROKE") && argv[i]) {
      pknj->set_tratti(argv[i]);
    } else if (std::string(azColName[i]) == std::string("STROKER") && argv[i]) {
      pknj->set_tratti_radicale(argv[i]);
    } else if (std::string(azColName[i]) == std::string("STATS") && argv[i]) {
      pknj->set_idv(argv[i]);
    } else if (std::string(azColName[i]) == std::string("RTYPE") && argv[i]) {
      rtype.assign(argv[i]);
    } else if (std::string(azColName[i]) == std::string("CORRECT") && argv[i]) {
      rec.correct = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("TOTAL") && argv[i]) {
      rec.total = atoi(argv[i]);
    } else if (std::string(azColName[i]) == std::string("DATE") && argv[i]) {
      rec.date = atol(argv[i]);
    } else if (std::string(azColName[i]) == std::string("ERROR") && argv[i]) {
      rec.error = atoi(argv[i]);
      pknj->set_record(Test::get_test_type(rtype),rec);
    }
  }
  if (pknj == &knj) {
    kanji->push_back(knj);
  }
  return 0;
}

int Kanji::retrieve_id(void *data, int argc, char **argv, char **azColName) {
  std::vector<long long int>* kanji { static_cast<std::vector<long long int>*>(data) };
  for(int i=0; i<argc; i++) {
    if (std::string(azColName[i]) == std::string("IDK") && argv[i]) {
      kanji->push_back(atoll(argv[i]));
    }
  }
  return 0;
}

std::list<Glib::ustring> Kanji::parse_letture(const Glib::ustring& str) {
  std::list<Glib::ustring> list;
  size_t inizio = 0;
  size_t fine = str.length();
  while (inizio < fine) {
    size_t pos = str.find_first_of(" ,　、", inizio);
    if (pos != Glib::ustring::npos) {
      Glib::ustring trovato = str.substr(inizio, pos - inizio);				
      if (trovato != "") {
	list.push_back(trovato);
      }
      inizio = pos + 1;
    } else {
      Glib::ustring trovato = str.substr(inizio);
      if (trovato != "") {
	list.push_back(trovato);
      }
      inizio = fine;
    }
  }
  return list;
}

Glib::ustring Kanji::create_string(const std::list<Glib::ustring>& list) {
  Glib::ustring str;
  if (list.size() > 0) {
    std::list<Glib::ustring>::const_iterator iter = list.cbegin();
    str.assign(*iter);
    iter++;
    while (iter != list.cend()) {
      str.append(Glib::ustring::compose(", %1", *iter));
      iter++;
    }
  }
  return str;
}

bool Kanji::contiene_stringa(const std::list<Glib::ustring> &list, const Glib::ustring &str) {
  if (list.size() > 0) {
    std::list<Glib::ustring>::const_iterator iter = list.cbegin();
    while (iter != list.cend()) {
      if (iter->raw().find(str.raw()) != std::string::npos) {
	return true;
      }
      iter++;
    }
  }
  return false;
}

Kanji::Kanji(const Kanji &knj) : idk (knj.idk), kanji (knj.kanji), onyomi (knj.onyomi), kunyomi (knj.kunyomi), significato (knj.significato), radicale (knj.radicale), tratti (knj.tratti), tratti_radicale (knj.tratti_radicale) {
  idv = knj.idv;
  statistics = knj.statistics;
}

Kanji& Kanji::operator=(const Kanji& knj) {
  if (this != &knj) {
    idk = knj.idk;
    kanji = knj.kanji;
    onyomi = knj.onyomi;
    kunyomi = knj.kunyomi;
    significato = knj.significato;
    radicale = knj.radicale;
    tratti = knj.tratti;
    tratti_radicale = knj.tratti_radicale;
    idv = knj.idv;
    statistics = knj.statistics;
  }
  return *this;
}

bool Kanji::operator<(const Kanji& knj) const {
  if (idk < knj.idk) {
    return true;
  }
  return false;
}

bool Kanji::operator==(const Kanji& knj) const {
  if (kanji.compare(knj.kanji) == 0) {
    if (onyomi.compare(knj.onyomi) == 0) {
      if (kunyomi.compare(knj.kunyomi) == 0) {
	if (significato.compare(knj.significato) == 0) {
	  if (radicale.compare(knj.radicale) == 0) {
	    if (tratti == knj.tratti) {
	      if (tratti_radicale == knj.tratti_radicale) {
		return true;
	      }
	    }
	  }
	}
      }
    }
  }
  return false;
}

bool Kanji::operator!=(const Kanji& knj) const {
  if (kanji.compare(knj.kanji) != 0 || onyomi.compare(knj.onyomi) != 0 || kunyomi.compare(knj.kunyomi) != 0 || significato.compare(knj.significato) != 0 || radicale.compare(knj.radicale) != 0 || tratti != knj.tratti || tratti_radicale == knj.tratti_radicale) {
    return true;
  }
  return false;
}

bool Kanji::is_valid(void) const {
  if (kanji.compare(Glib::ustring()) != 0 && is_kanji(kanji.cbegin())) {
    if (significato.compare(Glib::ustring()) != 0 && radicale.compare(Glib::ustring()) != 0) {
      if (tratti > 0 && tratti_radicale > 0) {
	return true;
      }
    }
  }
  return false;
}
