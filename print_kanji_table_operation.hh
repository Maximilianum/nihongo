/*
 * print_kanji_table_operation.hh
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
Nihongo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Nihongo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PRINTKANJITABLEOPERATION_HH_
#define _PRINTKANJITABLEOPERATION_HH_

#include <gtkmm.h>
#include <iostream>

class DrawingKanjiSheet;


class PrintKanjiTableOperation: public Gtk::PrintOperation {
 public:
	static Glib::RefPtr<PrintKanjiTableOperation> create();
	virtual ~PrintKanjiTableOperation ();
	inline void set_kaku_area(DrawingKanjiSheet* kp) { kakuArea = kp; }
 protected:
	PrintKanjiTableOperation();
	virtual void on_begin_print(const Glib::RefPtr<Gtk::PrintContext>& context);
	virtual void on_draw_page(const Glib::RefPtr<Gtk::PrintContext>& context, int page_nr);
private:
	DrawingKanjiSheet* kakuArea;
};

#endif // _PRINTKANJITABLEOPERATION_HH_
