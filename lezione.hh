/*
 * lezione.hh
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LEZIONE_HH_
#define _LEZIONE_HH_

#include <glibmm/ustring.h>
#include <set>
#include <vector>
#include <algorithm>
#include <iostream>

class Lezione {
 public:
  // static functions
  static inline std::string get_sqlite_lesson_name(void) { return sqlite_lesson_name; }
  static inline std::string get_sqlite_lesson_table(void) { return sqlite_lesson_table; }
  static inline std::string get_sqlite_lword_name(void) { return sqlite_lword_name; }
  static inline std::string get_sqlite_lword_table(void) { return sqlite_lword_table; }
  static inline std::string get_sqlite_lkanji_name(void) { return sqlite_lkanji_name; }
  static inline std::string get_sqlite_lkanji_table(void) { return sqlite_lkanji_table; }
  static inline std::string get_sqlite_lpattern_name(void) { return sqlite_lpattern_name; }
  static inline std::string get_sqlite_lpattern_table(void) { return sqlite_lpattern_table; }
  static int retrieve_lezione(void *data, int argc, char **argv, char **azColName);
  static int retrieve_links(void *data, int argc, char **argv, char **azColName);
  inline Lezione(const Glib::ustring &str = Glib::ustring()) : name (str) {}
  inline Lezione(long long int id, const Glib::ustring &str = Glib::ustring()) : idl (id), name (str) {}
  inline Lezione(const Lezione &lsn) : idl (lsn.idl), name (lsn.name), words (lsn.words), kanji (lsn.kanji), patterns (lsn.patterns) {}
  Lezione& operator=(const Lezione &lsn);
  inline bool operator<(const Lezione &lsn) const { return name.compare(lsn.name) < 0; }
  inline bool operator==(const Lezione &lsn) const { return name.compare(lsn.name) == 0; }
  inline void set_idl(long long int val) { idl = val; }
  inline void set_idl(const char* chr) { idl = atoll(chr); }
  inline long long int get_idl(void) const { return idl; }
  inline void set_name(const Glib::ustring &str) { name.assign(str); }
  inline void set_name(const char *cstr) { name.assign(cstr); }
  inline Glib::ustring get_name(void) const { return name; }
  inline void set_parole(const std::set<long long int>& set) { words = set; }
  inline const std::set<long long int> get_parole(void) const { return words; }
  inline bool contiene_parola(long long int val) const { return words.count(val) > 0; }
  inline void add_parola(long long int val) { words.insert(words.end(), val); }
  inline unsigned int count_parole(void) const { return words.size(); }
  inline void set_kanji(const std::set<long long int>& set) { kanji = set; }
  inline const std::set<long long int>& get_kanji(void) const { return kanji; }
  inline bool contiene_kanji(long long int val) const { return kanji.count(val) > 0; }
  inline void add_kanji(long long int val) { kanji.insert(kanji.end(), val); }
  inline unsigned int count_kanji(void) const { return kanji.size(); }
  inline void set_patterns(const std::set<long long int>& set) { patterns = set; }
  inline const std::set<long long int>& get_patterns(void) const { return patterns; }
  inline bool contiene_pattern(long long int val) const { return patterns.count(val) > 0; }
  inline void add_pattern(long long int val) { patterns.insert(patterns.end(), val); }
  inline unsigned int count_patterns(void) const { return patterns.size(); }
private:
  static std::string sqlite_lesson_name;
  static std::string sqlite_lesson_table;
  static std::string sqlite_lword_name;
  static std::string sqlite_lword_table;
  static std::string sqlite_lkanji_name;
  static std::string sqlite_lkanji_table;
  static std::string sqlite_lpattern_name;
  static std::string sqlite_lpattern_table;
  long long int idl;
  Glib::ustring name;
  std::set<long long int> words;
  std::set<long long int> kanji;
  std::set<long long int> patterns;
};

#endif // _LEZIONE_HH_
