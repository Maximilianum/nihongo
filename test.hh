/*
 * test.hh (ex voce.hh)
 * ven giugno 10 09:47:56 2011
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TEST_HH_
#define _TEST_HH_

#include <gtkmm.h>
#include <iostream>
#include <iomanip>
#include <map>

#define MEM_WS  "MEM_WS"
#define CNJ_S   "CNJ_S"
#define CNJ_I   "CNJ_I"
#define READ_WS "READ_WS"
#define READ_WI "READ_WI"
#define MEM_KS  "MEM_KS"

typedef struct {
  int correct;
  int total;
  time_t date;
  bool error;
} Record;

enum class ItemType { word, kanji, pattern };
enum class TestType { null, mem_wrd_sel, cnj_sel, cnj_ins, read_wrd_sel, read_wrd_ins, mem_knj_sel, numb_ins };

class Test {
public:
  // static class functions
  static inline std::string get_sqlite_test_name(void) { return sqlite_test_name; }
  static inline std::string get_sqlite_test_table(void) { return sqlite_test_table; }
  static inline std::string get_sqlite_record_name(void) { return sqlite_record_name; }
  static inline std::string get_sqlite_record_table(void) { return sqlite_record_table; }
  static int retrieve_test(void *data, int argc, char **argv, char **azColName);
  static int retrieve_record(void *data, int argc, char **argv, char **azColName);
  static inline bool is_kanji(Glib::ustring::const_iterator it) { return (*it >= 0x4E00 && *it <= 0x9FCF) || (*it >= 0x3400 && *it <= 0x4DBF) || (*it >= 0x20000 && *it <= 0x2A6DF) || (*it >= 0x2A700 && *it <= 0x2B73F) || (*it >= 0x2B740 && *it <= 0x2B81F) || (*it >= 0x2F800 && *it <= 0x2FA1F) || (*it >= 0xF900 && *it <= 0xFAFF); }
  static Glib::ustring get_date_string(time_t raw);
  static Glib::ustring get_test_type_id(TestType tt);
  static TestType get_test_type(const Glib::ustring& str);
  static Glib::ustring get_test_type(TestType tt);
  static short get_test_index(TestType tt);
  // constructors
  inline Test() {}
  Test(long long int id, const std::map<TestType,Record>& stats);
  Test(const Test& tst);
  Test& operator=(const Test &tst);
  // member functions
  inline void set_idv(long long int val) { idv = val; }
  inline void set_idv(const char* chr) { idv = atoll(chr); }
  inline long long int get_idv(void) const { return idv; }
  inline void set_date(time_t t) { creation = t; }
  inline time_t get_date(void) const { return creation; }
  void set_record(const TestType& index, const Record& rec);
  Record get_record(TestType index) const;
  inline void set_statistics(const std::map<TestType,Record>& stats) { statistics = stats;}
  inline std::map<TestType,Record> get_statistics(void) const { return statistics; }
  inline void reset_statistics(void) { statistics.clear(); }
  Record test(TestType tt, bool result);
protected:
  long long int idv;
  time_t creation;
  std::map<TestType,Record> statistics;
private:
  static std::string sqlite_test_name;
  static std::string sqlite_test_table;
  static std::string sqlite_record_name;
  static std::string sqlite_record_table;
};

#endif // _TEST_HH_
