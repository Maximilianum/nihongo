/*
 * parole_tmcr.hh
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PAROLE_TREE_MODEL_COLUMN_RECORD_HH_
#define _PAROLE_TREE_MODEL_COLUMN_RECORD_HH_

#include <gtkmm.h>

class ParoleTreeModelColumnRecord : public Gtk::TreeModelColumnRecord {
 public:
  ParoleTreeModelColumnRecord() { add(idw);
    add(wtype);
    add(kotoba);
    add(furigana);
    add(itariago);
    add(fcolour);
    add(fsize); }
  Gtk::TreeModelColumn<long long int> idw;
  Gtk::TreeModelColumn<Glib::ustring> wtype;
  Gtk::TreeModelColumn<Glib::ustring> kotoba;
  Gtk::TreeModelColumn<Glib::ustring> furigana;
  Gtk::TreeModelColumn<Glib::ustring> itariago;
  Gtk::TreeModelColumn<Gdk::RGBA> fcolour;
  Gtk::TreeModelColumn<double> fsize;
};

#endif // _PAROLE_TREE_MODEL_COLUMN_RECORD_HH_
