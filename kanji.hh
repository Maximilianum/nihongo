/*
 * kanji.hh
 *
 * Copyright (C) 2011 - 2021 Massimiliano Maniscalco <maximilianum@protonmail.com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _KANJI_HH_
#define _KANJI_HH_

#include "test.hh"

class Kanji: public Test {	
public:
  static inline std::string get_sqlite_kanji_name(void) { return sqlite_kanji_name; }
  static inline std::string get_sqlite_kanji_table(void) { return sqlite_kanji_table; }
  static int retrieve_kanji(void *data, int argc, char **argv, char **azColName);
  static int retrieve_id(void *data, int argc, char **argv, char **azColName);
  static std::list<Glib::ustring> parse_letture(const Glib::ustring& str);
  static Glib::ustring create_string(const std::list<Glib::ustring>& list);
  static bool contiene_stringa(const std::list<Glib::ustring> &list, const Glib::ustring &str);
  inline Kanji() : tratti (0), tratti_radicale (0) {}
  inline Kanji(const Glib::ustring& kan, const Glib::ustring& ony, const Glib::ustring& kun, const Glib::ustring& sig, const Glib::ustring& rad, int tra, int trar) : kanji (kan), onyomi (ony), kunyomi (kun), significato (sig), radicale (rad), tratti (tra), tratti_radicale (trar) {}
  inline Kanji(long long int id, const Glib::ustring& kan, const Glib::ustring& ony, const Glib::ustring& kun, const Glib::ustring& sig, const Glib::ustring& rad, int tra, int trar) : idk (id), kanji (kan), onyomi (ony), kunyomi (kun), significato (sig), radicale (rad), tratti (tra), tratti_radicale (trar) {}
  Kanji(const Kanji &knj);
  inline ~Kanji() {}
  Kanji& operator=(const Kanji& knj);
  bool operator<(const Kanji &knj) const;
  bool operator==(const Kanji &knj) const;
  bool operator!=(const Kanji &knj) const;
  inline void set_idk(long long int val) { idk = val; }
  inline void set_idk(const char *cstr) { idk = atoll(cstr); }
  inline long long int get_idk(void) const { return idk; }
  inline void set_kanji(const Glib::ustring &str) { kanji.assign(str); }
  inline void set_kanji(const char *cstr) { kanji.assign(cstr); }
  inline Glib::ustring get_kanji(void) const { return kanji; }
  inline void set_onyomi(const Glib::ustring& str) { onyomi.assign(str); }
  inline void set_onyomi(const char* cstr) { onyomi.assign(cstr); }
  inline Glib::ustring get_onyomi(void) const { return onyomi; }
  //inline bool contiene_onyomi(const Glib::ustring &str) const { return contiene_stringa(onyomi, str); }
  //inline unsigned char count_onyomi(void) const { return onyomi.size(); }
  inline void set_kunyomi(const Glib::ustring& str) { kunyomi.assign(str);  }
  inline void set_kunyomi(const char* cstr) { kunyomi.assign(cstr); }
  inline Glib::ustring get_kunyomi(void) const { return kunyomi; }
  //inline bool contiene_kunyomi(const Glib::ustring &str) const { return contiene_stringa(kunyomi, str); }
  //inline unsigned char count_kunyomi(void) const { return kunyomi.size(); }
  inline void set_significato(const Glib::ustring &str) { significato.assign(str); }
  inline void set_significato(const char *cstr) { significato.assign(cstr); }
  inline Glib::ustring get_significato(void) const { return significato; }
  inline void set_radicale(const Glib::ustring &str) { radicale.assign(str); }
  inline void set_radicale(const char *cstr) { radicale.assign(cstr); }
  inline Glib::ustring get_radicale(void) const { return radicale; }
  inline void set_tratti(int val) { tratti = val; }
  inline void set_tratti(const char *cstr) { tratti = atoi(cstr); }
  inline int get_tratti(void) const { return tratti; }
  inline void set_tratti_radicale(int val) { tratti_radicale = val; }
  inline void set_tratti_radicale(const char *cstr) { tratti_radicale = atoi(cstr); }
  inline int get_tratti_radicale(void) const { return tratti_radicale; }
  bool is_valid(void) const;
private:
  static std::string sqlite_kanji_name;
  static std::string sqlite_kanji_table;
  long long int idk;
  Glib::ustring kanji;
  Glib::ustring onyomi;
  Glib::ustring kunyomi;
  Glib::ustring significato;
  Glib::ustring radicale;
  int tratti;
  int tratti_radicale;
};

#endif // _KANJI_HH_
